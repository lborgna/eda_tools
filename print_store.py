from pandas import DataFrame, HDFStore


import argparse


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "quickly check the HDFStore")

    parser.add_argument('-i','--input-file', required = True, dest = 'input', metavar = 'input', type = str, help =  'Input h5 store')

    arguments = parser.parse_args()

    arguments = vars(arguments)

    print(arguments)

    filename = arguments['input']

    store = HDFStore(filename)

    print(store.keys())

    df_topo = store['df_topo']

    print(df_topo.head())
