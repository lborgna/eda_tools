from __future__ import print_function
import sys, os

sys.path.append(os.path.dirname(__file__))

import numpy as np
import pandas as pd
import histogram_helper as hh
import utilities as u
import json
import math as m
import copy
import matplotlib.pyplot as plt
from matplotlib import gridspec
from numpy import array

from collections import Counter
from collections import OrderedDict


def write_json(data, filename="test.json"):
    with open(filename, "w") as outfile:
        json.dump(data, outfile, indent=4)


def read_json(filename):
    with open(filename) as f:
        datastore = json.load(f)
    return datastore


def combined_trigger_counts(triggerMap, triggers_combined):
    combined_trigger_yield = 0
    for row in triggerMap:
        if any(key in row for key in triggers_combined):
            combined_trigger_yield += 1

    return combined_trigger_yield


def check_trigger_in_key(row, triggers_list):
    """Checks if a particular trigger/s are in a row of the triggerMap

    Parameters
    ----------
    row :
        single row of the triggerMap df
    triggers_list :
        list of triggers to check for
    Returns
    -------
    type
        True or False (used to create a mask)

    """
    if any(key in row for key in triggers_list):
        return True
    else:
        return False


def feature_array_passedTrigger(df, triggers, feature="m_hh"):
    """this function returns an array of values of a given feature of the original dataframe that have passed the desired triggers

    Parameters
    ----------
    df : type
        Description of parameter `df`.
    triggers : type
        Description of parameter `triggers`.
    feature : type
        Description of parameter `feature`.

    Returns
    -------
    type
        array of values of the feature chosen (e.g. m_hh)
    """

    column = "triggerMap"
    mask = copy.deepcopy(df[column].values)

    for row, _ in enumerate(mask):
        mask[row] = check_trigger_in_key(mask[row].keys(), triggers)

    df_reduced = df[mask]

    array = df_reduced[feature].values
    return df_reduced


def passedTriggerMask(df, triggers):
    """
    returns the mask of the dataframe showing where the trigger or triggers have fired
    """

    column = "triggerMap"
    mask = copy.deepcopy(df[column].values)

    for row, _ in enumerate(mask):
        mask[row] = check_trigger_in_key(mask[row].keys(), triggers)

    return pd.Series(mask)


def feature_efficiency(
    df, df_reduced, feature, bin_edges, error="bayesian", weight_column="mc_sf"
):
    eps = 1e-20

    print(len(df_reduced[weight_column].values))
    print(len(df[weight_column].values))
    ki, _ = np.histogram(
        df_reduced[feature].values, weights=df_reduced[weight_column], bins=bin_edges
    )  # need to add weights
    ni, _ = np.histogram(df[feature].values, weights=df[weight_column], bins=bin_edges)

    x = hh.bin_midpoint(bin_edges)
    xerr = hh.bin_width_error(bin_edges)

    if error == "binomial":
        yerr = 100 * hh.binomial_error(ki, (ni + eps))
    elif error == "bayesian":
        yerr = 100 * hh.bayesian_error(ki, (ni + eps))
    elif error == "poisson":
        yerr = 100 * hh.poissonian_error(ki, (ni + eps))

    y = 100 * (ki / (ni))

    return x, y, xerr, yerr


def feature_efficiency_plot(
    df_topo,
    df_pflow,
    triggers,
    feature,
    year="2017",
    pflow=True,
    error="bayesian",
    clean=True,
    save=False,
    show=True,
):
    plt.clf()

    topo_feature_array = feature_array_passedTrigger(df_topo, triggers, feature)
    _, bin_edges = hh.adaptive_binning(
        topo_feature_array[feature].values,
        weights=topo_feature_array["mcEventWeight"],
        threshold=20,
        bi=30,
    )

    xt, yt, xet, yet = feature_efficiency(
        df_topo, topo_feature_array, feature, bin_edges=bin_edges, error=error
    )

    # print('xt: ', xt)
    # print('yt: ', yt)
    # print('xet: ', xet)
    # print('yet: ', yet)

    if pflow:
        pflow_feature_array = feature_array_passedTrigger(df_pflow, triggers, feature)
        xp, yp, xep, yep = feature_efficiency(
            df_pflow, pflow_feature_array, feature, bin_edges=bin_edges, error=error
        )

        # print('xp: ', xp)
        # print('yp: ', yp)
        # print('xep: ', xep)
        # print('yep: ', yep)

    if pflow == False:
        if clean:
            plt.plot(xt, yt, "k--.", label="topo")
        else:
            plt.errorbar(xt, yt, xerr=xet, yerr=yet, fmt="--.k", label="topo")
        plt.fill_between(xt, yt - yet, yt + yet, alpha=0.2, facecolor="k")
        plt.xlabel("DiHiggs " + feature, fontsize=14)
        plt.ylabel("Efficiency (%)", fontsize=14)
        plt.title("Trigger efficiency " + feature + " " + year, fontsize=14)
        plt.legend()
        plt.ylim([90, 100])
        plt.xlim([200, 1500])
        if len(triggers) > 1:
            plt.savefig(
                "feature-efficiencies/"
                + feature
                + "_t"
                + str(len(triggers))
                + "_"
                + year
                + ".png",
                dpi=300,
            )
        else:
            plt.savefig(
                "feature-efficiencies/"
                + feature
                + "_"
                + str(triggers[0])
                + "_"
                + year
                + ".png",
                dpi=300,
            )
    elif pflow == True:
        fig = plt.figure(figsize=(8, 8))
        gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
        gs.update(wspace=0.0, hspace=0.0)
        ax0 = plt.subplot(gs[0])
        if clean:
            ax0.plot(xt, yt, "--.k", label="topo")
        else:
            ax0.errorbar(xt, yt, xerr=xet, yerr=yet, fmt="--.k", label="topo")
        ax0.fill_between(xt, yt - yet, yt + yet, alpha=0.2, facecolor="k")
        if clean:
            ax0.plot(xp, yp, "--.r", label="Pflow")
        else:
            ax0.errorbar(xp, yp, xerr=xep, yerr=yep, fmt="--.r", label="Pflow")
        ax0.fill_between(xp, yp - yep, yp + yep, alpha=0.2, facecolor="r")
        ax0.set_ylabel("Efficiency (%)")
        ax0.set_xticklabels([])
        ax0.legend(loc=4)
        plt.title("Feature Efficiency " + feature + " topo vs Pflow")

        ax1 = plt.subplot(gs[1])
        ax1.axhline(y=1, color="k", linestyle="--")
        R = array(yt) / (yp)
        Rx = np.abs(xt) * np.sqrt(((xet / xt) ** 2) + ((xep) / (xp) ** 2))
        Ry = np.abs(R) * np.sqrt(((yet / yt) ** 2) + ((yep / yp) ** 2))

        if clean:
            ax1.plot(xt, R, "--.g")
        else:
            ax1.errorbar(xt, R, xerr=Rx, yerr=Ry, fmt="+g")
        ax1.set_ylabel("Ratio Topo/Pflow")
        ax1.set_xlabel(feature + " GeV")
        ax1.fill_between(xt, R - Ry, R + Ry, alpha=0.2, facecolor="g")

        if save:
            if len(triggers) > 1:
                plt.savefig(
                    "feature-efficiencies/"
                    + feature
                    + "_t"
                    + str(len(triggers))
                    + "_"
                    + year
                    + ".png",
                    dpi=300,
                )
            else:
                plt.savefig(
                    "feature-efficiencies/"
                    + feature
                    + "_"
                    + str(triggers[0])
                    + "_"
                    + year
                    + ".png",
                    dpi=300,
                )
        if show:
            plt.show()


def sortSecond(val):
    return val[1]


def sort_topN_triggers(best_triggers, N=10):
    sorted_topN_triggers = sorted(best_triggers, key=sortSecond, reverse=True)
    return sorted_topN_triggers[0:N]


def sort_trigger_yields(efficiency, triggerlist, reversed=True):
    trigger_yield_dict = {key: value for key, value in zip(triggerlist, efficiency)}

    trigger_yield_sorted = dict(
        OrderedDict(
            sorted(trigger_yield_dict.items(), key=lambda kv: kv[1], reverse=reversed)
        )
    )

    efficiency_sorted = list(trigger_yield_sorted.values())
    triggerlist_sorted = list(trigger_yield_sorted.keys())

    return efficiency_sorted, triggerlist_sorted


def read_input_triggers(filename):
    """
    Reads the csv file containing the list of triggers used in the resolved reconstruction. Care needs to be taken here
    as the list contains all entries on the first row
    :param filename: string/ path to specify file location
    :return: trigger_list containing all the trigger names as strings
    """
    import csv

    trigger_list = []

    with open(filename) as csvfile:
        csv_parser = csv.reader(csvfile, delimiter=",")
        for row in csv_parser:
            for i, _ in enumerate(row):
                trigger_list.append(row[i])
    return trigger_list


def passed_triggers_counts(triggerMap, trigger_list, normalization):
    """
    The trigger Map information is stored in the triggerMap column of the dataframe as dictionaries, this function
    is used to check whether the keys in each dictionary match the any of the triggers in the trigger list and counts
    them.
    :param triggerMap: triggerMap column of the dataframe
    :param trigger_list: list of triggers
    :return: frequency, frequency_density and percentage of events passing each trigger.
    """
    counts = np.zeros(len(trigger_list))

    for i in range(len(triggerMap)):
        for j, _ in enumerate(trigger_list):
            if trigger_list[j] in triggerMap[i]:
                counts[j] += 1

    frequency = counts
    frequency_density = counts / normalization  # normalize by the length
    percentage = 100 * frequency_density
    return frequency, frequency_density, percentage


def passed_triggers_counts_weighted(triggerMap, triggers, weights):

    counts = np.zeros(len(triggers))

    total = sum(weights)

    for i, _ in enumerate(triggerMap):
        for j, _ in enumerate(triggers):
            if triggers[j] in triggerMap[i]:
                counts[j] += weights[i]

    return counts, counts / total, 100 * counts / total


def createTriggerMap(f, mc=True):
    """
    for a specific file, return the triggerMap dataframe
    triggerMap stored as a list of int
    """
    tL = f["triggerList"].pandas.df()

    """
    if mc:
        tmV = tL.groupby(["eventNumber", "rand_run_nr"])["triggerMap"].apply(list)
    else:
        tmV = tL.groupby(["eventNumber", "runNumber"])["triggerMap"].apply(list)

    tmV = tmV.reset_index()
    """

    return tL


def mergeTriggerMap(f, tree="fullmassplane", how="inner", mc=True, verbose=False):
    """Function returns the merged dataframe of the event tree and the triggerlist

    Args:
        f (uproot.file): uproot file pointer
        tree (str, optional): Treename containing the event data. Defaults to 'fullmassplane'.
        mc (bool, optional): is the file MC or data?. Defaults to True.
        verbose (bool, optional): Display information about the merging for debugging. Defaults to False.

    Returns:
        (pd.DataFrame): dataframe with event data and trigger information merged on run number and event number keys
    """
    if mc:
        event_keys = ["eventNumber", "rand_run_nr"]
    else:
        event_keys = ["eventNumber", "runNumber"]

    if verbose:
        print("loading tree")

    fmp = f[tree].pandas.df()

    fmpN = u.getNominalNNT(fmp)

    if verbose:
        print("loading triggerMap")

    tL = createTriggerMap(f, mc=mc)

    tL = tL.loc[
        tL[event_keys[0]].isin(fmpN["event_number"])
        & tL[event_keys[1]].isin(fmpN["run_number"])
    ]

    tmV = tL.groupby(event_keys)["triggerMap"].apply(list)

    """
    if mc:
        tmV = tL.groupby(["eventNumber", "rand_run_nr"])["triggerMap"].apply(list)
    else:
        tmV = tL.groupby(["eventNumber", "runNumber"])["triggerMap"].apply(list)
    """

    tmV = tmV.reset_index()

    if verbose:
        print("merging trees")
    """
    if mc:
        tMkeys = ["eventNumber", "rand_run_nr"]
    else:
        tMkeys = ["eventNumber", "runNumber"]
    """

    tmV.head()
    df_merged = pd.merge(
        tmV, fmpN, how=how, left_on=event_keys, right_on=["event_number", "run_number"]
    )

    if verbose:
        print(len(tmV))
        print(len(fmpN))
        print(len(df_merged))

    return df_merged


def nominalTriggersLUT(year, verbose=False):

    if year == 2015:
        triggers = {"1b": 1, "2b1j": 2, "2b2j": 3}

    elif year == 2016:
        triggers = {"1b": 4, "2b1j": 5, "2b2j": 6}

    elif year == 2017:
        triggers = {"2b1j": 7, "2b2j": 8, "2bHT": 9, "1b": 10}

    elif year == 2018:
        triggers = {"2b1j": 11, "2b2j": 12, "2bHT": 13, "1b": 14}

    if verbose:
        print(triggers)

    return triggers


def triggers_lut(trigger_names: list = None):

    triggers = {
        "HLT_j225_bloose": 1,
        "HLT_j100_2j55_bmedium": 2,
        "HLT_2j35_btight_2j35_L13J25.0ETA23": 3,
        "HLT_j225_bmv2c2060_split": 4,
        "HLT_j100_2j55_bmv2c2060_split": 5,
        "HLT_2j35_bmv2c2060_split_2j35_L14J15.0ETA25": 6,
        "HLT_j110_gsc150_boffperf_split_2j35_gsc55_bmv2c1070_split_L1J85_3J30": 7,
        "HLT_2j15_gsc35_bmv2c1040_split_2j15_gsc35_boffperf_split_L14J15.0ETA25": 8,
        "HLT_2j35_gsc55_bmv2c1050_split_ht300_L1HT190-J15s5.ETA21": 9,
        "HLT_j225_gsc300_bmv2c1070_split": 10,
        "HLT_j110_gsc150_boffperf_split_2j45_gsc55_bmv2c1070_split_L1J85_3J30": 11,
        "HLT_2j35_bmv2c1060_split_2j35_L14J15.0ETA25": 12,
        "HLT_2j45_gsc55_bmv2c1050_split_ht300_L1HT190-J15s5.ETA21": 13,
        #"HLT_j225_gsc300_bmv2c1070_split": 14, #this trigger is repeated
    }

    if trigger_names is not None:
        triggers = {
            trigger_key: triggers[trigger_key]
            for trigger_key in trigger_names
            if trigger_key in triggers
        }

    return triggers


def setContains(s1, s2):
    """
    Check if any of the elements of set1 are in set2
    Return True or False
    """
    if s1.intersection(s2):
        return True
    else:
        return False


def trig_check(x1: list, x2: list) -> bool:
    """
    Wrapper, to check if the triggerMap entry is NaN
    occurs when there is a missing trigger.
    """
    if np.isnan(x2).all():
        return False
    else:
        return setContains(set(x1), set(x2))


def passedTriggersMask(df, triggerList):
    """This function returns a pd.Series boolean mask of events that have passed
    any triggers in the provided triggerList. TriggerMap column is expected to be in a list format

    Args:
        df (pd.Dataframe): pandas dataframe containing triggerMap column
        triggerList (list): list of triggers to be checked
    Returns:
        pd.Series: boolean mask of events passing any trigger querried.
    """
    # return df.triggerMap.apply(lambda x: setContains(set(triggerList), set(x)))
    return df["triggerMap"].apply(lambda x: trig_check(triggerList, x))


def passedTriggersFrame(df, triggerList):
    """Function returns dataframe with events that passed any triggers in the triggerList

    Args:
        df (pd.DataFrame): pandas dataframe containing triggerMap column
        triggerList (list): list of triggers to be checked
    Returns:
        pd.DataFrame : dataframe with events that passed the trigger
    """

    return df[passedTriggersMask(df, triggerList)]


def convert_nicoles_triggermap(df, mc=True):
    """Convert Nicoles NNT trigger columns to the normal trigger map from resolved recon
    Each row should contain a list with the number of the trigger that has passed for that event.

    e.g.
    triggerMap
    '[2, 4]' (triggers 2, and 4 fired and the LUT tells us its 2b2j and 2b1j)

    Args:
        df (pd.DataFrame): NNT from nicole
        mc (bool, optional): is this a mc or data sample. Defaults to True.

    Returns:
        pd.DataFrame: dataframe with the associated triggerMap.
    """
    event_columns = ["eventNumber"]
    if mc:
        event_columns = ["rand_run_nr"] + event_columns
    else:
        event_columns = ["runNumber"] + event_columns

    trigger_names = df.columns[df.columns.str.contains("HLT")].to_list()
    trigger_conversion_dict = triggers_lut(trigger_names)
    df_renamed = df.rename(columns=trigger_conversion_dict)

    df_melted = df_renamed.melt(
        id_vars=event_columns,
        value_vars=list(trigger_conversion_dict.values()),
        var_name="triggerMap",
    )

    df_melted = df_melted.loc[df_melted["value"]]
    df_triggerMap = (
        df_melted.groupby(event_columns)["triggerMap"].apply(list).reset_index()
    )

    df = df.merge(df_triggerMap, how="left", on=event_columns)
    return df

def load_trigger_df(f, trigger_lut:dict=None, mc=True, event_number:list=None, run_number:list=None) -> pd.DataFrame:
    if trigger_lut is None:
        trigger_lut = triggers_lut()
    event_columns = ['eventNumber']
    if mc:
        event_columns.append('rand_run_nr')
    else:
        event_columns.append('runNumber')
    
    trigger_list = f['triggerList'].pandas.df()

    if (event_number is not None) and (run_number is not None):
        trigger_list = trigger_list.loc[
            trigger_list[event_columns[0]].isin(event_number) &
            trigger_list[event_columns[1]].isin(run_number)
        ]

    trigger_list['passed_trigger'] = True

    trigger_list = pd.pivot_table(trigger_list, values = 'passed_trigger', columns = 'triggerMap', index = event_columns)
    trigger_list.reset_index(inplace = True)
    trigger_list.fillna(False, inplace = True)

    trigger_lut_reversed = {v : k for k, v in trigger_lut.items() if v in trigger_list.columns}

    trigger_list.rename(columns=trigger_lut_reversed, inplace = True)
    #Align with the rest of the data trees
    trigger_list.rename(columns={
        event_columns[0]: 'event_number',
        event_columns[1]: 'run_number'
    }, inplace = True)
    return trigger_list