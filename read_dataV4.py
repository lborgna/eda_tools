import sys, os
sys.path.append(os.path.dirname(__file__))

import uproot
import pandas as pd
from pandas import DataFrame, HDFStore
import argparse
import trigger_utilities as u
import utilities as util
import numpy as np
import json

class MergeTrees(object):

    def __init__(self, object):

        #Initialize EM Topo File
        self.filename_topo = object['input']

        if object['Nevents'] is not None:
            self.Nevents_sample = object['Nevents']
            self.sample = True
        else:
            self.sample = False

        if object['mc'] is True:
            self.mc = True
        elif object['mc'] is False:
            self.mc = False

        if object['after'] is True:
            self.after = True
        elif object['after'] is False:
            self.after = False


        self.ROOT_topo = uproot.open(self.filename_topo)

        if object['newformat'] == False:
            self.df_TriggerList_topo = self.ROOT_topo["triggerList"].pandas.df()
        elif object['newformat'] == True:
            self.df_TriggerList_topo = self.NewFormatDataframe(self.ROOT_topo)


        self.df_fmp_topo = self.ROOT_topo["fullmassplane"].pandas.df()
        self.n_events_initial_topo = self.ROOT_topo["TwoTagCutflow"].values[0]
        self.n_events_selected_topo = self.ROOT_topo["TwoTagCutflow"].values[1]
        self.NTuple_topo = self.ROOT_topo["TwoTagCutflow"].values[2]
        self.TriggerMap_count_topo = self.ROOT_topo["TwoTagCutflow"].values[3]

        #Triggers are in commmon between pflow and topo
        self.triggers_filename = object['triggers']
        #self.triggers = list(set(u.read_input_triggers(self.triggers_filename))) #Unique triggers only
        self.triggers = list(set(u.read_json(self.triggers_filename)))


        #Initialize PFlow File
        if object['pflow'] is not None:
            self.is_pflow = True
            self.filename_pflow = object['pflow']
            self.ROOT_pflow = uproot.open(self.filename_pflow)

            if object['newformat'] == False:
                self.df_TriggerList_pflow = self.ROOT_pflow["triggerList"].pandas.df()
            elif object['newformat'] == True:
                self.df_TriggerList_pflow = self.NewFormatDataframe(self.ROOT_pflow)


            self.df_fmp_pflow = self.ROOT_pflow["fullmassplane"].pandas.df()
            self.n_events_initial_pflow = self.ROOT_pflow['TwoTagCutflow'].values[0]
            self.n_events_selected_pflow = self.ROOT_pflow['TwoTagCutflow'].values[1]
            self.NTuple_pflow = self.ROOT_pflow['TwoTagCutflow'].values[2]
            self.TriggerMap_count_pflow = self.ROOT_pflow['TwoTagCutflow'].values[3]
        else:
            self.is_pflow = False

        #Output h5 file name
        self.output_filename = object['output']

    def NewFormatDataframe(self, file):
        import awkward
        t = file['triggerList']

        first = t['triggerMap.first'].array()
        second = t['triggerMap.second'].array()

        triggerMap = awkward.ObjectArray(awkward.JaggedArray.zip(awkward.fromiter(first), second), lambda x: dict(zip(x.i0, x.i1)))
        df = pd.DataFrame(np.array(triggerMap), columns=['triggerMap'])

        df.insert(1, 'runNumber', t['runNumber'].array(), allow_duplicates = True)
        df.insert(2, 'eventNumber', t['eventNumber'].array(), allow_duplicates = False)

        if self.mc == True:
            df.insert(3, 'mcEventWeight', t['mcEventWeight'].array(), allow_duplicates = True)
        elif self.mc == False:
            weights_ones = np.ones(len(df))
            df.insert(3, 'mcEventWeight', weights_ones, allow_duplicates = True)
            #df.dropna(inplace = True)

        return df



    def MergeFrames(self, how = "outer"):

        #self.df_merged = pd.merge(self.df_TriggerList_after, self.df_EventInfo, how, left_on = ["eventNumber", "runNumber"], right_on = ["event_number", "run_number"])
        self.df_merged_topo = pd.merge(self.df_TriggerList_topo, self.df_fmp_topo, how, left_on = ["eventNumber"], right_on = ["event_number"])

        if self.is_pflow:
            self.df_merged_pflow = pd.merge(self.df_TriggerList_pflow, self.df_fmp_pflow, how, left_on = ['eventNumber'], right_on = ['event_number'])

    def DecodeTriggerMap(self):
        def decode_dict(row):
            decoded_dict = {}
            for k, v in row.items():
                decoded_dict[k.decode()] = v
            return decoded_dict

        if self.after:
            self.df_merged_topo.dropna(inplace = True)

        #print(self.df_merged_topo)

        self.df_merged_topo['triggerMap'] = self.df_merged_topo['triggerMap'].apply(lambda row: decode_dict(row))
        #self.df_merged_topo['region'] = self.df_merged_topo.apply(lambda x: self.which_region(x['m_h1'], x['m_h2']), axis = 1)


        #print(self.df_merged_topo)

        if self.is_pflow:

            if self.after:
                self.df_merged_pflow.dropna(inplace = True)

            self.df_merged_pflow['triggerMap'] = self.df_merged_pflow['triggerMap'].apply(lambda row: decode_dict(row))

    def AddRegion(self):

        self.df_merged_topo['region'] = self.df_merged_topo.apply(lambda x: self.WhichRegion(x['m_h1'], x['m_h2']), axis = 1)

        if self.is_pflow:
            self.df_merged_pflow = self.df_merged_pflow.apply(lambda x: self.WhichRegion(x['m_h1'], x['m_h2']), axis = 1)



    def Compression(self):

        self.df_merged_topo = self.df_merged_topo.drop(['runNumber', 'event_number', 'tag_h1_j1', 'tag_h1_j2', 'tag_h2_j1', 'tag_h2_j2'], axis = 1)

        if self.is_pflow:
            self.df_merged_pflow = self.df_merged_pflow.drop(['runNumber', 'event_number', 'tag_h1_j1', 'tag_h1_j2', 'tag_h2_j1', 'tag_h2_j2'], axis = 1)


    def WhichRegion(self, mlead, msubl):

        a = ((mlead - 120)/(0.1*mlead))**2
        b = ((msubl - 110)/(0.1*msubl))**2

        Xhh = np.sqrt(a + b)
        CR = np.sqrt((mlead - (120 * 1.05))**2 + (msubl - (110 * 1.05)**2))
        SB = np.sqrt((mlead - (120 * 1.03))**2 + (msubl - (110 * 1.03)**2))
        region = 0

        if Xhh < 1.6:
            region = 1
        elif CR < 30:
            region = 2
        elif SB < 45:
            region = 3

        return region

    def CorrectDR(self, df):

        df['dRlead'] = df.apply(lambda x: util.dR(x['eta_h1_j1'], x['eta_h1_j2'], x['phi_h1_j1'], x['phi_h1_j2']), axis = 1)
        df['dRsub'] = df.apply(lambda x: util.dR(x['eta_h2_j1'], x['eta_h2_j2'], x['phi_h2_j1'], x['phi_h2_j2']), axis = 1)

        #ADD the renaming of dRjj_1 and dRjj_2 columns to dRjj_close and dRjj_notclose respectively

        df.rename(columns = {'dRjj_1': 'dRjj_close',
                             'dRjj_2': 'dRjj_notclose'},
                  inplace = True)

        return df 

    def ApplyCorrectedDR(self):

        self.df_merged_topo = self.CorrectDR(self.df_merged_topo)

        if self.is_pflow:
            self.df_merged_pflow = self.CorrectDR(self.df_merged_pflow)




    def SaveDataH5(self):

        store = HDFStore(self.output_filename)
        if self.sample:
            store.put('df_topo', self.df_merged_topo.sample(n=self.Nevents_sample).reset_index(drop = True))
        else:
            store.put('df_topo', self.df_merged_topo)
        #Store other information as metadata for the frame in the store.
        store.get_storer('df_topo').attrs.triggers = self.triggers
        store.get_storer('df_topo').attrs.n_events_initial = self.n_events_initial_topo
        store.get_storer('df_topo').attrs.n_events_selected = self.n_events_selected_topo
        store.get_storer('df_topo').attrs.NTuple = self.NTuple_topo
        store.get_storer('df_topo').attrs.TriggerMap_count = self.TriggerMap_count_topo
        store.get_storer('df_topo').attrs.filename_topo = self.filename_topo
        store.get_storer('df_topo').attrs.triggers_filename = self.triggers_filename

        if self.is_pflow:

            if self.sample:
                store.put('df_pflow', self.df_merged_pflow.sample(n=self.Nevents_sample).reset_index(drop = True))
            else:
                store.put('df_pflow', self.df_merged_pflow)
            #Storing extra information as metadata (some of it is common with df_topo)
            store.get_storer('df_pflow').attrs.triggers = self.triggers
            store.get_storer('df_pflow').attrs.n_events_initial = self.n_events_initial_pflow
            store.get_storer('df_pflow').attrs.n_events_selected = self.n_events_selected_pflow
            store.get_storer('df_pflow').attrs.NTuple = self.NTuple_pflow
            store.get_storer('df_pflow').attrs.TriggerMap_count = self.TriggerMap_count_pflow
            store.get_storer('df_pflow').attrs.input_filename = self.filename_pflow
            store.get_storer('df_pflow').attrs.triggers_filename = self.triggers_filename



        store.close()
        print("Merged dataframe save to HDFStore with metadata as: ", self.output_filename)



if __name__ == "__main__":
    parser = argparse.ArgumentParser( description = " Please give the correct inputs")
    parser.add_argument('-i','--input-file', required = True, dest = 'input', metavar = 'input', type = str, help = "Input ROOT file to read trees and merge.")
    parser.add_argument('-t','--triggers', required = True, dest = 'triggers', metavar = 'triggers', type = str, help = "csv file containing the list of possible triggers")
    parser.add_argument('-p','--pflow', required = False, dest = 'pflow', metavar = 'pflow', type = str, help = 'pflow ROOT file path (if desired)')
    parser.add_argument('-o','--output-file', required = False, dest = 'output', metavar = 'output', type = str, help = "Output pickle filename, if none provided default is used.",  default = "dataframesH5/dataframe.h5")
    parser.add_argument('-c','--compression', required = False, dest='compression', action = 'store_true', default = False, help = "compress H5 store by removing redudant data")
    parser.add_argument('-N','--Nevents', required = False, dest='Nevents', type = int, help = 'Number of events to save (if none, saves full triggerMap)')
    parser.add_argument('-n','--new-format', required = False, dest='newformat', action = 'store_true', default = False, help = 'new version of ROOT std::map?')
    parser.add_argument('-m','--mc', required = False, dest='mc', action = 'store_true', default = False, help = 'monte-carlo or data?')
    parser.add_argument('-a','--after', required = False, dest='after', action = 'store_true', default = False, help = 'dropna to get rid of NaN (not reconstructed) values')
    arguments = parser.parse_args()
    arguments = vars(arguments)

    print(arguments)

    MT = MergeTrees(arguments)
    MT.MergeFrames()
    #MT.AddRegion() #Function deprecated due to kinematic_region already in the frame
    MT.DecodeTriggerMap()
    MT.ApplyCorrectedDR()

    if arguments['compression']:
        MT.Compression()

    MT.SaveDataH5()
