
import sys, os
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = ""
sys.path.append(os.path.dirname(__file__))
import argparse

import pandas as pd

import NN_RW as nn
import utilities as u
import numpy as np
from numpy import savez_compressed


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description= "please give me the correct inputs")
    parser.add_argument('-i', '--input-file', required = True, dest = 'input', type = str)
    parser.add_argument('-n', '--nruns', required = True, dest = 'nruns', type = int)
    parser.add_argument('-o', '--output-folder', required = True, dest ='output', type = str)
    parser.add_argument('-e', '--epochs', required= True, dest='epochs', type = int)
    parser.add_argument('-k', '--kinematic-region', required = True, dest='kr', type = int)
    parser.add_argument('-b', '--batch-size', required=False, dest='batch_size', default = 8192, type = int)
    arguments  = parser.parse_args()
    arguments = vars(arguments)

    df = pd.read_pickle(arguments['input'])
    u.calculatedPhi(df)
    u.calculatedRhh(df)
    nruns = arguments['nruns']
    folder_out = arguments['output']
    epochs = arguments['epochs']
    kr = arguments['kr']
    bs = arguments['batch_size']

    nominal = nn.nominal()
    rw_cols_to_log = nominal['rw_cols_to_log']
    rw_cols_log    = nominal['rw_cols_log']
    nn.log_inputs(df, to_log = rw_cols_to_log)

    _, _ = nn.repeatNNtrain(df, 
                                    repeat = nruns,
                                    kinematic_region = kr,
                                    sort_rw_cols = rw_cols_log,
                                    epochs = epochs,
                                    save = True,
                                    batch_size = bs,
                                    model_storage_directory=folder_out)

    #savez_compressed(fname, np.exp(weights))
