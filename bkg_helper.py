from __future__ import print_function
import sys, os

sys.path.append(os.path.dirname(__file__))

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import gridspec
import seaborn as sns
import utilities as u
import trigger_utilities as tu
import histogram_helper as hh
import nicole_tools as nt
import numpy as np
from collections import OrderedDict
from scipy.stats import wasserstein_distance
from scipy.stats import ks_2samp
from scipy.stats import norm as normG
import matplotlib.mlab as mlab
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, roc_auc_score
import atlas_mpl_style

atlas_mpl_style.set_color_cycle("Paper")


def target_reg_mask(df, target_reg):
    if target_reg == "4b":
        mask_reg = df["rw_to_4b"]
        mask_reg_vr = df["rw_to_4b_VR"]
    elif target_reg == "3b1f":
        mask_reg = df["rw_to_3b1f"]
        mask_reg_vr = df["rw_to_3bf1_VR"]

    elif target_reg == "3b1l":
        mask_reg = df["rw_to_3b1l"]
        mask_reg_vr = df["rw_to_3b1l_VR"]
    else:
        mask_reg = pd.Series(np.ones(df.shape[0], dtype=bool))
        mask_reg_vr = pd.Series(np.ones(df.shape[0], dtype=bool))

    return mask_reg, mask_reg_vr


def binned_bootstraps_variation(df, be, weight_columns, hrange=(200, 1600)):
    CR2b = df.loc[(df.ntag == 2) & (df.kinematic_region == 2)]
    N4b = len(df.loc[(df.ntag >= 4) & (df.kinematic_region == 2)])

    W = CR2b[weight_columns].values

    Wmedian = np.median(W, axis=1)

    norms = N4b / np.sum(W, axis=0)

    norms_IQR = np.subtract(*np.percentile(norms, [75, 25]))

    muQCDmedian = N4b / sum(Wmedian)

    W_IQR = np.subtract(*np.percentile(W, [75, 25], axis=1))

    w1 = muQCDmedian * Wmedian

    h1, _ = np.histogram(CR2b.m_hh_cor, bins=be, range=hrange, weights=w1)  # Step 1
    w2 = w1 + 0.5 * W_IQR
    h2, _ = np.histogram(CR2b.m_hh_cor, bins=be, range=hrange, weights=w2)  # Step 2
    w3 = w2 * (sum(h1) / sum(h2))
    h3, _ = np.histogram(CR2b.m_hh_cor, bins=be, range=hrange, weights=w3)  # Step 3
    w4 = w1 * 0.5 * norms_IQR
    h4, _ = np.histogram(CR2b.m_hh_cor, bins=be, range=hrange, weights=w4)  # Step 4
    w5 = w4 + w3
    h5, _ = np.histogram(
        CR2b.m_hh_cor, bins=be, range=hrange, weights=w5
    )  # Step 5 (upper bound)

    err_mag = w5 - w1
    w6 = w1 - err_mag
    h6, _ = np.histogram(CR2b.m_hh_cor, bins=be, weights=w6)  # Step 6 (lower bound)

    h_err_bs_mag, _ = np.histogram(
        CR2b.m_hh_cor, weights=err_mag, bins=be, range=hrange
    )
    results = {}

    results["lowerBound"] = h6
    results["upperBound"] = h5
    results["variation"] = err_mag
    results["hist_var"] = h5 - h6
    results["h_err_bs_mag"] = h_err_bs_mag
    return results


def bs_error(
    df,
    kr,
    be,
    W_med,
    W_IQR,
    mu_med,
    mu_IQR,
    feature="m_hh_cor",
    hrange=(200, 1600),
    target_reg: str = None,
):

    mask = (df.ntag == 2) & (df.kinematic_region == kr)

    mask_reg, _ = target_reg_mask(df, target_reg=target_reg)

    df2b = df.loc[mask & mask_reg]

    w1 = mu_med * df2b[W_med]
    h1, _ = np.histogram(df2b[feature], bins=be, range=hrange, weights=w1)

    w2 = w1 + 0.5 * df2b[W_IQR]
    h2, _ = np.histogram(df2b[feature], bins=be, range=hrange, weights=w2)

    w3 = w2 * (sum(h1) / sum(h2))
    h3, _ = np.histogram(df2b[feature], bins=be, range=hrange, weights=w3)

    w4 = w1 * 0.5 * mu_IQR
    h4, _ = np.histogram(df2b[feature], bins=be, range=hrange, weights=w4)

    w5 = w4 + w3
    h5, _ = np.histogram(df2b[feature], bins=be, range=hrange, weights=w5)

    err_mag = w5 - w1

    h_err_bs_mag, _ = np.histogram(
        df2b[feature], bins=be, range=hrange, weights=err_mag
    )

    results = {}

    results["err_mag"] = err_mag
    results["h_err_bs_mag"] = h_err_bs_mag

    return results


def shape_variations(
    df,
    CR_weights="NN_CR",
    VR_weights="NN_VR",
    xlimmhh=(200, 1600),
    bins=50,
    log=False,
    clean=True,
    HTvar="HT_hc",
    lw=2,
):
    eps = 1e-15
    var = {}

    for HT, tag, ylim in zip(
        [(df[HTvar] < 300), (df[HTvar] >= 300)],
        ["Low", "High"],
        [(0.93, 1.07), (0.5, 1.5)],
    ):
        fig = plt.figure(figsize=(10, 10))
        gs = gridspec.GridSpec(4, 1)
        ax1 = fig.add_subplot(gs[:3, 0])
        ax2 = fig.add_subplot(gs[3:, 0], sharex=ax1)

        # Select out background events in the signal region (ntag == 2 & kinematic_region == 0)
        bkg_SR = nt.bkgSR(df)

        # mu QCD normalization in CR and VR
        muCR = u.getNorm(df, 2, weight_column=CR_weights)
        muVR = u.getNorm(df, 2, weight_column=VR_weights)

        cr, edges, _ = ax1.hist(
            df.loc[bkg_SR, "m_hh_cor"],
            bins=bins,
            range=xlimmhh,
            lw=lw,
            color="k",
            histtype="step",
            label="2b CR RW",
            log=log,
            weights=muCR * df.loc[bkg_SR, CR_weights],
        )

        vr, _, _ = ax1.hist(
            df.loc[bkg_SR, "m_hh_cor"],
            edges,
            range=xlimmhh,
            color="b",
            lw=lw,
            histtype="step",
            label=f"{tag} $H_T$ VR Weighted",
            log=log,
            weights=np.where(
                HT[bkg_SR],
                muVR * df.loc[bkg_SR, VR_weights],
                muCR * df.loc[bkg_SR, CR_weights],
            ),
        )

        inv, _, _ = ax1.hist(
            df.loc[bkg_SR, "m_hh_cor"],
            edges,
            range=xlimmhh,
            color="r",
            lw=lw,
            histtype="step",
            label=f"{tag} $H_T$ VR Inverted",
            log=log,
            weights=np.where(
                HT[bkg_SR],
                2 * muCR * df.loc[bkg_SR, CR_weights]
                - muVR * df.loc[bkg_SR, VR_weights],
                muCR * df.loc[bkg_SR, CR_weights],
            ),
        )

        yerr_vr = hh.bayesian_error(vr + eps, cr + eps)
        yerr_inv = hh.bayesian_error(inv + eps, cr + eps)

        x = hh.bin_midpoint(edges)
        if clean:
            # ax2.fill_between(x, vr/cr + yerr_vr, vr/cr - yerr_vr, alpha =0.5, color = 'blue')
            # ax2.fill_between(x, inv/cr + yerr_inv, inv/cr - yerr_inv, alpha = 0.5, color ='r')
            ax2.plot(x, vr / cr, "k-")
            ax2.plot(x, inv / cr, "k-")
            ax2.fill_between(x, vr / cr, inv / cr, alpha=0.5, color="grey")
        else:
            ax2.plot(x, vr / cr, marker=".", color="blue", ls="None")
            ax2.plot(x, inv / cr, marker=".", color="red", ls="None")
            # ax2.errorbar(x, vr / cr, yerr = yerr_vr, marker = '.', color = 'blue', fmt = '.')
            # ax2.errorbar(x, inv / cr, yerr = yerr_inv, marker = '.', color = 'r', fmt = '.')

        """
        Append the histograms for the limits
        """

        eps = 1e-15
        var[f"{tag}_HT"] = [list(cr + eps), list(inv + eps)]
        var["cr"] = cr
        var["vr"] = vr
        var["inv"] = inv
        var["bins"] = edges

        ax2.set_xlabel("$m_{HH}$ (corrected) [GeV]", fontsize=16)
        ax1.set_ylabel("Entries", fontsize=16)

        ax2.set_ylabel("Var / Nom", fontsize=12)
        ax1.legend(fontsize=12)
        ax1.set_title(
            "Background variation " + tag + " HT: fail VBF events",
            loc="right",
            fontsize=14,
        )
        ax2.set_ylim(ylim)

    return var


def shape_variations_inclusive(
    df,
    CR_weights="NN_CR",
    VR_weights="NN_VR",
    xlimmhh=(200, 1600),
    bins=50,
    log=False,
    clean=True,
    lw=2,
    ylims=(0.95, 1.05),
    title="default title",
    unblind=False,
    density=False,
    muQCDCR=None,
    muQCDVR=None,
    bootstraps_columns=None,
    figsize=(10, 10),
    errors=False,
):
    eps = 1e-15
    var = {}

    fig = plt.figure(figsize=figsize)
    gs = gridspec.GridSpec(4, 1)
    ax1 = fig.add_subplot(gs[:3, 0])
    ax2 = fig.add_subplot(gs[3:, 0], sharex=ax1)

    bkg_SR = nt.bkgSR(df)
    muCR = 0
    muVR = 0
    if muQCDCR == None:
        muCR = u.getNorm(df, 2, weight_column=CR_weights)
    else:
        muCR = muQCDCR

    if muQCDVR == None:
        muVR = u.getNorm(df, 2, weight_column=VR_weights)
    else:
        muVR = muQCDVR

    N4bSR = len(df[(df.ntag >= 4) & (df.kinematic_region == 0)])

    cr, edges, _ = ax1.hist(
        df.loc[bkg_SR, "m_hh_cor"],
        bins=bins,
        density=density,
        range=xlimmhh,
        lw=lw,
        color="k",
        histtype="step",
        label="2b CR RW",
        log=log,
        weights=muCR * df.loc[bkg_SR, CR_weights],
    )

    x = hh.bin_midpoint(edges)
    if bootstraps_columns is not None:
        bootstraps_syst = binned_bootstraps_variation(
            df, bins, weight_columns=bootstraps_columns
        )
        cr_error = 0.5 * abs(bootstraps_syst["hist_var"])
        # print(cr_error)

        ax1.errorbar(x, cr, yerr=cr_error, fmt="+k", label="bootstrap err.")

    vr, _, _ = ax1.hist(
        df.loc[bkg_SR, "m_hh_cor"],
        edges,
        range=xlimmhh,
        density=density,
        color="b",
        lw=lw,
        histtype="step",
        label="inclusive VR Weighted",
        log=log,
        weights=muVR * df.loc[bkg_SR, VR_weights],
    )

    inv, _, _ = ax1.hist(
        df.loc[bkg_SR, "m_hh_cor"],
        edges,
        range=xlimmhh,
        density=density,
        color="r",
        lw=lw,
        histtype="step",
        label="inclusive VR Inverted",
        log=log,
        weights=2 * muCR * df.loc[bkg_SR, CR_weights]
        - muVR * df.loc[bkg_SR, VR_weights],
    )
    if unblind:
        if (N4bSR != 0) and (errors == False):
            SR4b, _, _ = ax1.hist(
                df.loc[(df.ntag >= 4) & (df.kinematic_region == 0), "m_hh_cor"],
                edges,
                range=xlimmhh,
                density=density,
                color="g",
                lw=lw,
                histtype="step",
                label="4b SR",
                log=log,
            )
        elif (N4bSR != 0) and (errors == True):
            hSR4b, _ = np.histogram(
                df.loc[(df.ntag >= 4) & (df.kinematic_region == 0), "m_hh_cor"],
                bins=edges,
                range=xlimmhh,
            )
            SR4b_error = np.sqrt(hSR4b)

            ax1.errorbar(
                hh.bin_midpoint(edges), hSR4b, yerr=SR4b_error, label="4b SR", fmt="+g"
            )

    # yerr_vr = hh.bayesian_error(vr + eps, cr + eps)
    # yerr_inv = hh.bayesian_error(inv + eps, cr + eps)

    x = hh.bin_midpoint(edges)
    if clean:
        # ax2.fill_between(x, vr/cr + yerr_vr, vr/cr - yerr_vr, alpha =0.5, color = 'blue')
        # ax2.fill_between(x, inv/cr + yerr_inv, inv/cr - yerr_inv, alpha = 0.5, color ='r')
        ax2.plot(x, vr / cr, "-", color="indigo")
        ax2.plot(x, inv / cr, "-", color="indigo")
        ax2.fill_between(
            x, vr / cr, inv / cr, alpha=0.5, color="purple", label="shape systematic"
        )
        if bootstraps_columns is not None:
            h_err_bs_mag = bootstraps_syst["h_err_bs_mag"]
            h_err_total = np.sqrt(h_err_bs_mag ** 2 + ((vr - cr) ** 2))
            ax2.fill_between(
                x,
                (h_err_total + cr) / cr,
                (cr - h_err_total) / cr,
                alpha=0.5,
                color="grey",
                label="shape+boots",
            )
            # total_b1 = np.sqrt(((vr/cr)**2) + ((cr_error/cr)**2))
            # total_b2 = np.sqrt(((inv/cr)**2) + ((cr_error/cr)**2))
            # ax2.fill_between(x, total_b1, total_b2, alpha = 0.5, color = 'grey', label = 'shape + boots')
    else:
        ax2.plot(x, vr / cr, marker=".", color="blue", linestyle="None")
        ax2.plot(x, inv / cr, marker=".", color="red", linestyle="None")
        # ax2.errorbar(x, vr / cr, yerr = yerr_vr, marker = '.', color = 'blue', fmt = '.')
        # ax2.errorbar(x, inv / cr, yerr = yerr_inv, marker = '.', color = 'r', fmt = '.')
    if unblind:
        if (N4bSR != 0) and (errors == False):
            ax2.plot(
                x,
                SR4b / cr,
                marker=".",
                color="green",
                ls="None",
                label="4b SR/2b SR RW",
            )
        elif (N4bSR != 0) and (errors == True):
            Rerr = (hSR4b / cr) * (SR4b_error / hSR4b)
            ax2.errorbar(x, hSR4b / cr, yerr=Rerr, label="4b SR", fmt="+g")

    ax2.legend()
    """
    Append the histograms for the limits
    """

    var["inclusive"] = [list(vr + eps), list(inv + eps)]
    var["vr"] = vr
    var["cr"] = cr
    var["inv"] = inv
    var["bins"] = edges
    if unblind and errors == False:
        var["SR4b"] = SR4b
    elif unblind and errors == True:
        var["SR4b"] = hSR4b
        var["SR4bErr"] = SR4b_error
    if bootstraps_columns is not None:
        var["cr_error"] = cr_error

    ax2.set_xlabel("$m_{HH}$ (corrected) [GeV]", fontsize=16)
    ax1.set_ylabel("Entries", fontsize=16)

    ax2.set_ylabel("Var / Nom", fontsize=12)
    ax1.legend(fontsize=12)
    ax1.set_title("Background variation: " + title, loc="right", fontsize=14)
    ax2.set_ylim(ylims)

    return var


def shape_variations_inclusive2(
    df,
    CR_weights="NN_CR",
    VR_weights="NN_VR",
    xlimmhh=(200, 1600),
    bins=50,
    log=False,
    feature="m_hh_cor",
    clean=True,
    lw=2,
    ylims=(0.95, 1.05),
    title="default title",
    unblind=False,
    density=False,
    muQCDCR=None,
    muQCDVR=None,
    bootstraps_columns=None,
    bs_dict=None,
    figsize=(10, 10),
    errors=False,
):
    eps = 1e-15
    var = {}

    fig = plt.figure(figsize=figsize)
    gs = gridspec.GridSpec(4, 1)
    ax1 = fig.add_subplot(gs[:3, 0])
    ax2 = fig.add_subplot(gs[3:, 0], sharex=ax1)

    bkg_SR = nt.bkgSR(df)
    muCR = 0
    muVR = 0
    if muQCDCR == None:
        muCR = u.getNorm(df, 2, weight_column=CR_weights)
    else:
        muCR = muQCDCR

    if muQCDVR == None:
        muVR = u.getNorm(df, 2, weight_column=VR_weights)
    else:
        muVR = muQCDVR

    N4bSR = len(df[(df.ntag >= 4) & (df.kinematic_region == 0)])

    cr, edges, _ = ax1.hist(
        df.loc[bkg_SR, feature].values,
        bins=bins,
        density=density,
        range=xlimmhh,
        lw=lw,
        color="k",
        histtype="step",
        label="2b CR RW",
        log=log,
        weights=muCR * df.loc[bkg_SR, CR_weights].values,
    )

    x = hh.bin_midpoint(edges)
    if bootstraps_columns is not None:
        bootstraps_syst = binned_bootstraps_variation(
            df, bins, weight_columns=bootstraps_columns
        )
        cr_error = 0.5 * abs(bootstraps_syst["hist_var"])
        # print(cr_error)

        ax1.errorbar(x, cr, yerr=cr_error, fmt="+k", label="bootstrap err.")
    elif bs_dict is not None:
        mu_QCD_IQR = bs_dict["mu_QCD_IQR"]
        weight_IQR_column = bs_dict["weight_IQR_column"]

        bs_syst = bs_error(
            df, 0, edges, CR_weights, weight_IQR_column, muCR, mu_QCD_IQR, feature
        )
        h_err_bs_mag = bs_syst["h_err_bs_mag"]

        cr_poisson = np.sqrt(
            np.histogram(
                df.loc[bkg_SR, feature].values,
                bins=edges,
                weights=muCR * df.loc[bkg_SR, CR_weights].values,
            )[0]
        )
        """
        cr_poisson = u.weighted_histo_error(df.loc[bkg_SR, feature].values,
            muCR * df.loc[bkg_SR, CR_weights], edges
        )
        """
        total_err = np.sqrt(h_err_bs_mag ** 2 + cr_poisson ** 2)
        ax1 = u.plotHatch(edges, cr - total_err, cr + total_err, ax1)

    vr, _, _ = ax1.hist(
        df.loc[bkg_SR, feature].values,
        edges,
        range=xlimmhh,
        density=density,
        color="b",
        lw=lw,
        histtype="step",
        label="inclusive VR Weighted",
        log=log,
        weights=muVR * df.loc[bkg_SR, VR_weights].values,
    )

    inv, _, _ = ax1.hist(
        df.loc[bkg_SR, feature].values,
        edges,
        range=xlimmhh,
        density=density,
        color="r",
        lw=lw,
        histtype="step",
        label="inclusive VR Inverted",
        log=log,
        weights=2 * muCR * df.loc[bkg_SR, CR_weights]
        - muVR * df.loc[bkg_SR, VR_weights].values,
    )
    if unblind:
        if (N4bSR != 0) and (errors == False):
            SR4b, _, _ = ax1.hist(
                df.loc[(df.ntag >= 4) & (df.kinematic_region == 0), feature].values,
                edges,
                range=xlimmhh,
                density=density,
                color="g",
                lw=lw,
                histtype="step",
                label="4b SR",
                log=log,
            )
        elif (N4bSR != 0) and (errors == True):
            hSR4b, _ = np.histogram(
                df.loc[(df.ntag >= 4) & (df.kinematic_region == 0), feature],
                bins=edges,
                range=xlimmhh,
            )
            SR4b_error = np.sqrt(hSR4b)

            ax1.errorbar(
                hh.bin_midpoint(edges), hSR4b, yerr=SR4b_error, label="4b SR", fmt="+g"
            )

    # yerr_vr = hh.bayesian_error(vr + eps, cr + eps)
    # yerr_inv = hh.bayesian_error(inv + eps, cr + eps)

    x = hh.bin_midpoint(edges)
    if clean:
        # ax2.fill_between(x, vr/cr + yerr_vr, vr/cr - yerr_vr, alpha =0.5, color = 'blue')
        # ax2.fill_between(x, inv/cr + yerr_inv, inv/cr - yerr_inv, alpha = 0.5, color ='r')
        ax2.plot(x, vr / cr, "-", color="indigo")
        ax2.plot(x, inv / cr, "-", color="indigo")
        ax2.fill_between(
            x, vr / cr, inv / cr, alpha=0.5, color="purple", label="shape systematic"
        )
        if bootstraps_columns is not None:
            h_err_bs_mag = bootstraps_syst["h_err_bs_mag"]
            h_err_total = np.sqrt(h_err_bs_mag ** 2 + ((vr - cr) ** 2))
            ax2.fill_between(
                x,
                (h_err_total + cr) / cr,
                (cr - h_err_total) / cr,
                alpha=0.5,
                color="grey",
                label="shape+boots",
            )
            # total_b1 = np.sqrt(((vr/cr)**2) + ((cr_error/cr)**2))
            # total_b2 = np.sqrt(((inv/cr)**2) + ((cr_error/cr)**2))
            # ax2.fill_between(x, total_b1, total_b2, alpha = 0.5, color = 'grey', label = 'shape + boots')
        elif bs_dict is not None:
            h_err_total = np.sqrt(total_err ** 2 + ((vr - cr) ** 2))
            h_bs_shape = np.sqrt(h_err_bs_mag ** 2 + ((vr - cr) ** 2))
            ax2.fill_between(
                x,
                (h_bs_shape + cr) / cr,
                (cr - h_bs_shape) / cr,
                alpha=0.5,
                color="grey",
                label="shape + boots",
            )
    else:
        ax2.plot(x, vr / cr, marker=".", color="blue", linestyle="None")
        ax2.plot(x, inv / cr, marker=".", color="red", linestyle="None")
        # ax2.errorbar(x, vr / cr, yerr = yerr_vr, marker = '.', color = 'blue', fmt = '.')
        # ax2.errorbar(x, inv / cr, yerr = yerr_inv, marker = '.', color = 'r', fmt = '.')
    if unblind:
        if (N4bSR != 0) and (errors == False):
            ax2.plot(
                x,
                SR4b / cr,
                marker=".",
                color="green",
                ls="None",
                label="4b SR/2b SR RW",
            )
        elif (N4bSR != 0) and (errors == True):
            Rerr = (hSR4b / cr) * (SR4b_error / hSR4b)
            ax2.errorbar(x, hSR4b / cr, yerr=Rerr, label="4b SR", fmt="+g")

    ax2.legend()
    """
    Append the histograms for the limits
    """

    var["inclusive"] = [list(vr + eps), list(inv + eps)]
    var["vr"] = vr
    var["cr"] = cr
    var["inv"] = inv
    var["bins"] = edges
    if unblind and errors == False:
        var["SR4b"] = SR4b
    elif unblind and errors == True:
        var["SR4b"] = hSR4b
        var["SR4bErr"] = SR4b_error
    if bootstraps_columns is not None:
        var["cr_error"] = cr_error
    elif bs_dict is not None:
        var["h_err_total"] = h_err_total
        var["bs_err"] = h_err_bs_mag
        var["cr_poisson"] = cr_poisson
        var["err_bkg"] = h_bs_shape
    x_label = u.x_label_dict[feature]
    ax2.set_xlabel(x_label, fontsize=16)
    ax1.set_ylabel("Entries", fontsize=16)

    ax2.set_ylabel("Var / Nom", fontsize=12)
    ax1.legend(fontsize=12)
    ax1.set_title("Background variation: " + title, loc="right", fontsize=14)
    ax2.set_ylim(ylims)

    return var


def classifierReweighted(
    df,
    weight_column="NN_CR50",
    feature_columns=["pT_2", "pT_4", "dRjj_1", "dRjj_2", "eta_i", "njets"],
    kr=2,
):

    df[feature_columns] = df[feature_columns] / df[feature_columns].max()
    target_column = "is4b"
    df[target_column] = df["ntag"].apply(lambda x: 1 if x >= 4 else 0)

    region_mask = df.kinematic_region == kr
    X = df.loc[region_mask, feature_columns].values
    y = df.loc[region_mask, target_column].values

    N4b = len(df[(df.ntag >= 4) & (df.kinematic_region == kr)])
    N2b = sum(df[(df.ntag == 2) & (df.kinematic_region == kr)][weight_column].values)

    muQCD = N4b / N2b
    W = muQCD * df.loc[region_mask, weight_column].values

    Xtr, Xts, Ytr, Yts, Wtr, Wts = train_test_split(
        X, y, W, random_state=42, train_size=0.51, test_size=0.49
    )

    clf = GradientBoostingClassifier(subsample=0.3, n_estimators=50).fit(
        Xtr, Ytr, sample_weight=Wtr
    )

    score = roc_auc_score(Yts, clf.predict_proba(Xts)[:, 1], sample_weight=Wts)
    fpr, tpr, threshold = roc_curve(
        Yts, clf.predict_proba(Xts)[:, 1], sample_weight=Wts
    )

    return score, (fpr, tpr)


def run_pulls(h_obs, h_obs_err, h_exp, h_exp_err):
    h_pull = np.zeros(len(h_obs))
    for i in range(len(h_obs)):
        nbObs = h_obs[i]
        nbExp = h_exp[i]
        nbExpEr = np.sqrt(h_obs_err[i] ** 2 + h_exp_err[i] ** 2)
        if nbExp == 0 or nbObs == 0:
            continue
        # significance calculated from https://cds.cern.ch/record/2643488
        # relabel variables to match CDS formula
        factor1 = nbObs * np.log(
            (nbObs * (nbExp + nbExpEr ** 2)) / (nbExp ** 2 + nbObs * nbExpEr ** 2)
        )
        factor2 = (nbExp ** 2 / nbExpEr ** 2) * np.log(
            1 + (nbExpEr ** 2 * (nbObs - nbExp)) / (nbExp * (nbExp + nbExpEr ** 2))
        )
        if nbObs < nbExp:
            pull = -np.sqrt(2 * (factor1 - factor2))
        else:
            pull = np.sqrt(2 * (factor1 - factor2))
        h_pull[i] = pull
    return h_pull


def run_pulls_simple(h_obs, h_obs_err, h_exp, h_exp_err):

    error = np.sqrt(h_obs_err ** 2 + h_exp_err ** 2)

    h_pull = (h_obs - h_exp) / error

    return h_pull


def reweightedHist(
    df,
    kinematic_region=2,
    feature="m_hh_cor",
    bins=50,
    hrange=(200, 1400),
    figsize=(8, 8),
    weighted=True,
    w=None,
    filename=None,
    bootstrap=None,
    muQCD=None,
    xlabel=None,
    weight_column="w_2b",
    histtype="step",
    color="darkorange",
    linewidth=2,
    norm=1,
    display=False,
    title="default",
    pull="atlas",
    pull_fit=False,
    density=True,
    show_stats=True,
):
    eps = 1e-15

    d2b = df[(df.ntag == 2) & (df.kinematic_region == kinematic_region)]
    d4b = df[(df.ntag >= 4) & (df.kinematic_region == kinematic_region)]

    four_b = np.sum((df.kinematic_region == kinematic_region) & (df.ntag >= 4))
    if w is not None:
        two_b = np.sum(w)
    else:
        two_b = df.loc[
            ((df.kinematic_region == kinematic_region) & (df.ntag == 2)), weight_column
        ].sum()

    if muQCD is None:
        N4bTo2b = four_b / two_b
    else:
        N4bTo2b = muQCD

    if weighted == True and w is None:
        weights = N4bTo2b * d2b[weight_column].values
    elif weighted == False and w is None:
        weights = N4bTo2b * np.ones(len(d2b))
    elif w is not None:
        weights = N4bTo2b * w

    y4b, be = np.histogram(d4b[feature], bins=bins, range=hrange, density=density)
    f4b, _ = np.histogram(d4b[feature], bins=be, range=hrange)
    f2b, _ = np.histogram(d2b[feature], bins=be, weights=weights, range=hrange)
    print("this is my poisson error")
    poisson_err = np.sqrt(
        np.histogram(d2b[feature].values, bins=be, weights=weights)[0]
    )

    # poisson_err = u.weighted_histo_error(d2b[feature].values, weights, be)

    x4b = hh.bin_midpoint(be)
    x4b_err = hh.bin_width_error(be)

    if density == True:
        y4b_err = np.where(f4b > 0, y4b * (np.sqrt(f4b) / (f4b + eps)), 0)
    elif density == False:
        y4b_err = np.where(f4b > 0, np.sqrt(f4b), 0)

    fig = plt.figure(figsize=figsize)
    gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
    gs.update(wspace=0.0, hspace=0.0)
    ax0 = plt.subplot(gs[0])
    ax0.errorbar(x4b, y4b, xerr=x4b_err, yerr=y4b_err, fmt="k+", label="4b")

    y2b, _, _ = ax0.hist(
        d2b[feature],
        bins=be,
        density=density,
        weights=weights,
        range=hrange,
        histtype="stepfilled",
        color="yellow",
        linewidth=1,
        label="2b",
        edgecolor="black",
        zorder=1,
    )
    """
    y2b, _ = np.histogram(d2b[feature], bins = be, density = density, weights = weights, range = hrange)

    ax0.plot(x4b, y2b, 'r+', label = '2b', zorder = 1)
    """
    if bootstrap is not None:
        bs_result = bs_error(
            df,
            kinematic_region,
            be,
            bootstrap["W_med"],
            bootstrap["W_IQR"],
            bootstrap["mu_med"],
            bootstrap["mu_IQR"],
            feature=feature,
            hrange=hrange,
        )
        f2b_err = np.sqrt(bs_result["h_err_bs_mag"] ** 2 + poisson_err ** 2)
    else:
        f2b_err = poisson_err
    # ax0.fill_between(x4b, f2b-f2b_err, f2b+f2b_err, hatch="\\\\\\\\\\\\", edgecolor = "grey", linewidth = 0.0, facecolor="None", step = "mid", label = 'Stat Error', zorder= 10)
    ax0 = u.plotHatch(be, f2b - f2b_err, f2b + f2b_err, ax0, label="Stat Error")

    ax1 = plt.subplot(gs[1])
    eps = 1e-15
    R = y4b / (y2b + 1e-15)
    Rdx = x4b_err

    if pull == "simple":
        pulls = (y2b - y4b) / y4b_err

    if pull == "atlas":

        pulls = u.run_pulls(f2b, f4b)
        ax1.plot(x4b, pulls, "k+")
        ax1.set_ylabel("Pull", fontsize=14)
        # atlas_mpl_style.set_ylabel('Pull', ax = ax1, fontsize = 14)
        ax1.axhline(y=0, color="k", ls="--")
        maxAx = np.max(np.abs(pulls))
        ax1.set_ylim((-1.1 * maxAx, 1.1 * maxAx))

        if pull_fit == True:
            print("function not yet ready")
            # (mu, sigma) = normG.fit(pull)
            # a = plt.plot([0.65, 0.6, 0.2, 0.2])
            # n, bins, patches = plt.hist(pull, 10, density = 1)
            # y = normG.pdf(bins, mu, sigma)
            # plt.plot(bins, y, 'r--', lw=2)
            # plt.title(r'$\mathrm{Histogram\ of\ pullssss:}\ \mu=%.3f,\ \sigma=%.3f$' %(mu, sigma))
            # ax1.set_ylim(-10*10**-4, 10*10**-4)
    elif pull == None:
        pulls = None
        R = y4b / (y2b + eps)
        Rdy = y4b_err / (y2b + 1e-15)
        R2b = R * (f2b_err / (f2b + eps))
        err_total = np.sqrt(Rdy ** 2 + R2b ** 2)
        ax1.errorbar(x4b, R, xerr=Rdx, yerr=Rdy, fmt=".k", elinewidth=1, capsize=0)
        ax1.set_ylabel(r"$\frac{4b}{2b}$", fontsize=14)
        ax1.set_ylim((0.5, 1.5))
        ax1.axhline(y=1, color="k", linestyle="--")
        # fax1.fill_between(x4b, (f2b-f2b_err)/(f2b+eps), (f2b+f2b_err)/(f2b+eps), hatch="\\\\\\\\\\\\", edgecolor = "grey", linewidth = 0.0, facecolor="None", step = "mid", label = 'Stat Error', zorder= 1)
        ax1 = u.plotHatch(
            be,
            (f2b - f2b_err) / (f2b + eps),
            (f2b + f2b_err) / (f2b + eps),
            ax1,
            label="Stat Error",
        )

    ax0.legend(loc="upper right", fontsize=16)
    # ax0.set_ylabel('Frequency', fontsize = 16)
    atlas_mpl_style.set_ylabel("Events", ax=ax0, fontsize=14)
    ax0.set_title(title)
    if xlabel == None:
        atlas_mpl_style.set_xlabel(feature, ax=ax1, fontsize=14)
    else:
        atlas_mpl_style.set_xlabel(xlabel, ax=ax1, fontsize=14)

    y2b_before, _ = np.histogram(d2b[feature], bins=be)

    chi_squared = np.sum(np.where(f4b > 0, ((f2b - f4b) ** 2) / (f4b + eps), 0))

    ndf = len(f2b) - 1

    chi_nnof = chi_squared / ndf

    if show_stats == True:

        a = "{:.3f}".format(chi_squared)
        b = "{:.0f}".format(ndf)
        c = "{:.3f}".format(chi_nnof)

        chi_def = r"$\frac{\chi^2}{ndf}$"
        chi_string = chi_def + " = " r"$\frac{" + a + "}{" + b + "} = " + c + "$"

        ax0.text(
            0.975,
            0.55,
            chi_string,
            ha="right",
            va="top",
            transform=ax0.transAxes,
            color="black",
            fontsize=14,
        )

    results = {
        "y2b": y2b,
        "y4b": y4b,
        "pulls": pulls,
        "bins": be,
        "f2b": f2b,
        "poisson_error": poisson_err,
        "f4b": f4b,
        "N4bTo2b": N4bTo2b,
        "chi_noff": chi_nnof,
    }
    if filename is not None:
        plt.savefig(filename, dpi=600)

    return results


def unweightedHist(
    df,
    kinematic_region=2,
    feature="m_hh_cor",
    bins=50,
    hrange=(200, 1400),
    figsize=(8, 8),
    filename=None,
    xlabel=None,
    histtype="step",
    color="darkorange",
    linewidth=2,
    display=False,
    title="default",
    pull="atlas",
    show_stats=True,
):
    eps = 1e-15

    d2b = df[(df.ntag == 2) & (df.kinematic_region == kinematic_region)]
    d4b = df[(df.ntag >= 4) & (df.kinematic_region == kinematic_region)]

    N4b = len(d4b)
    N2b = len(d2b)

    muQCD = N4b / N2b

    W2b = muQCD * np.ones(len(d2b))

    f4b, be = np.histogram(d4b[feature].values, bins=bins, range=hrange)
    f4b_err = np.sqrt(f4b)

    f2b, _ = np.histogram(d2b[feature], bins=bins, range=hrange, weights=W2b)
    f2bRaw, _ = np.histogram(d2b[feature], bins=bins, range=hrange)
    f2bRaw_err = np.sqrt(f2bRaw)
    f2b_err = f2b * (f2bRaw_err / f2bRaw)
    # f2b_err = u.weighted_histo_error(d2b[feature].values, W2b, be)

    x = hh.bin_midpoint(be)
    x_err = hh.bin_width_error(be)

    fig = plt.figure(figsize=figsize)
    gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
    gs.update(wspace=0.0, hspace=0.0)
    ax0 = plt.subplot(gs[0])
    ax0.errorbar(x, f4b, xerr=x_err, yerr=f4b_err, fmt="k+", label="4b")

    y2b, _, _ = ax0.hist(
        d2b[feature].values,
        bins=be,
        weights=W2b,
        range=hrange,
        histtype="stepfilled",
        color="yellow",
        linewidth=1,
        label="2b",
        edgecolor="black",
        zorder=1,
    )

    ax0 = u.plotHatch(be, f2b - f2b_err, f2b + f2b_err, ax0, label="Stat Error")

    ax1 = plt.subplot(gs[1], sharex=ax0)
    eps = 1e-15
    R = f4b / (f2b + 1e-15)
    Rdx = x_err
    pulls = None

    if pull == "simple":
        tot_err = np.sqrt(f4b_err ** 2 + f2b_err ** 2)
        pulls = (f2b - f4b) / tot_err
        ax1.plot(x, pulls, "k+")
        ax1.set_ylabel("Pull", fontsize=14)
        ax1.axhline(0, color="k", ls="--")
        ax1.axhline(-1, color="dimgrey", ls="--")
        ax1.axhline(1, color="dimgrey", ls="--")

    if pull == "atlas":

        pulls = u.run_pulls(f2b, f4b)
        ax1.plot(x, pulls, "k+")
        ax1.set_ylabel("Pull", fontsize=14)
        # atlas_mpl_style.set_ylabel('Pull', ax = ax1, fontsize = 14)
        ax1.axhline(y=0, color="k", ls="--")
        maxAx = np.max(np.abs(pulls))
        ax1.set_ylim((-1.1 * maxAx, 1.1 * maxAx))

    elif pull == None:
        R = f4b / (f2b + eps)
        Rdy = f4b_err / (y2b + eps)
        R2b = R * (f2b_err / (f2b + eps))
        err_total = np.sqrt(Rdy ** 2 + R2b ** 2)
        ax1.errorbar(x, R, xerr=Rdx, yerr=Rdy, fmt=".k", elinewidth=1, capsize=0)
        ax1.set_ylabel(r"$\frac{4b}{2b}$", fontsize=14)
        ax1.set_ylim((0.5, 1.5))
        ax1.axhline(y=1, color="k", linestyle="--")
        # fax1.fill_between(x4b, (f2b-f2b_err)/(f2b+eps), (f2b+f2b_err)/(f2b+eps), hatch="\\\\\\\\\\\\", edgecolor = "grey", linewidth = 0.0, facecolor="None", step = "mid", label = 'Stat Error', zorder= 1)
        # ax1 = u.plotHatch(be, (f2b-f2b_err)/(f2b+eps), (f2b+f2b_err)/(f2b+eps), ax1, label = "Stat Error")

    ax0.legend(loc="upper right", fontsize=16)
    # ax0.set_ylabel('Frequency', fontsize = 16)
    atlas_mpl_style.set_ylabel("Events", ax=ax0, fontsize=14)
    ax0.set_title(title)
    if xlabel == None:
        atlas_mpl_style.set_xlabel(feature, ax=ax1, fontsize=14)
    else:
        atlas_mpl_style.set_xlabel(xlabel, ax=ax1, fontsize=14)

    chi2, ndf, p = u.chi2_unweighted(f4b, f2b)

    if show_stats == True:

        a = "{:.3f}".format(chi2 * ndf)
        b = "{:.0f}".format(ndf)
        c = "{:.3f}".format(chi2)

        chi_def = r"$\frac{\chi^2}{ndf}$"
        chi_string = chi_def + " = " r"$\frac{" + a + "}{" + b + "} = " + c + "$"

        ax0.text(
            0.975,
            0.55,
            chi_string,
            ha="right",
            va="top",
            transform=ax0.transAxes,
            color="black",
            fontsize=14,
        )
        ax0.text(
            0.975,
            0.4,
            "(p-value: {:.1e})".format(p),
            ha="right",
            va="top",
            transform=ax0.transAxes,
            color="black",
            fontsize=14,
        )

    results = {
        "f2b": f2b,
        "f4b": f4b,
        "pulls": pulls,
        "R": R,
        "bins": be,
        "muQCD": muQCD,
        "chi_ndf": chi2,
        "ndf": ndf,
        "p-value": p,
    }
    if filename is not None:
        plt.savefig(filename, dpi=600)

    return results


def shapeSystNoPlot(
    df,
    CR_weights="NN_CR",
    VR_weights="NN_VR",
    xlimmhh=(200, 1600),
    bins=50,
    HTvar="HT_hc",
):

    eps = 1e-15
    var = {}

    for HT, tag, ylim in zip(
        [(df[HTvar] < 300), (df[HTvar] >= 300)],
        ["Low", "High"],
        [(0.93, 1.07), (0.5, 1.5)],
    ):

        # Select out background events in the signal region (ntag == 2 & kinematic_region == 0)
        bkg_SR = nt.bkgSR(df)

        # mu QCD normalization in CR and VR
        muCR = u.getNorm(df, 2, weight_column=CR_weights)
        muVR = u.getNorm(df, 2, weight_column=VR_weights)

        cr, edges = np.histogram(
            df.loc[bkg_SR, "m_hh_cor"],
            bins=bins,
            range=xlimmhh,
            weights=muCR * df.loc[bkg_SR, CR_weights],
        )

        vr, _ = np.histogram(
            df.loc[bkg_SR, "m_hh_cor"],
            edges,
            range=xlimmhh,
            weights=np.where(
                HT[bkg_SR],
                muVR * df.loc[bkg_SR, VR_weights],
                muCR * df.loc[bkg_SR, CR_weights],
            ),
        )

        inv, _ = np.histogram(
            df.loc[bkg_SR, "m_hh_cor"],
            edges,
            range=xlimmhh,
            weights=np.where(
                HT[bkg_SR],
                2 * muCR * df.loc[bkg_SR, CR_weights]
                - muVR * df.loc[bkg_SR, VR_weights],
                muCR * df.loc[bkg_SR, CR_weights],
            ),
        )

        """
        Append the histograms for the limits
        """

        eps = 1e-15
        var[f"{tag}_HT"] = [list(vr + eps), list(inv + eps)]
        var["cr"] = cr
        var["vr"] = vr
        var["inv"] = inv
        var["bins"] = edges

    return var


def shapeSystNoPlotNoHT(
    df, CR_weights="NN_CR", VR_weights="NN_VR", xlimmhh=(200, 1600), bins=50
):

    eps = 1e-15
    var = {}

    # Select out background events in the signal region (ntag == 2 & kinematic_region == 0)
    bkg_SR = nt.bkgSR(df)

    # mu QCD normalization in CR and VR
    muCR = u.getNorm(df, 2, weight_column=CR_weights)
    muVR = u.getNorm(df, 2, weight_column=VR_weights)

    cr, edges = np.histogram(
        df.loc[bkg_SR, "m_hh_cor"],
        bins=bins,
        range=xlimmhh,
        weights=muCR * df.loc[bkg_SR, CR_weights],
    )

    vr, _ = np.histogram(
        df.loc[bkg_SR, "m_hh_cor"],
        edges,
        range=xlimmhh,
        weights=muVR * df.loc[bkg_SR, VR_weights],
    )

    inv, _ = np.histogram(
        df.loc[bkg_SR, "m_hh_cor"],
        edges,
        range=xlimmhh,
        weights=2 * muCR * df.loc[bkg_SR, CR_weights]
        - muVR * df.loc[bkg_SR, VR_weights],
    )

    """
    Append the histograms for the limits
    """

    eps = 1e-15
    var["shape_syst"] = [list(vr + eps), list(inv + eps)]
    var["cr"] = cr
    var["vr"] = vr
    var["inv"] = inv
    var["bins"] = edges

    return var


"""
def bkgSystematics(df, bins = 50, range = (250, 1400), feature = 'm_hh_cor', CR_weights = 'NN_weight_median_CR',
                   VR_weights = 'NN_weight_median_VR'):

    bkg_SR = bkgSR(df)
    #sig_SR = sigSR(df)

    muCR = u.getNorm(df, 2, weight_column = CR_weights)
    muVR = u.getNorm(df, 1, weight_column = VR_weights)

    var = {}

    cr, edges = np.histogram(df.loc[bkg_SR, feature],
                                bins = bins,
                                range = range,
                                weights = muCR * df.loc[bkg_SR, CR_weights]
                                )

    HTbins = [(df.HT < 300), (df.HT >= 300)]
    labels =['Low', 'High']
    for HT, tag in zip(HTbins, labels):
        vr, _     = np.histogram(df.loc[bkg_SR, feature],
                                    bins = bins,
                                    range = range,
                                    weights = np.where(HT[bkg_SR], NormCR * df.loc[bkg_SR, CR_weights], NormSB * df.loc[bkg_SR, SB_weights])
                                    )

        inv, _    = np.histogram(df.loc[bkg_SR, feature],
                                    bins = bins,
                                    range = range,
                                    weights = np.where(HT[bkg_SR], 2 * NormSB * df.loc[bkg_SR, SB_weights] - NormCR * df.loc[bkg_SR, CR_weights], NormSB * df.loc[bkg_SR, SB_weights])
                                    )
        
        eps = 1e-15
        var[f'{tag}_HT'] = [list(cr + eps), list(inv + eps)]

    return sb, var, edges
"""