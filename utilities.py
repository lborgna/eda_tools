import sys, os
import pandas as pd

sys.path.append(os.path.dirname(__file__))

from scipy import stats
from scipy.interpolate import interp1d

from scipy.optimize import fsolve


import matplotlib.pyplot as plt
from matplotlib import gridspec
import nicole_tools as nt
from tqdm import tqdm
import numpy as np
import pyhf_models as pm


def userOptionsUndefined(user_options, default_options):

    difference = user_options.keys() - default_options.keys()

    if difference != set():
        raise KeyError(
            "Invalid option passed, ", difference, " is/are not valid options"
        )


def getNominalNNT(df):
    """get nominal (level 0) mc_sf dataframe from multi-index MC signal sample"""
    dfN = df.xs(0, axis=0, level=1, drop_level=True)
    return dfN


def dR(eta1, eta2, phi1, phi2):

    deta = eta1 - eta2
    dphi = np.abs(phi1 - phi2)

    if dphi >= np.pi:
        dphi = 2 * np.pi - dphi
    else:
        dphi = phi1 - phi2

    deltaR = np.sqrt(deta ** 2 + dphi ** 2)

    return deltaR


def reverse_order(x):

    x_reverse_sorted = np.zeros(len(x))

    for i, _ in enumerate(x):
        x_reverse_sorted[i] = x[len(x) - 1 - i]

    return x_reverse_sorted


def drawSR():
    xmin = 120 / 1.16
    xmax = 120 / 0.840000000001

    x = np.linspace(xmin, xmax, num=50)
    a = np.power(10 * (x - 120) / x, 2)

    y1 = 110 / (1 + 0.1 * np.sqrt(np.power(1.6, 2) - a))
    y2 = 110 / (1 - 0.1 * np.sqrt(np.power(1.6, 2) - a))

    x_reverse_sorted = reverse_order(x)

    x = np.append(x, x_reverse_sorted)
    y2 = reverse_order(y2)
    y = np.append(y1, y2)

    return x, y


def drawCR():

    xmin = 124 - 30
    xmax = 124 + 30

    x = np.linspace(xmin, xmax, num=50)  # 50 points by default
    a = np.sqrt(30 ** 2 - np.power(x - 124, 2))
    y1 = 113 + a
    y2 = 113 - a

    x_reverse_sorted = reverse_order(x)

    x = np.append(x, x_reverse_sorted)
    y2 = reverse_order(y2)
    y = np.append(y1, y2)

    return x, y


def drawSB():
    xmin = 126 - 45
    xmax = 126 + 45

    x = np.linspace(xmin, xmax, num=50)
    a = np.sqrt(45 ** 2 - np.power(x - 126, 2))
    y1 = 116 + a
    y2 = 116 - a

    x_reverse_sorted = reverse_order(x)

    x = np.append(x, x_reverse_sorted)
    y2 = reverse_order(y2)
    y = np.append(y1, y2)

    return x, y


def getKappaLambdaLimit(
    x_obs,
    y_obs,
    x_theory,
    y_theory,
    starting_points=[-15, 15],
    show_results=False,
    display=False,
    error_fsolve=False,
):
    """
    Given the x and y values of the observed and theoretical limit, this returns the constrained limits by finding when the two interpolated lines intersect
    Sometimes it fails to converge, use display = True to display the graph of the intersections its found
    """

    # carry out cubic interpolation of observed and theoretical points
    f_obs = interp1d(x_obs, y_obs, kind="cubic")
    f_theory = interp1d(x_theory, y_theory, kind="cubic")

    def difference(x):
        return f_theory(x) - f_obs(x)

    limit = fsolve(difference, starting_points)

    if show_results:
        # print(limit[0], r'$ < \kappa_{lambda} < $',limit[1])
        print("lower limit = ", limit[0])
        print("upper limit = ", limit[1])

    if display:
        xi = np.linspace(-20, 20, 1000)
        fig = plt.figure(figsize=(10, 10))

        ax1 = plt.subplot(212)
        ax1.semilogy(x_obs, y_obs, "r+", label="observed data point")
        ax1.semilogy(xi, f_obs(xi), "k--", label="observed interpolation")
        ax1.semilogy(x_theory, y_theory, "bx", label="theory data point")
        ax1.semilogy(xi, f_theory(xi), "g-.", label="theory interpolation")
        # ax1.set_xlim(limit[0]*0.9, limit[0]*1.1)

        ax2 = plt.subplot(221)
        ax2.semilogy(x_obs, y_obs, "r+", label="observed data point")
        ax2.semilogy(xi, f_obs(xi), "k--", label="observed interpolation")
        ax2.semilogy(x_theory, y_theory, "bx", label="theory data point")
        ax2.semilogy(xi, f_theory(xi), "g-.", label="theory interpolation")
        ax2.set_xlim(limit[0] * 0.9, limit[0] * 1.1)
        ax2.set_ylim(f_theory(limit[0] * 0.9), f_theory(limit[0] * 1.1))

        ax3 = plt.subplot(222)
        ax3.semilogy(x_obs, y_obs, "r+", label="observed data point")
        ax3.semilogy(xi, f_obs(xi), "k--", label="observed interpolation")
        ax3.semilogy(x_theory, y_theory, "bx", label="theory data point")
        ax3.semilogy(xi, f_theory(xi), "g-.", label="theory interpolation")
        ax3.set_xlim(limit[1] * 0.9, limit[1] * 1.1)
        ax3.set_ylim(f_theory(limit[1] * 0.9), f_theory(limit[1] * 1.1))

        # plt.legend()

        plt.show()

    return limit


def loadTheoryCurve(path="/data/theorycurvedigitized1.csv"):

    df = pd.read_csv(path)

    return df


def loadLimit_2016(path="/data/hh4bexpected.csv"):
    df = pd.read_csv(path)
    return df


def getNorm(df, k, verbose=False, weight_column=None):
    """
    Return the 2b -> 4b normalization in a specified kinematic region

    Inputs:
    - df: pandas df for data events
    - k: The kinematic region to calculate this normalization in
         * 2: SB
         * 1: CR
    - verbose: Prints out the value of 4b, 2b and ratio. Return type includes 4b and 2b values

    """

    four_b = np.sum((df.kinematic_region == k) & (df.ntag >= 4))

    if weight_column == None:
        two_b = np.sum((df.kinematic_region == k) & (df.ntag == 2))

    else:
        two_b = df.loc[
            ((df.kinematic_region == k) & (df.ntag == 2)), weight_column
        ].sum()

    N4bTo2b = four_b / two_b

    if verbose:
        print("4b: ", four_b)
        print("2b: ", two_b)
        print("4b/2b", N4bTo2b)

        return N4bTo2b, four_b, two_b

    else:
        return N4bTo2b


def muQCD(df, k, verbose=False, weights=None):
    """
    Return the 2b -> 4b normalization in a specified kinematic region

    Inputs:
    - df: pandas df for data events
    - k: The kinematic region to calculate this normalization in
         * 2: SB
         * 1: CR
    - verbose: Prints out the value of 4b, 2b and ratio. Return type includes 4b and 2b values

    """

    four_b = np.sum((df.kinematic_region == k) & (df.ntag >= 4))

    if weights is not None:
        two_b = np.sum(weights)

    else:
        two_b = np.sum((df.kinematic_region == k) & (df.ntag == 2))

    mu_QCD = four_b / two_b

    if verbose:
        print("4b: ", four_b)
        print("2b: ", two_b)
        print("4b/2b", N4bTo2b)

        return mu_QCD, four_b, two_b

    else:
        return mu_QCD


def bkgSR(df):
    """
    Returns the mask of the dataframe for background events in the signal region
    """

    mask = (df.kinematic_region == 0) & (df.ntag == 2)

    return mask


def sigSR(df):
    """
    Returns the mask of the dataframe for signal events in the signal region
    """

    mask = (df.kinematic_region == 0) & (df.ntag >= 4)

    return mask


def I(data):
    """
    This function is needed to fix a weird bug when inverting a trigger mask
    """
    i = pd.Series(np.ones(len(data), dtype=bool))
    return i


def run_pulls(h_obs, h_exp):
    """
    function requires un-normalized histograms in-order to work correctly
    """

    h_pull = np.zeros(len(h_exp))
    pull_err = np.zeros(len(h_exp))

    for i in range(0, len(h_exp)):

        nbObs = h_obs[i]
        nbExp = h_exp[i]

        # nbExpEr = np.sqrt(h_obs[i] + h_exp[i])
        nbExpEr = np.sqrt(h_exp[i])  # Only care about the error in the expected value

        if nbExp == 0 or nbObs == 0:
            h_pull[i] = 0
            pull_err[i] = 0
            continue

        f1 = nbObs * np.log(
            (nbObs * (nbExp + nbExpEr ** 2)) / (nbExp ** 2 + nbObs * nbExpEr ** 2)
        )
        f2 = (nbExp ** 2 / nbExpEr ** 2) * np.log(
            1 + (nbExpEr ** 2 * (nbObs - nbExp)) / (nbExp * (nbExp + nbExpEr ** 2))
        )

        if nbObs < nbExp:
            pull = -np.sqrt(2 * (f1 - f2))
        else:
            pull = np.sqrt(2 * (f1 - f2))

        h_pull[i] = pull
        pull_err[i] = 0

    return h_pull


def run_pulls2(h_obs, h_exp, exp_error):
    """
    testing the new function to utilize normalized histograms
    """

    h_pull = np.zeros(len(h_exp))
    pull_err = np.zeros(len(h_exp))

    for i in range(0, len(h_exp)):

        nbObs = h_obs[i]
        nbExp = h_exp[i]

        # nbExpEr = np.sqrt(h_obs[i] + h_exp[i])
        nbExpEr = np.sqrt(h_exp[i])  # Only care about the error in the expected value

        if nbExp == 0 or nbObs == 0:
            h_pull[i] = 0
            pull_err[i] = 0
            continue

        f1 = nbObs * np.log(
            (nbObs * (nbExp + nbExpEr ** 2)) / (nbExp ** 2 + nbObs * nbExpEr ** 2)
        )
        f2 = (nbExp ** 2 / nbExpEr ** 2) * np.log(
            1 + (nbExpEr ** 2 * (nbObs - nbExp)) / (nbExp * (nbExp + nbExpEr ** 2))
        )

        if nbObs < nbExp:
            pull = -np.sqrt(2 * (f1 - f2))
        else:
            pull = np.sqrt(2 * (f1 - f2))

        h_pull[i] = pull
        pull_err[i] = 0

    return h_pull


def kl_divergence(p, q):
    return np.sum(np.where(p != 0, p * np.log(p / q), 0))


def chi2(h_obs, h_exp, nnof=True):
    eps = 1e-15
    chi_squared = np.sum(np.where(h_exp > 0, ((h_obs - h_exp) ** 2) / (h_exp + eps), 0))

    if nnof == True:
        chi_squared = chi_squared / len(h_exp)

    return chi_squared


def weighted_chisquare(
    f_obs, f_exp, f_obs_err, f_exp_err, ignore_stats=False, residuals=False, stat_lim=10
):
    from scipy.stats import chi2

    # Calculate weighted chi-square using method in arXiv:physics/0605123
    if ignore_stats == False:
        w1 = f_obs[(f_obs > stat_lim) | (f_exp > stat_lim)]
        w2 = f_exp[(f_obs > stat_lim) | (f_exp > stat_lim)]
        s1 = f_obs_err[(f_obs > stat_lim) | (f_exp > stat_lim)]  # noqa
        s2 = f_exp_err[(f_obs > stat_lim) | (f_exp > stat_lim)]  # noqa
    else:
        w1 = f_obs
        w2 = f_exp
        s1 = f_obs_err  # noqa
        s2 = f_exp_err  # noqa

    ndf = len(w1) - 1
    W1 = np.sum(w1)  # noqa
    W2 = np.sum(w2)  # noqa

    pi = (w1 * W1 / (s1 ** 2)) + (w2 * W2 / (s2 ** 2)) / (
        ((W1 ** 2) / (s1 ** 2)) + ((W2 ** 2) / (s2 ** 2))
    )

    R = (W1 * w2 - W2 * w1) ** 2 / (W1 ** 2 * s2 ** 2 + W2 ** 2 * s1 ** 2)
    R = np.where(((np.isnan(R)) | (np.isinf(R))), 0, R)
    # X2 = np.sum((W1*w2 - W2*w1)**2 / (W1**2 * s2**2 + W2**2 * s1**2))
    X2 = np.sum(R)
    p_value = chi2.sf(X2, ndf)
    if residuals:
        frac = 1 + ((W2 ** 2) * (s1 ** 2)) / ((W1 ** 2) / (s2 ** 2))
        top = w1 - W1 * pi
        bottom = s1 * np.sqrt(1 - 1 / frac)
        ri = top / bottom
        return (X2, p_value, ndf, ri)
    else:
        return (X2, p_value, ndf)


def chi2_singleWeight(f_obs, f_exp, f_obs_err):
    """
    Has not been verified yet.
    """
    from scipy.stats import chi2

    w1 = f_obs
    n1 = f_exp
    s1 = f_obs_err
    W = np.sum(w1)
    N = np.sum(n1)

    a = W * w1 - N * (s1 ** 2)
    b = (W * w1 - N * (s1 ** 2)) ** 2
    c = 4 * (W ** 2) * (s1 ** 2) * n1
    d = np.sqrt(b + c)
    top = a + d
    bottom = 2 * (W ** 2)
    pi = top / bottom

    f1 = np.sum(((n1 - N * pi) ** 2) / (N * pi))
    f2 = np.sum(((w1 - W * pi) ** 2) / (s1 ** 2))

    X2 = f1 + f2
    ndf = len(w1) - 1
    p = chi2.sf(X2, ndf)
    # print(X2)
    return (X2, p, ndf)


def weighted_histo_error(x, w, be):
    h_error = []
    for i in range(len(be) - 1):

        xi = np.all([be[i] > x, be[i + 1] < x], axis=0)
        h_error.append(np.sqrt(np.sum(xi ** 2)))
        """
        bin_error = 0
        for j in range(len(x)):
            if x[j] >= be[i] and x[j] < be[i+1]:
                bin_error += w[j]**2
        h_error.append(np.sqrt(bin_error))
        """
    return np.array(h_error)


def h2b_err(df, w, kr=2, feature="m_hh_cor", bins=50):

    R4b2b = muQCD(df, 2, weights=w)

    W = R4b2b * w

    feature_array = df[((df.ntag == 2) & (df.kinematic_region == kr))][feature].values

    h2b_w, be = np.histogram(feature_array, bins=bins, weights=W)

    h2berror = weighted_histo_error(feature_array, W, be)

    return h2berror


def dPhi(phi0, phi1):
    return np.pi - abs(abs(phi0 - phi1) - np.pi)


def dR_hh(phi1, phi2, eta1, eta2):
    return np.sqrt(dPhi(phi1, phi2) ** 2 + (eta1 - eta2) ** 2)


def calculatedRhh(*args):

    list_of_df = []
    for arg in args:
        list_of_df.append(arg)

    for df in list_of_df:
        df["dR_hh"] = dR_hh(df.phi_h1, df.phi_h2, df.eta_h1, df.eta_h2)


def calculatedPhi(*args):

    list_of_df = []
    for arg in args:
        list_of_df.append(arg)

    for df in list_of_df:
        df["dPhi_h1"] = dPhi(df.phi_h1_j1, df.phi_h1_j2)
        df["dPhi_h2"] = dPhi(df.phi_h2_j1, df.phi_h2_j2)


def significance(s, b):
    return np.sqrt(np.sum(s[b != 0] ** 2 / b[b != 0]))


def Lumi(year):
    L = 0
    if year == 2015 or year == 15:
        L = 3.2195
    elif year == 2016 or year == 16:
        L = 24.5556
    elif year == 2017 or year == 17:
        L = 44.3072
    elif year == 2018 or year == 18:
        L = 58.5
    return L


def chi2_unweighted(h1, h2, ignore_stats=False):

    if ignore_stats == False:
        # Cut events with an event yield lower than 25 as recommended by the paper
        H1 = h1[(h1 > 25) & (h2 > 25)]
        H2 = h2[(h1 > 25) & (h2 > 25)]
    elif ignore_stats == True:
        H1 = h1
        H2 = h2

    N = np.sum(H1)
    M = np.sum(H2)

    chi2 = (1 / (M * N)) * np.sum(((M * H1 - N * H2) ** 2) / (H2 + H1))

    ndf = len(H1) - 1

    return chi2 / ndf, ndf, stats.chi2.sf(chi2, ndf)


def splitMask(df):
    # Combination1:
    comb1 = (
        (df.tag_h1_j1 == True)
        & (df.tag_h1_j2 == False)
        & (df.tag_h2_j1 == True)
        & (df.tag_h2_j2 == False)
    )

    # Combination2:
    comb2 = (
        (df.tag_h1_j1 == True)
        & (df.tag_h1_j2 == False)
        & (df.tag_h2_j1 == False)
        & (df.tag_h2_j2 == True)
    )

    # Combination3:
    comb3 = (
        (df.tag_h1_j1 == False)
        & (df.tag_h1_j2 == True)
        & (df.tag_h2_j1 == True)
        & (df.tag_h2_j2 == False)
    )

    # Combination4:
    comb4 = (
        (df.tag_h1_j1 == False)
        & (df.tag_h1_j2 == True)
        & (df.tag_h2_j1 == False)
        & (df.tag_h2_j2 == True)
    )

    mask = comb1 | comb2 | comb3 | comb4

    return mask


def togetherMask(df):
    # comb1:
    combT1 = (
        (df.tag_h1_j1 == True)
        & (df.tag_h1_j2 == True)
        & (df.tag_h2_j1 == False)
        & (df.tag_h2_j2 == False)
    )

    # comb2:
    combT2 = (
        (df.tag_h1_j1 == False)
        & (df.tag_h1_j2 == False)
        & (df.tag_h2_j1 == True)
        & (df.tag_h2_j2 == True)
    )

    mask = combT1 | combT2

    return mask


def mixSplitTogether(dfS, dfT, f):
    """
    returns the mixed dataframe of 2b-Split with a certain level of 2b-Together impurities
    df = 100% 2b-Split + f * 2b-Together
    """
    dfT_sample = dfT.sample(frac=f)

    df = dfS.append(dfT_sample)

    df.reset_index(drop=True, inplace=True)
    return df


def IQR(x, axis=1):
    IQR = np.subtract(*np.percentile(x, [75, 25], axis=axis))
    return IQR


def plotHatch(edges, lower, upper, ax, label="Stat Error"):
    ax.fill_between(
        edges[0:2],
        lower[0:1],
        upper[0:1],
        hatch="\\\\\\\\\\\\",
        edgecolor="grey",
        linewidth=0.0,
        facecolor="None",
        step="pre",
        label=label,
        zorder=10,
    )

    for i in range(len(edges) - 1):
        ax.fill_between(
            edges[i : i + 2],
            lower[i : i + 1],
            upper[i : i + 1],
            hatch="\\\\\\\\\\\\",
            edgecolor="grey",
            linewidth=0.0,
            facecolor="None",
            step="pre",
            zorder=10,
        )

    return ax


def getKLbands(
    data,
    smnr,
    lambdaFile="eda_tools/data/mhh-reweighting_LambdaWeightFile.root",
    year=2017,
    data_weights="NN_weight_median_CR",
):

    # Using Luminosity = 1 (mc_sf folded into lambda weights)
    nt.getLambdaWeights(smnr, lambdaFile=lambdaFile)

    mu_max = 300
    mu_test_lambda = np.concatenate(
        (np.linspace(0.01, 0.95, 100), np.linspace(1, 300, 300))
    )
    xsec = 31.05
    Br = 0.3392
    L = Lumi(2017)
    fbtopb = 0.001

    sig_mask = nt.sigSR(smnr)

    lambdas = np.arange(-20, 21)

    normKL = np.array(
        [np.sum(smnr.loc[sig_mask, "w_lambda{}".format(l)]) for l in lambdas]
    )

    norm = fbtopb * xsec * normKL * L

    bands = {i: [] for i in range(-2, 3)}

    for l in tqdm(lambdas):
        hs, be = np.histogram(
            smnr.loc[sig_mask, "m_hh_cor"],
            bins=50,
            range=(250, 1400),
            weights=L * smnr.loc[sig_mask, "w_lambda{}".format(l)].values,
        )

        hb, _ = nt.bkgHist(
            data, bins=be, hrange=(250, 1400), weight_column=data_weights
        )

        hs, hb = nt.hackHistos2(hs, hb)

        for i, bi in zip(range(-2, 3), pm.getExpectedBand(hs, hb, mu_test_lambda)):
            bands[i].append(bi)

    return bands, norm


def normKL(
    smnr,
    lambdaFile="eda_tools/data/mhh-reweighting_LambdaWeightFile.root",
    lambdas=np.arange(-20, 21),
    xsec=31.05,
    fbtopb=0.001,
    L=Lumi(2017),
):

    nt.getLambdaWeights(smnr, lambdaFile=lambdaFile)

    sigMask = nt.sigSR(smnr)

    normKL = np.array(
        [np.sum(smnr.loc[sigMask, "w_lambda{}".format(l)]) for l in lambdas]
    )

    norm = fbtopb * xsec * L * normKL

    return norm


def plotKLscan(norms, bands, theoryFile="eda_tools/data/theorycurvedigitized1.csv"):

    fig = plt.figure(figsize=(12, 8))
    gs = gridspec.GridSpec(4, 1)
    ax = fig.add_subplot(gs[:, :])

    lambdas = np.arange(-21, 20)
    theoryKL = pd.read_csv(theoryFile)

    ax.semilogy(
        lambdas,
        (norms * np.array(bands[0])),
        color="blue",
        lw=2,
        ls="--",
        label="Expected Limit",
    )

    ax.fill_between(
        lambdas,
        norms * np.array(bands[-2]),
        norms * np.array(bands[2]),
        alpha=0.5,
        facecolor="yellow",
    )
    ax.fill_between(
        lambdas,
        norms * np.array(bands[-1]),
        norms * np.array(bands[1]),
        alpha=0.5,
        facecolor="lime",
    )

    # ax.semilogy(lambdas, norms, lw = 2, ls = '-.', label ='reweighting curve')
    ax.semilogy(theoryKL.x, theoryKL.y, lw=2, color="C4", label="Theory curve")
    ax.axvline(x=1, color="k", ls="-")

    ax.set_ylabel(
        r"95% upper limit on $\sigma_{ggF}$ $(pp \rightarrow HH)$ [pb]", fontsize=18
    )
    ax.set_xlabel("$\kappa_\lambda$ = $\lambda_{HHH}$ / $\lambda_{SM}$", fontsize=18)
    ax.legend(loc="lower left", ncol=1, fontsize=16)
    plt.grid(True, which="both")
    ax.tick_params(axis="both", which="major", labelsize=18)
    plt.tight_layout()


def plotKLband(lambdas, bands):

    fig = plt.figure(figsize=(12, 8))
    gs = gridspec.GridSpec(4, 1)
    ax = fig.add_subplot(gs[:, :])

    ax.semilogy(lambdas, bands[0])
    ax.fill_between(
        lambdas, np.array(bands[-1]), np.array(bands[1]), alpha=0.5, facecolor="lime"
    )
    ax.fill_between(
        lambdas, np.array(bands[-2]), np.array(bands[2]), alpha=0.5, facecolor="yellow"
    )


x_label_dict = {
    "pT_1": "HC Jet 1 p_{T} [GeV]",
    "pT_2": "HC Jet 2 p_{T} [GeV]",
    "pT_3": "HC Jet 3 p_{T} [GeV]",
    "pT_4": "HC Jet 4 p_{T} [GeV]",
    "eta_1": "HC Jet 1 #eta",
    "eta_2": "HC Jet 2 #eta",
    "eta_3": "HC Jet 3 #eta",
    "eta_4": "HC Jet 4 #eta",
    "logpT_4": "HC Jet 4 log(p_{T})",
    "logpT_2": "HC Jet 2 log(p_{T})",
    "eta_i": "<|HC #eta|>",
    "dRjj_1": "#Delta R_{jj} Close",
    "dRjj_2": "#Delta R_{jj} Not Close",
    "njets": "Number of jets",
    "m_hh": "$m_{HH}$ [GeV]",
    "pt_hh": "HH p_{T} [GeV]",
    "logpt_hh": "HH log(p_{T})",
    "m_h1": "m_{H}^{lead} [GeV]",
    "m_h2": "m_{H}^{subl} [GeV]",
    "X_wt": "X_{Wt}",
    "pT_h1": "Leading HC p_{T} [GeV]",
    "pT_h2": "Subleading HC p_{T} [GeV]",
    "dEta_hh": "#Delta #eta_{HH}",
    "dR_hh": "#Delta R_{HH}",
    "dPhi_h1": "Lead HC #Delta #phi_{jj}",
    "dPhi_h2": "Sublead HC #Delta #phi_{jj}",
    "pairing_score_1": "BDT Pairing Score 1",
    "pairing_score_2": "BDT Pairing Score 2",
    "m_hh_cor": "$m_{HH}$ (corrected) [GeV]",
}
