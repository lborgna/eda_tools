from __future__ import print_function
import sys, os
sys.path.append(os.path.dirname(__file__))


import utilities as u
import trigger_utilities as tu
import histogram_helper as hh
import nicole_tools as nt
import numpy as np
import operator as SmoothOperator
import pandas as pd

def offlineMask(Bucket, df, data_size = None):
    """Function generates the offline variable mask for a spec

    Args:
        Bucket (Dict): Dict containing the specifics of the buckets (offVar, offVarCut and operator)
        df (pd.DataFrame): DataFrame that the offline mask is to be determined on. 
        data_size (int): size of mask to generate.

    Returns:
        mask (pd.Series): boolean mask indicating if the event passed offline variable cut for bucket.
    """
    if data_size == None:
        data_size = len(df)
    
    mask = pd.Series(np.ones(data_size, dtype = bool)) #Needs to be a pd.Series
    for op, offVar, offVarCut in zip(Bucket['operator'], Bucket['offVar'], Bucket['offVarCut']):
        mask = mask & op(df[offVar], offVarCut)
    return mask

def passedTriggerMask(df, trigger):
    """DEPRECATED: Function returns boolean mask for events passing the specific trigger
    (this function assumes that the triggers in each event is stored as a dictionary.)

    Args:
        df (pd.DataFrame): pandas dataframe containing events.
        trigger (string): trigger to be checked

    Returns:
        (pd.Series): boolean mask indicating events passing trigger.
    """
    return df['triggerMap'].apply(lambda x: trigger in x.keys())

def assignBucket(df, configDict, verbose = False, category_name = 'Bucket'):
    """
    configDict should contain the configuration information for how the buckets should be assigned
    
    #Bucket1 offline mask = lead_jet_pT > 300 AND lead_jet_tag == 1
        #Bucket1 = Bucket1 offline mask AND Bucket1 trigger Mask
    
    #Bucket2 offline mask = NOT bucket1 offline mask and lead_jet_pt > 200 and lead_jet_tag == 0
        # Bucket 2 = Bucket2 offline mask AND Bucket2 trigger Mask
    
    #Bucket3 offline mask = NOT bucket1 offline mask and NOT bucket2 offline mask and HT > 600
        # Bucket 3 = Bucket3 offline mask AND Bucket3 trigger Mask
        
    #Bucket4 offline mask = NOT bucket1 offline mask and NOT bucket2 offline mask and NOT bucket 3 offline mask and  bucket4 trigger Mask == True
        #Bucket 4 
    """
    
    data_size = len(df)
    
    B1_offline_mask = offlineMask(configDict['Bucket1'], df, data_size)    
    B1_trig_mask = passedTriggerMask(df, configDict['Bucket1']['trigger'])
    B1_mask = B1_offline_mask & B1_trig_mask
    
    B2_offline_mask = offlineMask(configDict['Bucket2'], df, data_size)    
    B2_trig_mask = passedTriggerMask(df, configDict['Bucket2']['trigger'])
    B2_mask = ~B1_offline_mask & B2_offline_mask & B2_trig_mask

    B3_offline_mask = offlineMask(configDict['Bucket3'], df, data_size)    
    B3_trig_mask = passedTriggerMask(df, configDict['Bucket3']['trigger'])
    B3_mask = ~B1_offline_mask & ~B2_offline_mask & B3_offline_mask & B3_trig_mask
    
    B4_offline_mask = offlineMask(configDict['Bucket4'], df, data_size)    
    B4_trig_mask = passedTriggerMask(df, configDict['Bucket4']['trigger'])
    B4_mask = ~B1_offline_mask & ~B2_offline_mask & ~B3_offline_mask & B4_offline_mask & B4_trig_mask

    df[category_name] = 0
    df.loc[B1_mask, category_name] = 1
    df.loc[B2_mask, category_name] = 2
    df.loc[B3_mask, category_name] = 3
    df.loc[B4_mask, category_name] = 4
    
    if verbose:
        print('-------------offline mask-------------')
        print("B1off", B1_offline_mask.value_counts())
        print("B2off", B3_offline_mask.value_counts())
        print("B3off", B3_offline_mask.value_counts())
        print("B4off", B4_offline_mask.value_counts())

        print('-------------Trigger mask-------------')
        print("B1T", B1_trig_mask.value_counts())
        print("B2T", B3_trig_mask.value_counts())
        print("B3T", B3_trig_mask.value_counts())
        print("B4T", B4_trig_mask.value_counts())
        

        print('-------------Final mask-------------')
        print("B1: ", B1_mask.value_counts())
        print("B2: ", B2_mask.value_counts())
        print("B3: ", B3_mask.value_counts())
        print("B4: ", B4_mask.value_counts())

    return df


def assignBucketNew(df, configDict, verbose = False, category_name = 'Bucket'):
    """
    configDict should contain the configuration information for how the buckets should be assigned
    
    #Bucket1 offline mask = lead_jet_pT > 300 AND lead_jet_tag == 1
        #Bucket1 = Bucket1 offline mask AND Bucket1 trigger Mask
    
    #Bucket2 offline mask = NOT bucket1 offline mask and lead_jet_pt > 200 and lead_jet_tag == 0
        # Bucket 2 = Bucket2 offline mask AND Bucket2 trigger Mask
    
    #Bucket3 offline mask = NOT bucket1 offline mask and NOT bucket2 offline mask and HT > 600
        # Bucket 3 = Bucket3 offline mask AND Bucket3 trigger Mask
        
    #Bucket4 offline mask = NOT bucket1 offline mask and NOT bucket2 offline mask and NOT bucket 3 offline mask and  bucket4 trigger Mask == True
        #Bucket 4 
    """
    
    data_size = len(df)
    
    B1_offline_mask = offlineMask(configDict['Bucket1'], df, data_size)    
    B1_trig_mask = tu.passedTriggersMask(df, configDict['Bucket1']['trigger'])
    B1_mask = B1_offline_mask & B1_trig_mask
    
    B2_offline_mask = offlineMask(configDict['Bucket2'], df, data_size)    
    B2_trig_mask = tu.passedTriggersMask(df, configDict['Bucket2']['trigger'])
    B2_mask = ~B1_offline_mask & B2_offline_mask & B2_trig_mask

    B3_offline_mask = offlineMask(configDict['Bucket3'], df, data_size)    
    B3_trig_mask = tu.passedTriggersMask(df, configDict['Bucket3']['trigger'])
    B3_mask = ~B1_offline_mask & ~B2_offline_mask & B3_offline_mask & B3_trig_mask
    
    B4_offline_mask = offlineMask(configDict['Bucket4'], df, data_size)    
    B4_trig_mask = tu.passedTriggersMask(df, configDict['Bucket4']['trigger'])
    B4_mask = ~B1_offline_mask & ~B2_offline_mask & ~B3_offline_mask & B4_offline_mask & B4_trig_mask

    df[category_name] = 0
    df.loc[B1_mask, category_name] = 1
    df.loc[B2_mask, category_name] = 2
    df.loc[B3_mask, category_name] = 3
    df.loc[B4_mask, category_name] = 4
    
    if verbose:
        print('-------------offline mask-------------')
        print("B1off", B1_offline_mask.value_counts())
        print("B2off", B3_offline_mask.value_counts())
        print("B3off", B3_offline_mask.value_counts())
        print("B4off", B4_offline_mask.value_counts())

        print('-------------Trigger mask-------------')
        print("B1T", B1_trig_mask.value_counts())
        print("B2T", B3_trig_mask.value_counts())
        print("B3T", B3_trig_mask.value_counts())
        print("B4T", B4_trig_mask.value_counts())
        

        print('-------------Final mask-------------')
        print("B1: ", B1_mask.value_counts())
        print("B2: ", B2_mask.value_counts())
        print("B3: ", B3_mask.value_counts())
        print("B4: ", B4_mask.value_counts())

    return df

def BucketConfigTemplate():
    """Function returns a dictionary example to assign a dataset to a buckets
        offVar: Offline variables
        offVarCut: Offline variables cuts/ thresholds
        operator: operation to apply (offVar -> operator -> offVarCut)
        trigger: Trigger that the bucket needs to pass.
    Returns:
        dictionary: Buckets configuration
    """
    BucketsDict = { "Bucket1" : {"offVar": ["lead_jet_pT", "lead_jet_tag"],
                             "offVarCut": [300, 1],
                             "operator": [SmoothOperator.gt, SmoothOperator.eq],
                             "trigger": "HLT_j225_gsc300_bmv2c1070_split"}, 
                "Bucket2" : {"offVar": ["lead_jet_pT", "lead_jet_tag"],
                             "offVarCut":[200, 0],
                             "operator":[SmoothOperator.gt, SmoothOperator.eq],
                             "trigger": "HLT_j110_gsc150_boffperf_split_2j35_gsc55_bmv2c1070_split_L1J85_3J30"},
                "Bucket3" : {"offVar": ["HT"],
                             "offVarCut": [600],
                             "operator" : [SmoothOperator.gt],
                             "trigger": "HLT_2j35_gsc55_bmv2c1050_split_ht300_L1HT190-J15s5.ETA21"},
                "Bucket4" : {"offVar": [],
                             "offVarCut": [],
                             "operator": [],
                             "trigger": "HLT_2j15_gsc35_bmv2c1040_split_2j15_gsc35_boffperf_split_L14J15.0ETA25"},
    
    }

    return BucketsDict

def BucketConfigTemplateNew(cut1=300, cut2=200, cut3=600, t1=10, t2=7, t3=9, t4=8):
    """Function returns a dictionary example to assign a dataset to a buckets
        offVar: Offline variables
        offVarCut: Offline variables cuts/ thresholds
        operator: operation to apply (offVar -> operator -> offVarCut)
        trigger: Trigger that the bucket needs to pass.
    Returns:
        dictionary: Buckets configuration
    """
    BucketsDict = { "Bucket1" : {"offVar": ["lead_jet_pT", "lead_jet_tag"],
                             "offVarCut": [cut1, 1],
                             "operator": [SmoothOperator.gt, SmoothOperator.eq],
                             "trigger": [t1]}, 
                "Bucket2" : {"offVar": ["lead_jet_pT", "lead_jet_tag"],
                             "offVarCut":[cut2, 0],
                             "operator":[SmoothOperator.gt, SmoothOperator.eq],
                             "trigger": [t2]},
                "Bucket3" : {"offVar": ["HT"],
                             "offVarCut": [cut3],
                             "operator" : [SmoothOperator.gt],
                             "trigger": [t3]},
                "Bucket4" : {"offVar": [],
                             "offVarCut": [],
                             "operator": [],
                             "trigger": [t4]},
    
    }

    return BucketsDict



def getBucketsLimit(df_sig, df_bkg, BucketsDict, bins = 50, verbose = False, hrange = (200, 1600), mus = None, bucket_column = 'Bucket'):
    
    df_sig = assignBucket(df_sig, BucketsDict, category_name=bucket_column)
    df_bkg = assignBucket(df_bkg, BucketsDict, category_name=bucket_column)
    
    b1_s, be = sigHist(df_sig[df_sig[bucket_column]==1], bins = bins)
    b2_s, _  = sigHist(df_sig[df_sig[bucket_column]==2], bins = be)
    b3_s, _  = sigHist(df_sig[df_sig[bucket_column]==3], bins = be)
    b4_s, _  = sigHist(df_sig[df_sig[bucket_column]==4], bins = be)
    
    b1_b, _  = bkgHist(df_bkg[df_bkg[bucket_column]==1], bins = be)
    b2_b, _  = bkgHist(df_bkg[df_bkg[bucket_column]==2], bins = be)
    b3_b, _  = bkgHist(df_bkg[df_bkg[bucket_column]==3], bins = be)
    b4_b, _  = bkgHist(df_bkg[df_bkg[bucket_column]==4], bins = be)
    
    
    #Hacking the histograms
    b1_s, b1_b = hackHistos2(b1_s, b1_b)
    b2_s, b2_b = hackHistos2(b2_s, b2_b)
    b3_s, b3_b = hackHistos2(b3_s, b3_b)
    b4_s, b4_b = hackHistos2(b4_s, b4_b)
    
    if mus is None:
        mus = np.linspace(1, 50, 200)
    
    
    band = pm.get4channelband(b1_s, b1_b, b2_s, b2_b, b3_s, b3_b, b4_s, b4_b, mus)
    
    resultsDict = {'band': band,
                   'b1_s': b1_s,
                   'b1_b': b1_b,
                   'b2_s': b2_s,
                   'b2_b': b2_b,
                   'b3_s': b3_s,
                   'b3_b': b3_b,
                   'b4_s': b4_s,
                   'b4_b': b4_b}
    
    if verbose:
        hs = b1_s + b2_s + b3_s + b4_s
        hb = b1_b + b2_b + b3_b + b4_b

        bandBgrouped = pm.getExpectedBand(hs, hb, mus)
        bandB1 = pm.getExpectedBand(b1_s, b1_b, mus)
        bandB2 = pm.getExpectedBand(b2_s, b2_b, mus)
        bandB3 = pm.getExpectedBand(b3_s, b3_b, mus)
        bandB4 = pm.getExpectedBand(b4_s, b4_b, mus)
        
        veboseDict = {'grouped': bandBgrouped, 
                   'bandB1': bandB1,
                   'bandB2': bandB2,
                   'bandB3': bandB3,
                   'bandB4': bandB4}
        resultsDict.update(veboseDict)
    
    
 
    
    
    return resultsDict

def bucketsPercentages(df, bucket_column = 'Bucket', 
                       weight_column = 'mc_sf'):
    """Function returns the percentages of events caught by each bucket in the dataset
    
    Args:
        df (pd.DataFrame): DataFrame of events
        bucket_column (str, optional): Column name of buckets categorization. Defaults to 'Bucket'.
        weight_column (str, optional): event weight column to use. Defaults to 'mc_sf'.

    Returns:
        [list]: Percentages of events caught by each bucket.
    """

    categories = list(set(df[bucket_column].values))
    bucketsP = [ 100 * df[df[bucket_column] == i][weight_column].sum() / df[weight_column].sum() for i in categories]

    return bucketsP

def bucketsSignificances(df_s, df_b, bins = 50, hrange=(250,1400), bucket_column = 'Bucket',
                         signal_weights = 'mc_sf',
                         data_weights ='NN_d24_weights',
                         year = 2017):

    categories = list(set(df_s[bucket_column].values))
    bucketsSig = []
    for i in categories:
        hs, be = nt.sigHist(df_s[df_s[bucket_column] == i], bins = bins, hrange = hrange)
        hb, _  = nt.bkgHist(df_b[df_b[bucket_column] == i], bins = be, hrange = hrange,
                            weight_column=data_weights)
        #hs, hb = nt.hackHistos2(hs, hb)
        sig = u.significance(hs, hb)
        bucketsSig.append(sig)

    return bucketsSig

