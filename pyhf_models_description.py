from __future__ import print_function
import sys, os
sys.path.append(os.path.dirname(__file__))

import pyhf
#This trick is needed since the hypotest got moved to a different member in newer versions
versionNum = int(pyhf.__version__.split('.')[1])
if versionNum > 1:
    from pyhf.infer import hypotest
else:
    from pyhf.utils import hypotest

"""
This file is meant to describe the .json formatted pyhf models to obtain limits
"""


def simple(signal_data,bkg_data,batch_size=None):
    spec = {
        'channels':[
            {
                'name': 'singlechannel',
                'samples' : [
                    {
                        'name': 'signal',
                        'data': signal_data,
                        'modifiers': [
                            {'name': 'mu', 'type': 'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data':bkg_data,
                        'modifiers':[
                        ],
                    },
                ],
            }
        ]
    }
    return pyhf.Model(spec, batch_size=batch_size)


def twochannel(sig1, bkg1, sig2, bkg2, batch_size = None):
    spec = {
        'channels':[
            {
                'name': 'firstchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': sig1,
                        'modifiers': [
                            {'name': 'mu', 'type': 'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': bkg1,
                        'modifiers':[
                        ],
                    },
                ],
            },
            {
                'name': 'secondchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': sig2,
                        'modifiers': [
                            {'name': 'mu', 'type': 'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': bkg2,
                        'modifiers':[
                        ],
                    },
                ],
            }
        ]
    }
    return pyhf.Model(spec, batch_size = batch_size)

def twochannel(sig1, bkg1, sig2, bkg2, batch_size = None):
    spec = {
        'channels':[
            {
                'name': 'firstchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': sig1,
                        'modifiers': [
                            {'name': 'mu', 'type': 'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': bkg1,
                        'modifiers':[
                        ],
                    },
                ],
            },
            {
                'name': 'secondchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': sig2,
                        'modifiers': [
                            {'name': 'mu', 'type': 'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': bkg2,
                        'modifiers':[
                        ],
                    },
                ],
            }
        ]
    }
    return pyhf.Model(spec, batch_size = batch_size)

def bkgSyst2channelHT(s1, b1, v1, s2, b2, v2, batch_size = None):

    spec = {
        'channels':[
            {
                'name': 'firstchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': s1,
                        'modifiers' : [
                            {'name': 'mu', 'type': 'normfactor', 'data':None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': b1,
                        'modifiers': [
                            {'name': 'low_HT', 'type':'histosys', 'data':{'lo_data':v1['Low_HT'][0], "hi_data":v1['Low_HT'][1]}},
                            {'name': 'high_HT', 'type':'histosys', 'data':{'lo_data':v1['High_HT'][0], 'hi_data':v1['High_HT'][1]}}
                        ],
                    },
                ],
            },
            {
                'name': 'secondchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': s2,
                        'modifiers' : [
                            {'name': 'mu', 'type':'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': b2,
                        'modifiers': [
                            {'name': 'low_HT', 'type':'histosys', 'data':{'lo_data':v2['Low_HT'][0], 'hi_data':v2['Low_HT'][1]}},
                            {'name': 'high_HT', 'type':'histosys', 'data':{'lo_data':v2['High_HT'][0], 'hi_data':v2['High_HT'][1]}}
                        ],
                    },
                ],
            },
        ],
    }
    return pyhf.Model(spec, batch_size = batch_size)

def bkgSyst2channel(s1, b1, v1, s2, b2, v2, batch_size = None):

    spec = {
        'channels':[
            {
                'name': 'firstchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': s1,
                        'modifiers' : [
                            {'name': 'mu', 'type': 'normfactor', 'data':None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': b1,
                        'modifiers': [
                            {'name': 'shape_syst', 'type':'histosys', 'data':{'lo_data':v1['var'][0], "hi_data":v1['var'][1]}},
                        ],
                    },
                ],
            },
            {
                'name': 'secondchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': s2,
                        'modifiers' : [
                            {'name': 'mu', 'type':'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': b2,
                        'modifiers': [
                            {'name': 'shape_syst', 'type':'histosys', 'data':{'lo_data':v2['var'][0], 'hi_data':v2['var'][1]}},
                        ],
                    },
                ],
            }
        ]
    }
    return pyhf.Model(spec, batch_size = batch_size)

def bkgSyst3channelHT(s1, b1, v1, s2, b2, v2, s3, b3, v3, batch_size = None):

    spec = {
        'channels':[
            {
                'name': 'firstchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': s1,
                        'modifiers' : [
                            {'name': 'mu', 'type': 'normfactor', 'data':None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': b1,
                        'modifiers': [
                            {'name': 'low_HT', 'type':'histosys', 'data':{'lo_data':v1['Low_HT'][0], "hi_data":v1['Low_HT'][1]}},
                            {'name': 'high_HT', 'type':'histosys', 'data':{'lo_data':v1['High_HT'][0], 'hi_data':v1['High_HT'][1]}}
                        ],
                    },
                ],
            },
            {
                'name': 'secondchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': s2,
                        'modifiers' : [
                            {'name': 'mu', 'type':'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': b2,
                        'modifiers': [
                            {'name': 'low_HT', 'type':'histosys', 'data':{'lo_data':v2['Low_HT'][0], 'hi_data':v2['Low_HT'][1]}},
                            {'name': 'high_HT', 'type':'histosys', 'data':{'lo_data':v2['High_HT'][0], 'hi_data':v2['High_HT'][1]}}
                        ],
                    },
                ],
            },
            {
                'name': 'thirdchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': s3,
                        'modifiers' : [
                            {'name': 'mu', 'type':'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': b3,
                        'modifiers' : [
                            {'name': 'low_HT', 'type':'histosys', 'data':{'lo_data': v3['Low_HT'][0], 'hi_data':v3['Low_HT'][1]}},
                            {'name': 'high_HT', 'type':'histosys', 'data':{'lo_data': v3['High_HT'][0], 'hi_data':v3['High_HT'][1]}}
                        ],
                    },
                ],
            },
        ],
    }
    return pyhf.Model(spec, batch_size = batch_size)

def bkgSyst4channelHT(s1, b1, v1, s2, b2, v2, s3, b3, v3, s4, b4, v4, batch_size = None):

    spec = {
        'channels':[
            {
                'name': 'firstchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': s1,
                        'modifiers' : [
                            {'name': 'mu', 'type': 'normfactor', 'data':None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': b1,
                        'modifiers': [
                            {'name': 'low_HT', 'type':'histosys', 'data':{'lo_data':v1['Low_HT'][0], "hi_data":v1['Low_HT'][1]}},
                            {'name': 'high_HT', 'type':'histosys', 'data':{'lo_data':v1['High_HT'][0], 'hi_data':v1['High_HT'][1]}}
                        ],
                    },
                ],
            },
            {
                'name': 'secondchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': s2,
                        'modifiers' : [
                            {'name': 'mu', 'type':'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': b2,
                        'modifiers': [
                            {'name': 'low_HT', 'type':'histosys', 'data':{'lo_data':v2['Low_HT'][0], 'hi_data':v2['Low_HT'][1]}},
                            {'name': 'high_HT', 'type':'histosys', 'data':{'lo_data':v2['High_HT'][0], 'hi_data':v2['High_HT'][1]}}
                        ],
                    },
                ],
            },
            {
                'name': 'thirdchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': s3,
                        'modifiers' : [
                            {'name': 'mu', 'type':'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': b3,
                        'modifiers' : [
                            {'name': 'low_HT', 'type':'histosys', 'data':{'lo_data': v3['Low_HT'][0], 'hi_data':v3['Low_HT'][1]}},
                            {'name': 'high_HT', 'type':'histosys', 'data':{'lo_data': v3['High_HT'][0], 'hi_data':v3['High_HT'][1]}}
                        ],
                    },
                ],
            },
            {
                'name': 'fourthchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': s4,
                        'modifiers' : [
                            {'name': 'mu', 'type': 'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': b4,
                        'modifiers': [
                            {'name': 'low_HT', 'type': 'histosys', 'data':{'lo_data': v4['Low_HT'][0], 'hi_data': v4['Low_HT'][1]}},
                            {'name': 'high_HT', 'type': 'histosys', 'data':{'lo_data': v4['High_HT'][0], 'hi_data': v4['High_HT'][1]}}
                        ],
                    },
                ],
            },
        ],
    }
    return pyhf.Model(spec, batch_size = batch_size)   


def fourchannel(sig1, bkg1, sig2, bkg2, sig3, bkg3, sig4, bkg4, batch_size = None):
    spec = {
        'channels':[
            {
                'name': 'firstchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': sig1,
                        'modifiers': [
                            {'name': 'mu', 'type': 'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': bkg1,
                        'modifiers':[
                        ],
                    },
                ],
            },
            {
                'name': 'secondchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': sig2,
                        'modifiers': [
                            {'name': 'mu', 'type': 'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': bkg2,
                        'modifiers':[
                        ],
                    },
                ],
            },
            {
                'name': 'thirdchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': sig3,
                        'modifiers': [
                            {'name': 'mu', 'type': 'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': bkg3,
                        'modifiers':[
                        ],
                    },
                ],
            },
            {
                'name': 'fourthchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': sig4,
                        'modifiers': [
                            {'name': 'mu', 'type': 'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': bkg4,
                        'modifiers':[
                        ],
                    },
                ],
            }
        ]
    }
    return pyhf.Model(spec, batch_size = batch_size)

def bkgSyst(signal_data,bkg_data,var,batch_size=None):
    '''

    '''

    spec = {
        'channels':[
            {
                'name': 'singlechannel',
                'samples' : [
                    {
                        'name': 'signal',
                        'data': signal_data,
                        'modifiers': [
                            {'name': 'mu', 'type': 'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data':bkg_data,
                        'modifiers':[
                            {'name': 'low_HT',  'type':'histosys', "data":{"lo_data":var['Low_HT'][0], "hi_data":var['Low_HT'][1]}},
                            {'name': 'high_HT', 'type':'histosys', "data":{"lo_data":var['High_HT'][0],"hi_data":var['High_HT'][1]}}
                        ],
                    },
                ],
            }
        ]
    }
    return pyhf.Model(spec, batch_size=batch_size)