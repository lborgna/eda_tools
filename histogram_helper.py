from __future__ import print_function
import sys, os

sys.path.append(os.path.dirname(__file__))

import numpy as np
from scipy import interpolate


def poissonian_error(k, n):

    yerr = (1 / n) * np.sqrt(k)

    return yerr


def binomial_error(k, n):
    """
    Determines the vertical (y) / count error when dividing two histograms that are correlated. For example when a
    histogram is a subset of the original histogram. This is done via binomial error propagation and has the formula
    yerr = (1/n) * sqrt(k * ( 1 - (k/n))). Where n is the number of total events in a given bin, and k is the events
    passing the selection in that same bin. This formula is an approximation as it uses the estimated efficiency.
    :param k: number of events passing the selection in a given bin
    :param n: number of events in a given bin
    :return: yerr: array of the absolute errors for each bin in the y direction.
    """
    # determines the error in efficiency through binomial error propgation
    yerr = (1 / n) * np.sqrt(k * (1 - (k / n)))
    return yerr


def bayesian_error(k, n):

    V = (((k + 1) * (k + 2)) / ((n + 2) * (n + 3))) - (((k + 1) ** 2) / ((n + 2) ** 2))
    yerr = np.sqrt(V)
    return yerr


def bin_width_error(bin_edges):
    """
    Determines the horizontal (x) error of a bin  by calculating half the bin size
    :param bin_edges:
    :return: xerr array containing the absolute magnitude of the error in x
    """
    # determines the error in a bin by +/- half of the bin_width
    xerr = []
    for k in range(len(bin_edges)):
        if k != (len(bin_edges) - 1):
            x1 = bin_edges[k]
            x2 = bin_edges[k + 1]
            bin_error = (x2 - x1) / 2
            xerr.append(bin_error)
    xerr = np.asarray(xerr)
    return xerr


def bin_midpoint(bin_edges):
    """
    Determines the midpoint of a bin (x) given a list of bin edges
    :param bin_edges: input the edges of the bins with length = number of bins + 1
    :return: x (np.array)
    """
    # determines the middle point of a bin from the bin_edges
    x = []
    for i in range(len(bin_edges)):
        if i != len(bin_edges) - 1:
            x1 = bin_edges[i]
            x2 = bin_edges[i + 1]
            bin_x = (x1 + x2) / 2
            x.append(bin_x)
    x = np.asarray(x)
    return x


def bin_width(bin_edges):
    return bin_edges[1:] - bin_edges[:-1]


def adaptive_binning(data, weights, threshold=20, bi=10, hrange=(250, 1250)):
    """
    Function that tries to determine the best binning for a given dataset. It starts by looking at the right hand side
    tail bin of an initial histogram with initial number of bins = bi, if a bin has counts which are lower than a threshold
    it will then merge with the next bin and so forth until all the bins have been checked.
    :param data: input data in array
    :param threshold: lowest number of counts/ frequency that you accept in your bins
    :param bi: initial number of bins to create the histogram (first guess)
    :return: returns the adjusted histogram and the new bin edges
    """
    hist, bin_edges = np.histogram(data, bins=bi, weights=weights, range=hrange)

    for i, e in reversed(list(enumerate(hist))):
        if i > 0:  # check to not be in the last bin (from reverse)
            if hist[i] < threshold:
                bin_edges = np.delete(bin_edges, i)
                hist[i - 1] = hist[i] + hist[i - 1]

    return hist, bin_edges


def median_histogram(h, edges):

    x = bin_midpoint(edges)
    cf = np.zeros(len(x))
    count = 0

    for i, _ in enumerate(cf):
        count = count + h[i]
        cf[i] = count

    cp = 100 * cf / sum(h)

    d = interpolate.interp1d(cp, x)

    return d(50)


def mean_histogram(h, edges):
    x = bin_midpoint(edges)

    a = np.sum(x * h)
    b = np.sum(h)

    mean = a / b
    return mean
