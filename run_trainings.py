
import sys, os
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = ""
sys.path.append(os.path.dirname(__file__))
import argparse

import pandas as pd

import NN_RW as nn
import utilities as u

import numpy as np
from numpy import savez_compressed


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description= "please give me the correct inputs")
    parser.add_argument('-i', '--input-file', required = True, dest = 'input', type = str)
    parser.add_argument('-n', '--nruns', required = True, dest = 'nruns', type = int)
    parser.add_argument('-o', '--output-file', required = True, dest ='output', type = str)
    parser.add_argument('-e', '--epochs', required= True, dest='epochs', type = int)
    parser.add_argument('-b', '--batch', required = True, dest='batch', type = int)
    parser.add_argument('-d', '--storage_directory', required = True, dest = 'dir', type = str)
    parser.add_argument('-r', '--region', required = True, dest = 'region', type = int)
    arguments  = parser.parse_args()
    arguments = vars(arguments)

    df = pd.read_pickle(arguments['input'])
    u.calculatedRhh(df)
    u.calculatedPhi(df)
    nn.log_inputs(df, to_log = ['pT_2', 'pT_4', 'dRjj_1', 'dRjj_2','pt_hh','X_wt'])
    rw_cols_log = ['pT_2_log', 'pT_4_log', 'eta_i', 'dRjj_1_log','dRjj_2_log','njets','pt_hh_log','X_wt_log','dR_hh','dPhi_h1','dPhi_h2']

    nruns = arguments['nruns']
    fname = arguments['output']
    epochs = arguments['epochs']
    batch_size = arguments['batch']
    storage_directory = arguments['dir']
    kr = arguments['region']
    #filename = "/mnt/storage/lborgna/models/" + "NN_model_" + "n" + str(nruns) + "_" + "b" + str(batch_size) + ".h5"
    
    weights, _, _ = nn.repeatNNtrain(df, 
                                    repeat = nruns,
                                    kinematic_region= kr,
                                    epochs = epochs,
                                    sort_rw_cols= rw_cols_log,
                                    batch_size = batch_size,
                                    model_storage_directory= storage_directory)

    savez_compressed(fname, np.exp(weights))
