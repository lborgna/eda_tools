import sys, os
sys.path.append(os.path.dirname(__file__))

import uproot
import pandas as pd

import trigger_utilities as tu
import utilities as util
import numpy as np
import json
import copy

def DecodeTriggerMap(df):
    def decode_dict(row):
        decoded_dict = {}
        for k, v in row.items():
            decoded_dict[k.decode()] = v
        return decoded_dict
    
    return df['triggerMap'].apply(lambda row: decode_dict(row))



def NewFormatTriggerMap(file, mc = False):
    import awkward
    t = file['triggerList']

    first = t['triggerMap.first'].array()
    second = t['triggerMap.second'].array()

    triggerMap = awkward.ObjectArray(awkward.JaggedArray.zip(awkward.fromiter(first), second), lambda x: dict(zip(x.i0, x.i1)))

    df = pd.DataFrame(np.array(triggerMap), columns = ['triggerMap'])
    df.insert(1, 'eventNumber', t['eventNumber'].array(), allow_duplicates = True)

    if mc:
        df.insert(2, 'runNumber', t['rand_run_nr'].array(), allow_duplicates = True)
    else:
        df.insert(2, 'runNumber', t['runNumber'].array(), allow_duplicates = True)
    """
    if mc:
        df.insert(3, 'mcEventWeight', t['mcEventWeight'].array(), allow_duplicates = True)
    """
    return df

def create_triggers_to_remove(triggers_to_keep, full_triggerlist):

    triggers_to_remove = copy.deepcopy(full_triggerlist)

    for _, trig in enumerate(triggers_to_keep):
        triggers_to_remove.remove(trig)

    return triggers_to_remove

def strip_triggerMap(df, triggers_to_remove):
    #df_new = df
    for row in df['triggerMap']:
        for _, trigger in enumerate(triggers_to_remove):
            if trigger in row.keys():
                row.pop(trigger)

    return df

def loadNNT(file, mc = False, year = 2018, strip = True, triggers_to_keep = None, triggerlist = None):

    if triggerlist is None:
        if year == 2018:
            triggerlist = 'triggerlist/triggers2018.json'
        elif year == 2017:
            triggerlist = 'triggerlist/triggers2017.json'
    
    if (strip == True) and (triggers_to_keep is None):
        if year == 2018:
            triggers_to_keep = tu.read_json('triggerlist/nominal3_2018.json')
        if year == 2017:
            triggers_to_keep = tu.read_json('triggerlist/nominal3_2017.json')

    uproot_file = uproot.open(file)

    df_fmp = uproot_file['fullmassplane'].pandas.df()
    print('fullmassplane loaded')
    df_tM  = NewFormatTriggerMap(uproot_file, mc)
    print('triggerMap loaded')
    df = pd.merge(df_tM, df_fmp, how = "inner", left_on = ['eventNumber'], right_on = ['event_number'])
    print('Merged!')
    df['triggerMap'] = DecodeTriggerMap(df)
    print('Decoded')
    if strip:
        triggers = tu.read_json(triggerlist)
        triggers_to_remove = create_triggers_to_remove(triggers_to_keep, triggers)
        df = strip_triggerMap(df, triggers_to_remove)
        print('stripped!')
    return df

