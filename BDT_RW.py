from __future__ import print_function
import sys, os
sys.path.append(os.path.dirname(__file__))

#Importing tools from Nicole Hartman Oct 2019 to produce statistical analysis

import numpy as np
import uproot
from uproot_methods.classes.TLorentzVector import TLorentzVectorArray

import os
#not importing hep_ml since i don't need to include bdt reweight
import pickle
import pyhf
import hep_ml
from hep_ml import reweight

import utilities as u
from numpy import savez_compressed
from tqdm import tqdm

def getBDTWeights(data, BDT_fname = None, trainBDT=False, kinematic_region=2, key = 'w_2b', append_df = True, save_weights = False):
    '''
    Add the BDT weights to the dataframe

    Note: I should add functionality for training in the SB before the cuts,
    but since min_dRjj_hc1 isn't looking promising, I'll do this later.

    Inputs:
    - data: pd DataFrame for the data
    - BDT_fname: Name of the pickle file to either save the BDT to or read it from
    - trainBDT: If true, retrain the BDT
    - kinematic_region: The region to use for the kinematic reweighting
                        * 2: SB (default)
                        * 1: CR (for the background systematic)

    '''

    mask_2b = (data.ntag == 2)&(data.kinematic_region == kinematic_region)
    mask_4b = (data.ntag >= 4)&(data.kinematic_region == kinematic_region)

    sort_rw_cols = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2', 'njets']

    if trainBDT:

        BDT_params = {
            'n_estimators' : 50,
            'learning_rate' : 0.1,
            'max_depth' : 3,
            'min_samples_leaf' : 125,
            'gb_args' : {'subsample': 0.4}
            }

        reweighter = reweight.GBReweighter(n_estimators=BDT_params['n_estimators'],
                                           learning_rate=BDT_params['learning_rate'],
                                           max_depth=BDT_params['max_depth'],
                                           min_samples_leaf=BDT_params['min_samples_leaf'],
                                           gb_args=BDT_params['gb_args'])

        original = data.loc[mask_2b,sort_rw_cols]
        target  = data.loc[mask_4b,sort_rw_cols]

        print("Training on columns:", sort_rw_cols)
        reweighter.fit(original, target)

        if BDT_fname is not None:
            pickle.dump(reweighter, open( BDT_fname, "wb" ))

    else:

        reweighter = pickle.load(open( BDT_fname, "rb" ))


    #N4bTo2b = np.sum(mask_4b) / np.sum(mask_2b)
    #key = "w_2b"
    if kinematic_region == 1:
        key += "_CR"
    if append_df == True:    
        data[key] = reweighter.predict_weights(data[sort_rw_cols])

    w2b = reweighter.predict_weights(data.loc[mask_2b, sort_rw_cols])
    event_number = data.loc[mask_2b, 'event_number']
    run_number = data.loc[mask_2b, 'run_number']
    
    return w2b, (event_number, run_number)


def repeatBDTtrain(data, repeat = 3, trainBDT=True, kinematic_region=2, fname = None):

    mask_2b = (data.ntag == 2)&(data.kinematic_region == kinematic_region)
    num2b = len(data.loc[mask_2b])
    event_number = data.loc[mask_2b, 'event_number']
    run_number = data.loc[mask_2b, 'run_number']

    
    weights = np.zeros((num2b, repeat))


    for i in tqdm(range(repeat)):
        
        w_i, _ = getBDTWeights(data, trainBDT = True, kinematic_region = 2)
        weights[:, i] = w_i


    weights_median = np.median(weights, axis = 1)
    weights_std = np.std(weights, axis = 1)

    if fname is not None:
        savez_compressed(fname, weights)


    return weights, weights_median, weights_std