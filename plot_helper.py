from __future__ import print_function
import sys, os
sys.path.append(os.path.dirname(__file__))

import matplotlib.pyplot as plt
from matplotlib import gridspec
import seaborn as sns
import utilities as u
import trigger_utilities as tu
import histogram_helper as hh
import numpy as np
from collections import OrderedDict
from scipy.stats import wasserstein_distance 
from scipy.stats import ks_2samp
from scipy.stats import norm as normG
import matplotlib.mlab as mlab

def dual_horizontal_bar_plot(h1, h2, ylabels, **kwargs):
    """
    Description
    :param h1: height of first set
    :param h2: height of second set
    :reutrn:

    """

    options = {'figsize'  : (10, 20),
               'width'    : 0.4,
               'sort'     : False,
               'xlabel'   : 'Yield (%)',
               'title'    : 'Efficiencies',
               'h1_label' : 'h1',
               'h2_label' : 'h2',
               'filename' : 'test.png',
               'bold'     : False,
               'show'     : False,
               'save'     : False}

    u.userOptionsUndefined(kwargs, options)

    options.update(kwargs)

    if options['bold'] == True:
        fontweight = 'bold'
    elif options['bold'] == False:
        fontweight = 'normal'


    if options['sort'] == True:
        #this assumes that h1 and h2 are sorted in the same manner
        h1, ylabels = tu.sort_trigger_yields(h1, ylabels)
        h2, ylabels2 = tu.sort_trigger_yields(h2, ylabels)

    if max(h1) > max(h2):
        maximum = max(h1)
    else:
        maximum = max(h2)

    N = len(h1)

    index = np.linspace(N-1, 0, N, endpoint = True)
    fig = plt.figure(figsize = options['figsize'])

    ax = fig.add_subplot(111)

    width = options['width']
    rects1 = ax.barh(index, h1, width) # horizontal bar1
    rects2 = ax.barh(index + width, h2, width) # horizontal bar2

    ax.set_xlabel(options['xlabel'], fontsize = 14, fontweight = fontweight)
    plt.title(options['title'], fontsize = 14, fontweight = fontweight)
    ax.set_yticks(index + width)
    ax.set_yticklabels(ylabels, fontsize = 14)
    ax.legend( (rects1[0], rects2[1]), (options['h1_label'], options['h2_label']), fontsize = 14, loc = 'lower right')
    ax.set(xlim=(0, maximum+10))
    ax.set(ylim=(-1, N))

    def autolabel_horizontal(rects, offset_x, offset_y):

        for rect in rects:
            h = rect.get_width()
            ax.text(offset_x * h, rect.get_y() + offset_y * rect.get_height() / 2, round(h, 2), fontsize = 14)

    #calls the autolabel function: change numbers to suit your placement.
    autolabel_horizontal(rects1, 1.05, 0.25)
    autolabel_horizontal(rects2, 1.05, 1)

    if options['save']:
        plt.savefig(options['filename'], dpi = 300, bbox_inches = 'tight')
    if options['show']:
        plt.show()



def horizontal_bar_plot(x, y, **kwargs):
    """
    Plots the horizontal bar chart showing the percentage of events passing a specifc trigger selection relative to the total
    number of events. Uses the Seaborn librzary
    :param x: array containing percentage values to plot
    :param y: list containing the names of the triggers as a string
    :param filename: output filename for bar chart plot
    :param show: boolean to turn showing the plot on or off during run time
    :param save: boolean to turn file saving on or off
    :return:
    """
    options = {'figsize' : (10, 30),
               'filename': 'test.png',
               'title'   : 'Working title',
               'xlabel'  : 'Efficiency (%)',
               'ylabel'  : 'Trigger Name',
               'color'   : 'blue',
               'show'    : False,
               'save'    : True,
               'sort'    : True
               }

    u.userOptionsUndefined(kwargs, options)

    options.update(kwargs)


    if options['sort']:
        x, y = tu.sort_trigger_yields(x, y, reversed = False)

    plt.clf()

    N = len(y)
    y_pos = [i for i, _ in enumerate(y)]
    #y_pos = np.linspace(N-1, 0, N, endpoint = True)
    #y_pos_ticks = np.linspace(0, N-1, N, endpoint = True)

    fig, ax = plt.subplots(figsize=options['figsize'])
    g = ax.barh(y_pos, x, color = options['color'])

    for i in range(len(x)):
        ax.text(x[i], i, round(x[i], 2), color = "black", fontsize = 14)

    plt.ylabel(options['ylabel'], fontsize = 14)
    plt.xlabel(options['xlabel'], fontsize = 14)
    plt.title(options['title'], fontsize = 14)
    ax.set(xlim=(0, max(x)+10))
    ax.set_yticks(y_pos)
    ax.set_yticklabels(y, fontsize = 14)


    if options['save']:
        fig.savefig(options['filename'], dpi = 300, bbox_inches = "tight")
    if options['show']:
        plt.show()

def trigger_efficiency_subplots(dataframe, column, triggerlist, selection, year, feature = "event.m_hh"):
    """
    Creates a plot for each trigger in the triggerlist against some specific feature, such as m_hh. It selects events
    passing the specific trigger and creates a new frame with the feature selected. It then tries to do adaptive binning
    to make sure the bins have sufficient statistics.
    :param dataframe: dataframe containing the full information
    :param column: Name of the trigger map column (sometimes its different)
    :param triggerlist: list of all triggers
    :param feature: particular feature to plot against such as m_hh or pt_hh
    :param selection: string to change the output file name to distinguish between before or after generic selection cuts
    :param year: string to change the output filename to distinguish which year the dataset came from.
    :return:
    """

    full_hist = dataframe[feature].values
    #print(full_hist)
    total_subplots = len(triggerlist)
    #plt.clf()
    #fig = plt.figure(figsize = (60, 40))


    folder = 'feature-efficiencies' + '/'

    for i in range(0, total_subplots):
        plt.clf()
        plt.figure(figsize=(8,8))
        trigger_hist = u.trigger_histogram(dataframe, column, triggerlist[i], feature)
        _, bin_edges = hh.adaptive_binning(trigger_hist, threshold = 20, bi = 20)
        ki, _ = np.histogram(trigger_hist, bins = bin_edges)
        ni, _ = np.histogram(full_hist, bins = bin_edges)
        x = hh.bin_midpoint(bin_edges)
        xerr = hh.bin_width_error(bin_edges)
        epsilon = 0.000000000000000001
        yerr = 100 * hh.binomial_error(ki, ni+epsilon)
        ratio = 100 * (ki/ (ni + epsilon))
        plt.errorbar(x, ratio, xerr = xerr, yerr = yerr, fmt = '--.k')
        plt.xlabel(feature + " (GeV)", fontsize = 14)
        plt.ylabel("efficiency (%)", fontsize = 14)
        plt.title(triggerlist[i])
        plt.savefig(folder+triggerlist[i]+"_" + selection + "_"+feature+year+".png", dpi = 300)


def massplane(df, **kwargs):
    # Default value of plotting
    options = {'figsize': (10, 8),
               'bins' : (30, 30),
               'xrange': (20, 200),
               'yrange': (20, 200),
               'weights': np.ones(len(df)),
               'xhisttype': 'step',
               'yhisttype': 'step',
               'cmap': 'YlGnBu',
               'title': '2D plane',
               'xlabel': '$m_{H1}$',
               'ylabel': '$m_{H2}$',
               'axis_hist_on': True,
               'hist_regions': True,
               'hist_lines': True,
               'hist_color': 'black',
               'SR_color': 'hotpink',
               'CR_color': 'darkorange',
               'SB_color': 'navy',
               'filename': 'test.png',
               'alpha': 0.7,
               'save': False,
               'show': False
    }

    u.userOptionsUndefined(kwargs, options)

    options.update(kwargs)

    m_x = df.m_h1.values
    m_y = df.m_h2.values

    fig = plt.figure(figsize=options['figsize'])

    grid =  plt.GridSpec(4, 4, hspace = 0.02, wspace = 0.02, height_ratios = [1, 1, 1, 1], width_ratios = [1, 1, 1, 0.1])
    main_ax = fig.add_subplot(grid[:-1,1:-1])

    colorbar = fig.add_subplot(grid[:-1,3])

    h = main_ax.hist2d(m_x, m_y, bins = options['bins'], range=(options['xrange'], options['yrange']), weights = options['weights'], cmap = options['cmap'])

    if options['hist_regions']:
        SR_x, SR_y1 = u.drawSR()
        main_ax.plot(SR_x, SR_y1, options['SR_color'])
        CR_x, CR_y1 = u.drawCR()
        main_ax.plot(CR_x, CR_y1, options['CR_color'])
        SB_x, SB_y1 = u.drawSB()
        main_ax.plot(SB_x, SB_y1, options['SB_color'])

    plt.colorbar(h[3], cax = colorbar)

    if options['axis_hist_on']:
        y_hist = fig.add_subplot(grid[:-1,0], xticklabels=[])
        x_hist = fig.add_subplot(grid[-1,1:-1], yticklabels=[])
        main_ax.set_xticks([])
        main_ax.set_yticks([])

        x_hist.hist(m_x, options['bins'][0], color = options['hist_color'], range = options['xrange'], histtype = options['xhisttype'], weights = options['weights'], orientation='vertical')
        x_hist.invert_yaxis()
        x_hist.set_xlabel(options['xlabel'], fontsize = 14)


        y_hist.hist(m_y, options['bins'][1], color = options['hist_color'], range = options['yrange'], histtype = options['yhisttype'], weights = options['weights'], orientation='horizontal')
        y_hist.invert_xaxis()
        y_hist.set_ylabel(options['ylabel'], fontsize = 14)

        if options['hist_lines']:

            x_hist.axvline(min(SB_x), color = options['SB_color'], ls = '--', alpha = options['alpha'])
            x_hist.axvline(max(SB_x), color = options['SB_color'], ls = '--', alpha = options['alpha'])
            x_hist.axvline(min(CR_x), color = options['CR_color'], ls = '--', alpha = options['alpha'])
            x_hist.axvline(max(CR_x), color = options['CR_color'], ls = '--', alpha = options['alpha'])
            x_hist.axvline(min(SR_x), color = options['SR_color'], ls = '--', alpha = options['alpha'])
            x_hist.axvline(max(SR_x), color = options['SR_color'], ls = '--', alpha = options['alpha'])

            y_hist.axhline(max(SB_y1), color = options['SB_color'], ls = '--', alpha = options['alpha'])
            y_hist.axhline(min(SB_y1), color = options['SB_color'], ls = '--', alpha = options['alpha'])
            y_hist.axhline(max(CR_y1), color = options['CR_color'], ls = '--', alpha = options['alpha'])
            y_hist.axhline(min(CR_y1), color = options['CR_color'], ls = '--', alpha = options['alpha'])
            y_hist.axhline(max(SR_y1), color = options['SR_color'], ls = '--', alpha = options['alpha'])
            y_hist.axhline(min(SR_y1), color = options['SR_color'], ls = '--', alpha = options['alpha'])
    else:
        main_ax.set_xlabel(options['xlabel'], fontsize = 14)
        main_ax.set_ylabel(options['ylabel'], fontsize = 14)
    main_ax.set_title(options['title'], fontsize = 18)
    if options['save']:
        plt.savefig(options['filename'], dpi = 300, bbox_inches = 'tight')
    if options['show']:
        plt.show()

    return {"h2d":h[0],"bins":h[1], 'fig':fig, "main_ax":main_ax}


def dRplanes(x, y, **kwargs):

    options = {'figsize': (10, 8),
                   'bins' : (30, 30),
                   'xrange': (20, 200),
                   'yrange': (20, 200),
                   'weights': np.ones(len(x)),
                   'xhisttype': 'step',
                   'yhisttype': 'step',
                   'cmap': 'YlGnBu',
                   'title': '2D plane',
                   'xlabel': 'x',
                   'ylabel': 'y',
                   'axis_hist_on': True,
                   'hist_color': 'black',
                   'filename': 'test.png',
                   'alpha': 0.7,
                   'save': False,
                   'show': False
        }

    u.userOptionsUndefined(kwargs, options)

    options.update(kwargs)

    fig = plt.figure(figsize=options['figsize'])

    grid =  plt.GridSpec(4, 4, hspace = 0.02, wspace = 0.02, height_ratios = [1, 1, 1, 1], width_ratios = [1, 1, 1, 0.1])
    main_ax = fig.add_subplot(grid[:-1,1:-1])

    colorbar = fig.add_subplot(grid[:-1,3])

    h = main_ax.hist2d(x, y, bins = options['bins'], range=(options['xrange'], options['yrange']), weights = options['weights'], cmap = options['cmap'])

    plt.colorbar(h[3], cax = colorbar)

    if options['axis_hist_on']:
        y_hist = fig.add_subplot(grid[:-1,0], xticklabels=[])
        x_hist = fig.add_subplot(grid[-1,1:-1], yticklabels=[])
        main_ax.set_xticks([])
        main_ax.set_yticks([])

        x_hist.hist(x, options['bins'][0], color = options['hist_color'], range = options['xrange'], histtype = options['xhisttype'], weights = options['weights'], orientation='vertical')
        x_hist.invert_yaxis()
        x_hist.set_xlabel(options['xlabel'], fontsize = 14)


        y_hist.hist(y, options['bins'][1], color = options['hist_color'], range = options['yrange'], histtype = options['yhisttype'], weights = options['weights'], orientation='horizontal')
        y_hist.invert_xaxis()
        y_hist.set_ylabel(options['ylabel'], fontsize = 14)
    else:
        main_ax.set_xlabel(options['xlabel'], fontsize = 14)
        main_ax.set_ylabel(options['ylabel'], fontsize = 14)

    main_ax.set_title(options['title'], fontsize = 18)
    if options['save']:
        plt.savefig(options['filename'], dpi = 300, bbox_inches = 'tight')
    if options['show']:
        plt.show()

    return plt

def test(x,y):

    a = x/y
    b = 0.25*y*(x*y**(-1.5) + y**(-0.5))**2

    error = np.sqrt(a+b)

    return error

def reweightedHist(df, kinematic_region = 2, feature = 'm_hh_cor', bins = 50, hrange = (200, 1400), figsize = (8,8),
                   weighted = True,
                   w = None, 
                   weight_column = 'w_2b',
                   histtype='step',
                   color = 'darkorange',
                   linewidth = 2,
                   norm = 1,
                   display = False,
                   title = 'default',
                   pull = 'atlas',
                   pull_fit = False,
                   density = True,
                   show_stats = True):
    eps = 1e-15

    d2b = df[(df.ntag == 2) & (df.kinematic_region == kinematic_region)]
    d4b = df[(df.ntag >= 4) & (df.kinematic_region == kinematic_region)]

    four_b = np.sum((df.kinematic_region == kinematic_region) & (df.ntag >=4))
    if w is not None:
        two_b = np.sum(w)
    else:
        two_b = df.loc[((df.kinematic_region == kinematic_region) & (df.ntag == 2)), weight_column].sum()

    N4bTo2b = four_b/two_b
    #N4bTo2b = 0.006677424919

    if weighted == True and w is None:
        weights = N4bTo2b * d2b[weight_column].values
    elif weighted == False and w is None:
        weights = N4bTo2b * np.ones(len(d2b))
    elif w is not None:
        weights = N4bTo2b * w

    #print(hrange)
    y4b, be = np.histogram(d4b[feature], bins = bins, range = hrange, density = density)
    #print(be)
    f4b, _ = np.histogram(d4b[feature], bins = be, range = hrange)
    f2b, _ = np.histogram(d2b[feature], bins = be, weights = weights, range = hrange)

    x4b = hh.bin_midpoint(be)
    x4b_err = hh.bin_width_error(be)

    if density == True:
        y4b_err = np.where(f4b > 0, y4b * (np.sqrt(f4b)/(f4b+eps)), 0)
    elif density == False:
        y4b_err = np.where(f4b > 0, np.sqrt(f4b), 0)

    
    fig = plt.figure(figsize = figsize)
    gs = gridspec.GridSpec(2, 1, height_ratios =[3,1])
    gs.update(wspace =0.0, hspace =0.0)
    ax0 = plt.subplot(gs[0])
    ax0.errorbar(x4b, y4b, xerr = x4b_err, yerr = y4b_err, fmt ='k+', label = '4b')



    y2b, _, _ = ax0.hist(d2b[feature], bins = be, density = density,
                        weights = weights, range = hrange, histtype = histtype,
                        color = color, linewidth = linewidth, label = '2b')

    #print(y2b)
    ax1 = plt.subplot(gs[1])
    eps = 1e-15
    R = y4b/(y2b + 1e-15)
    Rdx = x4b_err

    if pull == 'simple':
        pulls = (y2b - y4b) / y4b_err
    
    if pull == 'atlas':
        
        pulls = u.run_pulls(f2b, f4b)
        ax1.plot(x4b, pulls, 'k+')
        ax1.set_ylabel('Pull', fontsize = 14)
        ax1.axhline(y = 0, color = 'k', ls = '--')

        if pull_fit == True:
            print("function not yet ready")
            #(mu, sigma) = normG.fit(pull)
            #a = plt.plot([0.65, 0.6, 0.2, 0.2])
            #n, bins, patches = plt.hist(pull, 10, density = 1)
            #y = normG.pdf(bins, mu, sigma)
            #plt.plot(bins, y, 'r--', lw=2)
            #plt.title(r'$\mathrm{Histogram\ of\ pullssss:}\ \mu=%.3f,\ \sigma=%.3f$' %(mu, sigma))
            #ax1.set_ylim(-10*10**-4, 10*10**-4)
    elif pull == None:
        pulls = None
        R = y4b/(y2b + eps)
        Rdy = (y4b_err/ (y2b +1e-15))
        ax1.errorbar(x4b, R, xerr = Rdx, yerr = Rdy, fmt = 'k+')
        ax1.set_ylabel(r'$\frac{4b}{2b}$', fontsize = 14)
        ax1.set_ylim(0, 2)
        ax1.axhline(y= 1, color = 'k', linestyle = '--')


    ax0.legend(loc = 'upper center')
    ax0.set_ylabel('Frequency', fontsize = 16)
    ax0.set_title(title)
    ax1.set_xlabel(feature, fontsize = 16)
    y2b_before, _ = np.histogram(d2b[feature], bins = be)
    wd_before = wasserstein_distance(y2b_before, y4b)
    wd_after = wasserstein_distance(y2b, y4b)
    ks_before = ks_2samp(y2b_before, y4b)
    ks_after  = ks_2samp(y2b, y4b)
    kl_b      = u.kl_divergence(y2b_before, y4b)
    kl_a      = u.kl_divergence(y2b, y4b)

    chi_squared = np.sum(np.where(f4b > 0, ((f2b - f4b)**2)/(f4b+eps), 0))

    nnof = len(be) - 1

    chi_nnof = chi_squared/nnof


    if show_stats == True:
        ax0.text(0.975, 0.95, 'ks before = {:.3f}'.format(ks_before[0]) + ' (p-value = {:.3e})'.format(ks_before[1]),
                ha = 'right', va = 'top', transform = ax0.transAxes, color = 'black')
        ax0.text(0.975, 0.85, 'ks after = {:.3f}'.format(ks_after[0]) + ' (p-value = {:.3e})'.format(ks_before[0]),
                ha = 'right', va = 'top', transform = ax0.transAxes, color = 'black')
        ax0.text(0.975, 0.75, 'EMD before = {:.3f}'.format(wd_before),
                ha = 'right', va = 'top', transform = ax0.transAxes, color = 'black')
        ax0.text(0.975, 0.65, 'EMD after = {:.3e}'.format(wd_after),
                ha = 'right', va = 'top', transform = ax0.transAxes, color = 'black')
        #ax0.text(0.975, 0.55, '$\\frac{\\chi^2}{nnof}$ = $\\frac{{:.3f}'.format(chi_squared) + '\}$',
        #        ha = 'right', va = 'top', transform = ax0.transAxes, color = 'black')         
        
        a = '{:.3f}'.format(chi_squared)
        b = '{:.0f}'.format(nnof)
        c = '{:.3f}'.format(chi_nnof)

        chi_def = r'$\frac{\chi^2}{nnof}$'
        chi_string = chi_def + ' = ' r'$\frac{' + a + '}{' + b + '} = ' + c + '$'

        ax0.text(0.975, 0.55, chi_string,
                ha = 'right', va = 'top', transform = ax0.transAxes, color = 'black', fontsize = 14) 
        #ax0.text(0.975, 0.45, 'KL before = {:.3f}'.format(kl_b), 
        #        ha = 'right', va = 'top', transform = ax0.transAxes, color = 'black', fontsize = 14)
        #ax0.text(0.975, 0.35, 'KL after = {:.3f}'.format(kl_a),
        #        ha = 'right', va = 'top', transform = ax0.transAxes, color = 'black', fontsize = 14)                   



    if display:
        print("wasserstein distance before = ", wd_before)
        print("wasserstein distance after = ", wd_after)

    #print(be)
    results = {'y2b': y2b,
               'y4b': y4b,
               'wd_before': wd_before,
               'wd_after': wd_after,
               'ks_before': ks_before,
               'ks_after': ks_after,
               'pulls': pulls,
               'bins': be,
               'f2b': f2b,
               'f4b': f4b,
               'N4bTo2b': N4bTo2b,
               'chi_noff': chi_nnof}

    return results

def fit_pulls(pulls, edges = 10, title = None, histrange = 'auto', histtype = 'bar', hist_off = False, fill_between = False, remove_zeros = True):

    plt.style.use('seaborn-whitegrid')

    if remove_zeros == True:
        pulls = pulls[pulls!=0]

    (mu, sigma) = normG.fit(pulls)

    #hmin = 0
    #hmax = 0

    if histrange == 'auto':
        hmin = np.min(pulls)
        hmax = np.max(pulls)
    else:
        hmin = histrange[0]  
        hmax = histrange[1]

    if hist_off == False:
        n, be, patches = plt.hist(pulls, bins = edges, range = (hmin, hmax), density = 1, lw = 2, histtype = histtype)
    if hist_off == True:
        n, be = np.histogram(pulls, bins = edges, range = (hmin, hmax), density = True)
        f, _ = np.histogram(pulls, bins = edges, range = (hmin, hmax), density = False)
        nerr = n * (np.sqrt(f)/f)
        if fill_between == False:
            plt.errorbar(hh.bin_midpoint(be), n, xerr = hh.bin_width_error(be), yerr = nerr, fmt='.k', capsize = 10)
        elif fill_between == True:
            plt.fill_between(hh.bin_midpoint(be), n-nerr, n+nerr, alpha = 0.2, facecolor='r')

    y = normG.pdf(hh.bin_midpoint(be), mu, sigma)
    l = plt.plot(hh.bin_midpoint(be), y, 'r--', lw = 2)
    plt.axvline(mu,color = 'grey', ls ='--')
    plt.title(title + r'$: \mu=%.3f,\ \sigma=%.3f$' %(mu, sigma))

def cdf_err(f):
    
    cdf_err = np.zeros(len(f))
    a = 0
    for i, _ in enumerate(f):
        a = a + np.sqrt(f[i])**2
        cdf_err[i] = np.sqrt(a)
        
    return cdf_err

def bkgCDF(hists, feature = r'$m_{4j}$ [GeV]', title = 'ECDF', save = False, filename = 'test.pdf'):

    x = hh.bin_midpoint(hists['bins'])
    
    #Normalized cumulative dist
    y4b = np.cumsum(hists['y4b']) / np.sum(hists['y4b'])
    y2b = np.cumsum(hists['y2b']) / np.sum(hists['y2b'])

    fig, ax1 = plt.subplots(figsize=(10,10))
    #yerr_cdf = np.cumsum(np.sqrt(np.sqrt(a['f4b'])**2)/np.sum(a['f4b']))
    yerr_cdf = cdf_err(hists['f4b'])/np.sum(hists['f4b'])

    yerr_cdf_2b = np.cumsum(np.sqrt(hists['f2b']/hists['N4bTo2b'])/np.sum(hists['f2b']/hists['N4bTo2b']))

    ax1.fill_between(x, y4b-yerr_cdf, y4b+yerr_cdf, alpha = 0.4, color = 'green', label = '4b')
    ax1.fill_between(x, y2b-yerr_cdf_2b, y2b+yerr_cdf_2b, alpha = 0.4, color = 'red', label = '2b')


    

    ax1.set_xlabel(feature, fontsize = 16)
    ax1.set_ylabel('Cummulative Frequency', fontsize = 16)
    ax1.set_title(title, fontsize = 16)
    ax1.legend(loc='upper left', fontsize = 16)

    plt.show()
    
    if save == True:
        plt.savefig(filename, dpi = 600, bbox_inches ='tight')
    
    return ax1