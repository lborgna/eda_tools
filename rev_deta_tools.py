from __future__ import print_function
import sys, os

sys.path.append(os.path.dirname(__file__))

import matplotlib.pyplot as plt
import atlas_mpl_style as ampl

ampl.set_color_cycle("Paper")
import pandas as pd
import numpy as np

np.seterr(divide="ignore")
from iminuit import Minuit
from probfit import BinnedLH, gaussian
import scipy
from scipy.stats import norm as normG
import matplotlib.mlab as mlab
from matplotlib import gridspec


import bkg_helper as bh
import utilities as u
import histogram_helper as hh


def bkg_validation(
    df: pd.DataFrame,
    feature: str,
    hrange: tuple,
    nbins: int,
    muIQR: float,
    filename: str = None,
    muCR: float = None,
    muVR: float = None,
    xlabel: str = "x",
    year: int = 2016,
    weightColumnCR: str = "NN_d24_weight",
    weightColumnVR: str = "NN_d24_weight_VRderiv",
    weightIQRColumnCR: str = "NN_d24_weight_IQR_17",
    ignore_stats: bool = True,
    target_reg: str = None,
    xlim: tuple = None,
    ylim: tuple = None,
    plot_tag: str = None,
):

    maskSR4b = (df.ntag >= 4) & (df.kinematic_region == 0)

    maskSR2b = (df.ntag == 2) & (df.kinematic_region == 0)

    if target_reg == "4b":
        mask_reg = df["rw_to_4b"]
        mask_reg_vr = df["rw_to_4b_VR"]
    elif target_reg == "3b1f":
        mask_reg = df["rw_to_3b1f"]
        mask_reg_vr = df["rw_to_3bf_VR"]

    elif target_reg == "3b1l":
        mask_reg = df["rw_to_3b1l"]
        mask_reg_vr = df["rw_to_3b1l_VR"]
    else:
        mask_reg = pd.Series(np.ones(df.shape[0], dtype=bool))
        mask_reg_vr = pd.Series(np.ones(df.shape[0], dtype=bool))

    if muCR is None or muVR is None:
        maskCR4b = (df.ntag >= 4) & (df.kinematic_region == 2)
        maskCR2b = (df.ntag == 2) & (df.kinematic_region == 2)
        N4bCR = len(df[maskCR4b])
        N4bSR = len(df[maskSR4b])
        N2bCRw = df.loc[maskCR2b & mask_reg, weightColumnCR].sum()
        N2bVRw = df.loc[maskCR2b & mask_reg_vr, weightColumnVR].sum()
        muCR = N4bCR / N2bCRw
        muVR = N4bCR / N2bVRw

    fig = plt.figure(figsize=(15, 15))
    gs = gridspec.GridSpec(3, 2, height_ratios=[3, 1, 1], width_ratios=[4, 1])
    ax1 = fig.add_subplot(gs[0, 0])
    ax2 = fig.add_subplot(gs[1, 0], sharex=ax1)
    ax3 = fig.add_subplot(gs[2, 0], sharex=ax1)
    ax4 = fig.add_subplot(gs[2, 1], sharey=ax3)
    plt.setp(ax4.get_yticklabels(), visible=False)
    gs.update(hspace=0.0)
    gs.update(wspace=0.0)

    cr, edges, _ = ax1.hist(
        df.loc[maskSR2b & mask_reg, feature],
        bins=nbins,
        density=0,
        range=hrange,
        lw=2,
        color="k",
        histtype="step",
        log=False,
        label="2b SR (CR Model)",
        weights=muCR * df.loc[maskSR2b & mask_reg, weightColumnCR],
    )

    cr200, edges200 = np.histogram(
        df.loc[maskSR2b & mask_reg, feature],
        bins=100,
        density=0,
        range=hrange,
        weights=muCR * df.loc[maskSR2b & mask_reg, weightColumnCR],
    )

    bs_syst200 = bh.bs_error(
        df,
        0,
        edges200,
        weightColumnCR,
        weightIQRColumnCR,
        muCR,
        muIQR,
        target_reg=target_reg,
    )
    h_err_bs200 = bs_syst200["h_err_bs_mag"]
    cr_poisson200 = u.weighted_histo_error(
        df.loc[maskSR2b & mask_reg, feature].values,
        muCR * df.loc[maskSR2b & mask_reg, weightColumnCR],
        edges200,
    )

    cr_err200 = np.sqrt(h_err_bs200 ** 2 + cr_poisson200 ** 2)

    x = hh.bin_midpoint(edges)

    bs_syst = bh.bs_error(
        df,
        0,
        edges,
        weightColumnCR,
        weightIQRColumnCR,
        muCR,
        muIQR,
        feature=feature,
        target_reg=target_reg,
    )
    h_err_bs_mag = bs_syst["h_err_bs_mag"]

    cr_poisson_err = u.weighted_histo_error(
        df.loc[maskSR2b & mask_reg, feature].values,
        muCR * df.loc[maskSR2b & mask_reg, weightColumnCR],
        edges,
    )

    cr_err = np.sqrt(h_err_bs_mag ** 2 + cr_poisson_err ** 2)

    ax1 = u.plotHatch(edges, cr - cr_err, cr + cr_err, ax1)

    vr, _, _ = ax1.hist(
        df.loc[maskSR2b & mask_reg_vr, feature],
        edges,
        density=0,
        color="b",
        lw=2,
        histtype="step",
        label="2b SR (VR Model)",
        log=False,
        weights=muVR * df.loc[maskSR2b & mask_reg_vr, weightColumnVR],
    )

    vr200, _ = np.histogram(
        df.loc[maskSR2b & mask_reg_vr, feature],
        edges200,
        density=0,
        weights=muVR * df.loc[maskSR2b & mask_reg_vr, weightColumnVR],
    )

    inv, _, _ = ax1.hist(
        df.loc[maskSR2b & mask_reg_vr, feature],
        edges,
        density=0,
        color="r",
        lw=2,
        histtype="step",
        label="2b SR (VR inverted)",
        log=False,
        weights=2 * muCR * df.loc[maskSR2b & mask_reg, weightColumnCR]
        - muVR * df.loc[maskSR2b & mask_reg_vr, weightColumnVR],
    )

    hSR4b, _ = np.histogram(df.loc[maskSR4b, feature], bins=edges)
    SR4b_error = np.sqrt(hSR4b)
    hSR4b200, _ = np.histogram(df.loc[maskSR4b, feature], bins=edges200)
    SR4b_err200 = np.sqrt(hSR4b200)

    ax1.errorbar(x, hSR4b, yerr=SR4b_error, label="4b SR", fmt="+g")

    ax2.plot(x, vr / cr, "-", color="indigo")
    ax2.plot(x, inv / cr, "-", color="indigo")
    ax2.fill_between(
        x, vr / cr, inv / cr, alpha=0.5, color="purple", label="shape systematic"
    )

    h_err_total = np.sqrt(cr_err ** 2 + ((vr - cr) ** 2))
    h_err_total200 = np.sqrt(cr_err200 ** 2 + ((vr200 - cr200) ** 2))

    ax2.fill_between(
        x,
        (h_err_total + cr) / cr,
        (cr - h_err_total) / cr,
        alpha=0.5,
        color="grey",
        label="shape+boots",
    )
    Rerr = (hSR4b / cr) * (SR4b_error / hSR4b)
    ax2.errorbar(x, hSR4b / cr, yerr=Rerr, label="4b SR", fmt="g+")
    ax2.axhline(1, color="black")
    ax2.set_ylim(0.4, 1.6)

    pull = bh.run_pulls_simple(hSR4b, SR4b_error, cr, h_err_total)
    pull200 = bh.run_pulls_simple(hSR4b200, SR4b_err200, cr200, h_err_total200)

    ax3.plot(x, pull, "b+")
    ax3.axhline(3, color="dimgrey", ls="--")
    ax3.axhline(2, color="dimgrey", ls="--")
    ax3.axhline(1, color="black", ls="--")
    ax3.axhline(-1, color="black", ls="--")
    ax3.axhline(-2, color="dimgrey", ls="--")
    ax3.axhline(-3, color="dimgrey", ls="--")
    ax3.axhline(0, color="black")
    ax3.set_ylim(-3.5, 3.5)
    ampl.set_xlabel(xlabel, ax=ax3, fontsize=16)
    ax3.set_ylabel("Significance", fontsize=14)
    ampl.set_ylabel("arb. units", ax=ax1, fontsize=16)
    ax1.legend(fontsize=16)
    ax2.set_ylabel("Var / Nom", fontsize=14)
    ax2.legend(loc="upper left", ncol=3)
    (mu, sigma) = normG.fit(pull200)

    binned_likelihood = BinnedLH(gaussian, pull200, bins=13, bound=[-3, 3])
    minuit = Minuit(binned_likelihood, mean=0.0, sigma=1.0)
    minuit.migrad()
    results = binned_likelihood.draw(minuit, no_plot=True)

    pull_edges = results[0][0]
    pull_x = hh.bin_midpoint(pull_edges)
    pull_y, _ = np.histogram(pull200, bins=pull_edges, density=0)
    pull_y_norm, _ = np.histogram(pull200, bins=pull_edges, density=1)

    pull_y_err_norm = (pull_y_norm) * (np.sqrt(pull_y) / pull_y)
    ax4.errorbar(pull_y_norm, pull_x, xerr=pull_y_err_norm, fmt="b+")

    mu0 = minuit.values[0]
    sig0 = minuit.values[1]

    x_fit = np.linspace(1.2 * pull_x.min(), 1.2 * pull_x.max())
    y = normG.pdf(x_fit, mu0, sig0)
    yp = normG.pdf(x_fit, 0, 1)  # Perfect Fit

    mu_e = minuit.errors[0]
    sig_e = minuit.errors[1]

    ax4.plot(y, x_fit, "r-")
    ax4.plot(yp, x_fit, "k--")
    ymax = y.copy()
    ymin = y.copy()

    for m1, m2 in zip([1, 1, -1, -1], [1, -1, 1, -1]):
        ynew = normG.pdf(x_fit, mu0 + m1 * mu_e, sig0 + m2 * sig_e)

        for i, _ in enumerate(ynew):
            if ynew[i] > ymax[i]:
                ymax[i] = ynew[i]

            if ynew[i] < ymin[i]:
                ymin[i] = ynew[i]

    ax4.fill_betweenx(x_fit, ymin, ymax, alpha=0.4, color="red")
    ax4.axhline(0, color="black", zorder=1)
    shapeVBF = {"up": (h_err_total + cr) / cr, "down": (cr - h_err_total) / cr, "x": x}
    stdTextN = "$\mathbf{ATLAS}$ Internal\n"
    stdTextN += r"$\sqrt{s} = 13$ TeV"
    stdTextN += ", {} {:.2f}".format(year, u.Lumi(year))
    stdTextN += r" $fb^{-1}$" + "\n"
    stdTextN += r"$|\Delta \eta_{HH}| > 1.5$ Resolved Signal Region"
    ax1.text(
        0.65,
        0.98,
        stdTextN,
        ha="right",
        va="top",
        transform=ax1.transAxes,
        color="black",
        fontsize=18,
    )

    """
    ampl.draw_atlas_label(
        0.25,
        0.98,
        ax=ax1,
        fontsize=18,
        simulation=False,
        energy="13 TeV",
        lumi=u.Lumi(year),
        status="int",
        desc=stdTextN,
    )
    """

    chi2, p, ndf = u.weighted_chisquare(
        hSR4b, cr, SR4b_error, h_err_total, stat_lim=10, ignore_stats=ignore_stats
    )

    chi2_ndf = chi2 / ndf

    p2 = scipy.stats.chi2.sf(chi2, ndf)

    a = "{:.3f}".format(chi2)
    b = "{:.0f}".format(ndf)
    c = "{:.3f}".format(chi2_ndf)

    chi_def = r"$\frac{\chi^2}{ndf}$"
    chi_string = chi_def + " = " r"$\frac{" + a + "}{" + b + "} = " + c + "$"

    ax1.text(
        0.8,
        0.7,
        chi_string,
        ha="right",
        va="top",
        transform=ax1.transAxes,
        color="black",
        fontsize=18,
    )
    ax1.text(
        0.8,
        0.6,
        "(p-value: {:.3f})".format(p),
        ha="right",
        va="top",
        transform=ax1.transAxes,
        color="black",
        fontsize=18,
    )
    if plot_tag is not None:
        ax1.set_title(plot_tag, fontsize=18, loc="right")

    ax4.set_xlim((-0.1, 0.5))
    if ylim is not None:
        ax1.set_ylim(ylim)
    if xlim is not None:
        ax1.set_xlim(xlim)
    ax1.tick_params(labelbottom=False)
    ax2.tick_params(labelbottom=False)
    ax4.set_title(
        r"$\mu$ = {:.3f}".format(mu0)
        + " $\pm$ {:.3f}".format(mu_e)
        + "\n"
        + "$\sigma$ = {:.3f}".format(sig0)
        + " $\pm$ {:.3f}".format(sig_e)
    )
    plt.tight_layout()

    if filename is not None:
        plt.savefig(filename, dpi=600, bbox_inches="tight")

    resultsDict = {
        "muCR": muCR,
        "muVR": muVR,
        "edges": edges,
        "edges200": edges200,
        "nbins": nbins,
        "chi2": chi2,
        "chi2ndf": chi2_ndf,
        "ndf": ndf,
        "p": p2,
        "pull": pull,
        "pull200": pull200,
        "mu": mu0,
        "std": sig0,
        "mu_e": mu_e,
        "std_e": sig_e,
        "fig": fig,
        "ax1": ax1,
        "ax2": ax2,
        "ax3": ax3,
        "ax4": ax4,
    }

    return resultsDict


def pulls_gaussian_fit(
    pulls: np.array,
    nbins: int = 30,
    bound: list = [-3, 3],
    filename: str = "pulls_gaussian_fit.png",
    title: str = "Gaussian fit of pulls",
):

    binned_likelihood = BinnedLH(gaussian, pulls, bins=nbins, bound=bound)

    minuit = Minuit(binned_likelihood, mean=0.0, sigma=1.0)
    minuit.migrad()
    results = binned_likelihood.draw(minuit, no_plot=True)
    pull_edges = results[0][0]

    fig = plt.figure(figsize=(11, 6))

    pull_y, _ = np.histogram(pulls, bins=pull_edges, density=0)
    pull_y_norm, _ = np.histogram(pulls, bins=pull_edges, density=1)

    pull_x = hh.bin_midpoint(pull_edges)

    norm = pull_y[4] / pull_y_norm[4]

    pull_y_err_norm = (pull_y_norm) * (np.sqrt(pull_y) / pull_y)
    plt.errorbar(pull_x, pull_y_norm, yerr=pull_y_err_norm, fmt="b+", label="pull")

    x_fit = np.linspace(1.5 * pull_x.min(), 1.5 * pull_x.max(), 1000)
    y = normG.pdf(x_fit, minuit.values[0], minuit.values[1])
    yp = normG.pdf(x_fit, 0, 1)
    mu0 = minuit.values[0]
    sig0 = minuit.values[1]

    mu_e = minuit.errors[0]
    sig_e = minuit.errors[1]

    plt.plot(x_fit, y, "r-", label="nominal Gaussian fit")
    hp = np.random.normal(0, 1, size=100000)
    hist_p, _, _ = plt.hist(
        hp,
        bins=pull_edges,
        color="black",
        ls="--",
        density=1,
        histtype="step",
        label="Ideal Gaussian Hist",
    )
    plt.plot(x_fit, yp, "k--", label="Ideal Gaussian")
    ymax = y.copy()
    ymin = y.copy()

    for m1, m2 in zip([1, 1, -1, -1], [1, -1, 1, -1]):
        ynew = normG.pdf(x_fit, mu0 + m1 * mu_e, sig0 + m2 * sig_e)

        for i, _ in enumerate(ynew):
            if ynew[i] > ymax[i]:
                ymax[i] = ynew[i]

            if ynew[i] < ymin[i]:
                ymin[i] = ynew[i]

    plt.fill_between(x_fit, ymin, ymax, alpha=0.4, color="red", label="Fit error")
    plt.xlim((-3, 3))
    plt.ylim((-0.05, 1))
    plt.axvline(0, color="black", lw=1)
    plt.xlabel("Significance", fontsize=18)
    plt.ylabel("arb. units", fontsize=18)
    plt.legend(loc="upper left", fontsize=12)

    pull_y_err_norm = np.where(np.isnan(pull_y_err_norm), 0, pull_y_err_norm)
    chi2, p, ndf = u.weighted_chisquare(
        norm * pull_y_norm,
        norm * hist_p,
        norm * pull_y_err_norm,
        np.zeros(len(hist_p)),
        stat_lim=10,
        ignore_stats=True,
    )

    # chi2, p, ndf = u.chi2_singleWeight(norm*pull_y_norm, norm*hist_p, norm*pull_y_err_norm)
    # chi2, p, ndf = u.chi2_unweighted(norm*pull_y_norm, norm*hist_p)
    # chi2, p, _ = u.chi2_unweighted(norm*pull_y_norm, norm*hist_p)
    # chi2, _, _  = u.chi2_unweighted(norm*pull_y_norm, norm*hist_p)
    # chi2, _, _ = u.chi2_unweighted(norm*pull_y_norm, norm*hist_p)
    # ndf = len(pull_edges)-2
    chi2_ndf = chi2 / ndf

    a = "{:.3f}".format(chi2)
    b = "{:.0f}".format(ndf)
    c = "{:.3f}".format(chi2_ndf)

    chi_def = r"$\frac{\chi^2}{ndf}$"
    chi_string = chi_def + " = " r"$\frac{" + a + "}{" + b + "} = " + c + "$"

    plt.text(1.2, 0.35, chi_string, color="black", fontsize=18)
    plt.text(1.2, 0.3, "(p-value: {:.3f})".format(p), color="black", fontsize=18)

    # plt.text(-1.8, 0.6, '$\\frac{\\chi^2}{ndf}$' + '= {:.3f}'.format(chi2_ndf), ha='left',va='top', fontsize = 18)
    plt.text(1.2, 0.55, "Fit results:", fontsize=18)
    plt.text(
        1.2,
        0.45,
        "$\mu$ = {:.3f} ".format(mu0)
        + "$\pm$ {:.3f}".format(mu_e)
        + "\n"
        + "$\sigma$ = {:.3f} ".format(sig0)
        + "$\pm$ {:.3f}".format(sig_e),
        fontsize=18,
    )

    plt.ylim((0.0, 0.6))
    plt.title(title, fontsize=16, loc="right")
    plt.tight_layout()
    plt.savefig(filename, dpi=600, bbox_inches="tight")
