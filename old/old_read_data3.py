import uproot
import pandas as pd
from pandas import DataFrame, HDFStore
import argparse
import trigger_utilities as u

class MergeTrees(object):

    def __init__(self, object):

        #Initialize EM Topo File
        self.filename_topo = object['input']

        if object['Nevents'] is not None:
            self.Nevents_sample = object['Nevents']
            self.sample = True
        else:
            self.sample = False

        self.ROOT_topo = uproot.open(self.filename_topo)
        self.df_TriggerList_topo = self.ROOT_topo["triggerList"].pandas.df()
        self.df_fmp_topo = self.ROOT_topo["fullmassplane"].pandas.df()
        self.n_events_initial_topo = self.ROOT_topo["TwoTagCutflow"].values[0]
        self.n_events_selected_topo = self.ROOT_topo["TwoTagCutflow"].values[1]
        self.NTuple_topo = self.ROOT_topo["TwoTagCutflow"].values[2]
        self.TriggerMap_count_topo = self.ROOT_topo["TwoTagCutflow"].values[3]

        #Triggers are in commmon between pflow and topo
        self.triggers_filename = object['triggers']
        self.triggers = list(set(u.read_input_triggers(self.triggers_filename))) #Unique triggers only

        #Initialize PFlow File
        if object['pflow'] is not None:
            self.is_pflow = True
            self.filename_pflow = object['pflow']
            self.ROOT_pflow = uproot.open(self.filename_pflow)
            self.df_TriggerList_pflow = self.ROOT_pflow["triggerList"].pandas.df()
            self.df_fmp_pflow = self.ROOT_pflow["fullmassplane"].pandas.df()
            self.n_events_initial_pflow = self.ROOT_pflow['TwoTagCutflow'].values[0]
            self.n_events_selected_pflow = self.ROOT_pflow['TwoTagCutflow'].values[1]
            self.NTuple_pflow = self.ROOT_pflow['TwoTagCutflow'].values[2]
            self.TriggerMap_count_pflow = self.ROOT_pflow['TwoTagCutflow'].values[3]
        else:
            self.is_pflow = False

        #Output h5 file name
        self.output_filename = object['output']

    def MergeFrames(self, how = "outer"):

        #self.df_merged = pd.merge(self.df_TriggerList_after, self.df_EventInfo, how, left_on = ["eventNumber", "runNumber"], right_on = ["event_number", "run_number"])
        self.df_merged_topo = pd.merge(self.df_TriggerList_topo, self.df_fmp_topo, how, left_on = ["eventNumber"], right_on = ["event_number"])

        if self.is_pflow:
            self.df_merged_pflow = pd.merge(self.df_TriggerList_pflow, self.df_fmp_pflow, how, left_on = ['eventNumber'], right_on = ['event_number'])

    def DecodeTriggerMap(self):
        def decode_dict(row):
            decoded_dict = {}
            for k, v in row.items():
                decoded_dict[k.decode()] = v
            return decoded_dict

        self.df_merged_topo['triggerMap'] = self.df_merged_topo['triggerMap'].apply(lambda row: decode_dict(row))

        print(self.df_merged_topo)

        if self.is_pflow:
            self.df_merged_pflow['triggerMap'] = self.df_merged_pflow['triggerMap'].apply(lambda row: decode_dict(row))



    def Compression(self):

        self.df_merged_topo = self.df_merged_topo.drop(['runNumber', 'event_number', 'tag_h1_j1', 'tag_h1_j2', 'tag_h2_j1', 'tag_h2_j2'], axis = 1)

        if self.is_pflow:
            self.df_merged_pflow = self.df_merged_pflow.drop(['runNumber', 'event_number', 'tag_h1_j1', 'tag_h1_j2', 'tag_h2_j1', 'tag_h2_j2'], axis = 1)



    def SaveDataH5(self):

        store = HDFStore(self.output_filename)
        if self.sample:
            store.put('df_topo', self.df_merged_topo.sample(n=self.Nevents_sample).reset_index(drop = True))
        else:
            store.put('df_topo', self.df_merged_topo)
        #Store other information as metadata for the frame in the store.
        store.get_storer('df_topo').attrs.triggers = self.triggers
        store.get_storer('df_topo').attrs.n_events_initial = self.n_events_initial_topo
        store.get_storer('df_topo').attrs.n_events_selected = self.n_events_selected_topo
        store.get_storer('df_topo').attrs.NTuple = self.NTuple_topo
        store.get_storer('df_topo').attrs.TriggerMap_count = self.TriggerMap_count_topo
        store.get_storer('df_topo').attrs.filename_topo = self.filename_topo
        store.get_storer('df_topo').attrs.triggers_filename = self.triggers_filename

        if self.is_pflow:

            if self.sample:
                store.put('df_pflow', self.df_merged_pflow.sample(n=self.Nevents_sample).reset_index(drop = True))
            else:
                store.put('df_pflow', self.df_merged_pflow)
            #Storing extra information as metadata (some of it is common with df_topo)
            store.get_storer('df_pflow').attrs.triggers = self.triggers
            store.get_storer('df_pflow').attrs.n_events_initial = self.n_events_initial_pflow
            store.get_storer('df_pflow').attrs.n_events_selected = self.n_events_selected_pflow
            store.get_storer('df_pflow').attrs.NTuple = self.NTuple_pflow
            store.get_storer('df_pflow').attrs.TriggerMap_count = self.TriggerMap_count_pflow
            store.get_storer('df_pflow').attrs.input_filename = self.filename_pflow
            store.get_storer('df_pflow').attrs.triggers_filename = self.triggers_filename



        store.close()
        print("Merged dataframe save to HDFStore with metadata as: ", self.output_filename)



if __name__ == "__main__":
    parser = argparse.ArgumentParser( description = " Please give the correct inputs")
    parser.add_argument('-i','--input-file', required = True, dest = 'input', metavar = 'input', type = str, help = "Input ROOT file to read trees and merge.")
    parser.add_argument('-t','--triggers', required = True, dest = 'triggers', metavar = 'triggers', type = str, help = "csv file containing the list of possible triggers")
    parser.add_argument('-p','--pflow', required = False, dest = 'pflow', metavar = 'pflow', type = str, help = 'pflow ROOT file path (if desired)')
    parser.add_argument('-o','--output-file', required = False, dest = 'output', metavar = 'output', type = str, help = "Output pickle filename, if none provided default is used.",  default = "dataframesH5/dataframe.h5")
    parser.add_argument('-c','--compression', required = False, dest='compression', action = 'store_true', default = True, help = "compress H5 store by removing redudant data")
    parser.add_argument('-N','--Nevents', required = False, dest='Nevents', type = int, help = 'Number of events to save (if none, saves full triggerMap)')
    arguments = parser.parse_args()
    arguments = vars(arguments)

    print(arguments)

    MT = MergeTrees(arguments)
    MT.MergeFrames()
    MT.DecodeTriggerMap()

    if arguments['compression']:
        MT.Compression()

    MT.SaveDataH5()
