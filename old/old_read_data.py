import uproot
import pandas as pd
from pandas import DataFrame, HDFStore
import argparse
import utilities as u

class MergeTrees(object):

    def __init__(self, object):

        self.input_filename = object['input']
        self.output_filename = object['output']
        self.triggers_filename = object['triggers']
        self.ROOT_file = uproot.open(self.input_filename)
        self.df_TriggerList_before = self.ROOT_file["triggerList_before"].pandas.df()
        self.df_TriggerList_after  = self.ROOT_file["triggerList_after"].pandas.df()
        self.df_EventInfo = self.ROOT_file["fullmassplane"].pandas.df()
        self.n_events_initial = self.ROOT_file["TwoTagCutflow"].values[0]
        self.n_events_selected = self.ROOT_file["TwoTagCutflow"].values[1]
        self.NTuple = self.ROOT_file["TwoTagCutflow"].values[2]
        self.TriggerMap_count = self.ROOT_file["TwoTagCutflow"].values[3]
        self.triggers = u.read_input_triggers(self.triggers_filename)

    def MergeFrames(self, how = "inner"):

        self.df_merged = pd.merge(self.df_TriggerList_after, self.df_EventInfo, how, left_on = ["eventNumber", "runNumber"], right_on = ["event_number", "run_number"])


    def SanityCheck(self):

        length_TriggerList = len(self.df_TriggerList_after)
        legnth_EventInfo = len(self.df_EventInfo)
        df_outer = self.Merge(self.df_TriggerList_after, self.df_EventInfo, how = "outer")
        df_inner = self.Merge(self.df_TriggerList_after, self.df_EventInfo, how = "inner")
        length_outer = len(df_outer)
        length_inner = len(df_inner)

        if length_TriggerList != lenghth_EventInfo:
            raise RuntimeError("The length of the TriggerList does not match the length of the EventInfo")
        if length_EventInfo != length_inner:
            raise RuntimeError("The number of events in the EventInfo frame does not match the number of events in the merged (inner) frame")
        if length_outer != length_inner:
            raise RuntimeError("The number of events in the outer merged frame does not match the number of events in the inner frame ")

    def SaveDataH5(self):

        store = HDFStore(self.output_filename)
        store.put('df', self.df_merged)
        #Store other information as metadata for the frame in the store.
        store.get_storer('df').attrs.triggers = self.triggers
        store.get_storer('df').attrs.n_events_initial = self.n_events_initial
        store.get_storer('df').attrs.n_events_selected = self.n_events_selected
        store.get_storer('df').attrs.NTuple = self.NTuple
        store.get_storer('df').attrs.TriggerMap_count = self.TriggerMap_count
        store.get_storer('df').attrs.input_filename = self.input_filename
        store.get_storer('df').attrs.triggers_filename = self.triggers_filename
        store.put('df_TriggerList_before', self.df_TriggerList_before)

        store.close()
        print("Merged dataframe save to HDFStore with metadata as: ", self.output_filename)


        #self.df_merged.to_pickle(self.output_filename)
        #print("Merged dataframe saved to pickle file as: ", self.output_filename)


if __name__ == "__main__":
    parser = argparse.ArgumentParser( description = " Please give the correct inputs")
    parser.add_argument('-i','--input-file', required = True, dest = 'input', metavar = 'input', type = str, help = "Input ROOT file to read trees and merge.")
    parser.add_argument('-t','--triggers', required = True, dest = 'triggers', metavar = 'triggers', type = str, help = "csv file containing the list of possible triggers")
    parser.add_argument('-o','--output-file', required = False, dest = 'output', metavar = 'output', type = str, help = "Output pickle filename, if none provided default is used.",  default = "dataframesH5/dataframe.h5")
    parser.add_argument('-s','--sanity-check', required = False, dest='sanity', action = 'store_true', default = False)
    arguments = parser.parse_args()
    arguments = vars(arguments)

    print(arguments)

    MT = MergeTrees(arguments)
    MT.MergeFrames()

    if arguments["sanity"]:
        MT.SanityCheck()

    MT.SaveDataH5()
