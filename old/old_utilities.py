from __future__ import print_function
import numpy as np
import histogram_helper as hh

def read_input_triggers(filename):
    """
    Reads the csv file containing the list of triggers used in the resolved reconstruction. Care needs to be taken here
    as the list contains all entries on the first row
    :param filename: string/ path to specify file location
    :return: trigger_list containing all the trigger names as strings
    """
    import csv
    trigger_list = []

    with open(filename) as csvfile:
        csv_parser = csv.reader(csvfile, delimiter = ',')
        for row in csv_parser:
            for i in range(len(row)):
                trigger_list.append(row[i])
    return trigger_list


def tree_2_df(filename, treename):
    """
    Converts information from root file into a pandas dataframe using the uproot package.
    :param filename: string/ path pointing to file location
    :param treename: particular tree of root file to be converted.
    :return: dataframe containing the tree information.
    """
    import uproot
    import pandas as pd

    file = uproot.open(filename)

    tree = file[treename]
    dataframe = tree.pandas.df()

    return dataframe

def passed_triggers_counts(triggerMap, trigger_list, normalization):
    """
    The trigger Map information is stored in the triggerMap column of the dataframe as dictionaries, this function
    is used to check whether the keys in each dictionary match the any of the triggers in the trigger list and counts
    them.
    :param triggerMap: triggerMap column of the dataframe
    :param trigger_list: list of triggers
    :return: frequency, frequency_density and percentage of events passing each trigger.
    """
    counts = np.zeros(len(trigger_list))

    for i in range(len(triggerMap)):
        for j in range(len(trigger_list)):
            trigger = trigger_list[j].encode()
            if trigger in triggerMap[i]:
                counts[j] += 1

    frequency = counts
    frequency_density = counts / normalization # normalize by the length
    percentage = 100 * frequency_density
    return frequency, frequency_density, percentage

def trigger_histogram(dataframe, column, trigger, feature):
    """
    selects out of a dataframe the events that passed a specific trigger along with a desired feature infomration
    :param dataframe: full dataframe containing the triggerMap and event information
    :param column: name of the triggerMap column (sometimes its different)
    :param trigger: string of trigger to select out of the dataframe
    :param feature: desired feature/ column to look at.
    :return:
    """
    import copy
    mask = copy.deepcopy(dataframe[column].values)
    trigger = trigger.encode()
    for i in range(len(mask)):
        mask[i] = trigger in mask[i].keys()

    df_reduced = dataframe[mask]

    histogram = df_reduced[feature].values

    return histogram
