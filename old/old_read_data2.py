import uproot
import pandas as pd
from pandas import DataFrame, HDFStore
import argparse
import utilities as u

class MergeTrees(object):

    def __init__(self, object):

        #Initialize EM Topo File
        self.filename_topo = object['input']
        self.ROOT_topo = uproot.open(self.filename_topo)
        self.df_TriggerList_topo = self.ROOT_topo["triggerList"].pandas.df()
        self.df_fmp_topo = self.ROOT_topo["fullmassplane"].pandas.df()
        self.n_events_initial_topo = self.ROOT_topo["TwoTagCutflow"].values[0]
        self.n_events_selected_topo = self.ROOT_topo["TwoTagCutflow"].values[1]
        self.NTuple_topo = self.ROOT_topo["TwoTagCutflow"].values[2]
        self.TriggerMap_count_topo = self.ROOT_topo["TwoTagCutflow"].values[3]

        #Triggers are in commmon between pflow and topo
        self.triggers_filename = object['triggers']
        self.triggers = list(set(u.read_input_triggers(self.triggers_filename))) #Unique triggers only

        #Initialize PFlow File
        if object['pflow'] is not None:
            self.is_pflow = True
            self.filename_pflow = object['pflow']
            self.ROOT_pflow = uproot.open(self.filename_pflow)
            self.df_TriggerList_pflow = self.ROOT_pflow["triggerList"].pandas.df()
            self.df_fmp_pflow = self.ROOT_pflow["fullmassplane"].pandas.df()
            self.n_events_initial_pflow = self.ROOT_pflow['TwoTagCutflow'].values[0]
            self.n_events_selected_pflow = self.ROOT_pflow['TwoTagCutflow'].values[1]
            self.NTuple_pflow = self.ROOT_pflow['TwoTagCutflow'].values[2]
            self.TriggerMap_count_pflow = self.ROOT_pflow['TwoTagCutflow'].values[3]
        else:
            self.is_pflow = False

        #Output h5 file name
        self.output_filename = object['output']

    def MergeFrames(self, how = "inner"):

        #self.df_merged = pd.merge(self.df_TriggerList_after, self.df_EventInfo, how, left_on = ["eventNumber", "runNumber"], right_on = ["event_number", "run_number"])
        self.df_merged_topo = pd.merge(self.df_TriggerList_topo, self.df_fmp_topo, how, left_on = ["eventNumber"], right_on = ["event_number"])

        if self.is_pflow:
            self.df_merged_pflow = pd.merge(self.df_TriggerList_pflow, self.df_fmp_pflow, how, left_on = ['eventNumber'], right_on = ['event_number'])

    def SanityCheck(self):

        length_TriggerList = len(self.df_TriggerList_after)
        legnth_EventInfo = len(self.df_EventInfo)
        df_outer = self.Merge(self.df_TriggerList_after, self.df_EventInfo, how = "outer")
        df_inner = self.Merge(self.df_TriggerList_after, self.df_EventInfo, how = "inner")
        length_outer = len(df_outer)
        length_inner = len(df_inner)

        if length_TriggerList != lenghth_EventInfo:
            raise RuntimeError("The length of the TriggerList does not match the length of the EventInfo")
        if length_EventInfo != length_inner:
            raise RuntimeError("The number of events in the EventInfo frame does not match the number of events in the merged (inner) frame")
        if length_outer != length_inner:
            raise RuntimeError("The number of events in the outer merged frame does not match the number of events in the inner frame ")

    def SaveDataH5(self):

        store = HDFStore(self.output_filename)
        store.put('df_topo', self.df_merged_topo)
        #Store other information as metadata for the frame in the store.
        store.get_storer('df_topo').attrs.triggers = self.triggers
        store.get_storer('df_topo').attrs.n_events_initial = self.n_events_initial_topo
        store.get_storer('df_topo').attrs.n_events_selected = self.n_events_selected_topo
        store.get_storer('df_topo').attrs.NTuple = self.NTuple_topo
        store.get_storer('df_topo').attrs.TriggerMap_count = self.TriggerMap_count_topo
        store.get_storer('df_topo').attrs.filename_topo = self.filename_topo
        store.get_storer('df_topo').attrs.triggers_filename = self.triggers_filename
        store.put('df_full_topo', self.df_TriggerList_topo)

        if self.is_pflow:
            store.put('df_pflow', self.df_merged_pflow)
            #Storing extra information as metadata (some of it is common with df_topo)
            store.get_storer('df_pflow').attrs.triggers = self.triggers
            store.get_storer('df_pflow').attrs.n_events_initial = self.n_events_initial_pflow
            store.get_storer('df_pflow').attrs.n_events_selected = self.n_events_selected_pflow
            store.get_storer('df_pflow').attrs.NTuple = self.NTuple_pflow
            store.get_storer('df_pflow').attrs.TriggerMap_count = self.TriggerMap_count_pflow
            store.get_storer('df_pflow').attrs.input_filename = self.filename_pflow
            store.get_storer('df_pflow').attrs.triggers_filename = self.triggers_filename

            store.put('df_full_pflow', self.df_TriggerList_pflow)


        store.close()
        print("Merged dataframe save to HDFStore with metadata as: ", self.output_filename)



if __name__ == "__main__":
    parser = argparse.ArgumentParser( description = " Please give the correct inputs")
    parser.add_argument('-i','--input-file', required = True, dest = 'input', metavar = 'input', type = str, help = "Input ROOT file to read trees and merge.")
    parser.add_argument('-t','--triggers', required = True, dest = 'triggers', metavar = 'triggers', type = str, help = "csv file containing the list of possible triggers")
    parser.add_argument('-p','--pflow', required = False, dest = 'pflow', metavar = 'pflow', type = str, help = 'pflow ROOT file path (if desired)')
    parser.add_argument('-o','--output-file', required = False, dest = 'output', metavar = 'output', type = str, help = "Output pickle filename, if none provided default is used.",  default = "dataframesH5/dataframe.h5")
    parser.add_argument('-s','--sanity-check', required = False, dest='sanity', action = 'store_true', default = False)
    arguments = parser.parse_args()
    arguments = vars(arguments)

    print(arguments)

    MT = MergeTrees(arguments)
    MT.MergeFrames()

    if arguments["sanity"]:
        MT.SanityCheck()

    MT.SaveDataH5()
