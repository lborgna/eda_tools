from __future__ import print_function
import sys, os
sys.path.append(os.path.dirname(__file__))

#Importing tools from Nicole Hartman Oct 2019 to produce statistical analysis

import numpy as np
import uproot
from uproot_methods.classes.TLorentzVector import TLorentzVectorArray

#not importing hep_ml since i don't need to include bdt reweight
import pickle
import pyhf

versionNum = int(pyhf.__version__.split('.')[1])
if versionNum > 1:
    from pyhf.infer import hypotest
else:
    from pyhf.utils import hypotest


import hep_ml
from hep_ml import reweight

import utilities as u

import pyhf_models_description as pmd 


def getExpectedBand(s,b,mus,alpha=0.05):
    '''
    Given histograms for signal and a background and a grid of mu values to test,
    return a list for the -2 sigma, -1 sigma, exp mu, +1 sigma, +2 sigma limits.
    '''

    # Define the pyhf model
    mi = pmd.simple(list(s), list(b))

    # Get the scan over mu
    hypo_tests = [hypotest(mu, mi.expected_data([0]), mi,
                                      0.5, [(0,np.max(mus))],
                                      return_expected_set=True,
                                      return_test_statistics=True,
                                      qtilde=True)
                  for mu in mus]

    cls_exp = [np.array([test[1][i] for test in hypo_tests]).flatten() for i in range(5)]

    # Invert the interval
    band = [np.interp(alpha, list(reversed(y_vals)), list(reversed(mus)))
            for y_vals in cls_exp]

    return band

def get2channelband(s1, b1, s2, b2, mus, alpha = 0.05):

    # defining the model pyhf
    mi = pmd.twochannel(list(s1), list(b1), list(s2), list(b2))

    #Get the scanb over mu
    hypo_tests = [hypotest(mu, mi.expected_data([0]), mi,
                                     0.5, [(0, np.max(mus))],
                                     return_expected_set = True,
                                     return_test_statistics = True,
                                     qtilde= True)
                 for mu in mus]

    cls_exp = [np.array([test[1][i] for test in hypo_tests]).flatten() for i in range(5)]

    #Invert the interval
    band = [np.interp(alpha, list(reversed(y_vals)), list(reversed(mus)))
           for y_vals in cls_exp]
    return band

def getSystBand3channel(s1, b1, var1,s2,b2,var2, s3, b3, var3,  mus, alpha = 0.05):

    # defining the model pyhf
    mi = pmd.bkgSyst3channelHT(list(s1),  list(b1), var1, list(s2), list(b2), var2, list(s3), list(b3), var3)

    #Get the scanb over mu
    hypo_tests = [hypotest(mu, mi.expected_data([0, 0, 0]), mi,
                                     [0.5, 1, 1], [(0, np.max(mus)), (-1, 1), (-20, 20)],
                                     return_expected_set = True,
                                     return_test_statistics = True,
                                     qtilde= True)
                 for mu in mus]

    cls_exp = [np.array([test[1][i] for test in hypo_tests]).flatten() for i in range(5)]

    #Invert the interval
    band = [np.interp(alpha, list(reversed(y_vals)), list(reversed(mus)))
           for y_vals in cls_exp]
    return band

def getSystBand2channelnoHT(s1, b1, var1,s2,b2,var2,mus, alpha = 0.05):
    print("hi")
    # defining the model pyhf
    mi = pmd.bkgSyst2channel(list(s1),  list(b1), var1, list(s2), list(b2), var2)

    #Get the scanb over mu
    hypo_tests = [hypotest(mu, mi.expected_data([0, 0]), mi,
                                     [0.5, 1], [(0, np.max(mus)), (-1, 1)],
                                     return_expected_set = True,
                                     return_test_statistics = True,
                                     qtilde= True)
                 for mu in mus]

    cls_exp = [np.array([test[1][i] for test in hypo_tests]).flatten() for i in range(5)]

    #Invert the interval
    band = [np.interp(alpha, list(reversed(y_vals)), list(reversed(mus)))
           for y_vals in cls_exp]
    return band

def getSystBand2channelHT(s1, b1, var1,s2,b2,var2,mus, alpha = 0.05):

    # defining the model pyhf
    mi = pmd.bkgSyst2channelHT(list(s1),  list(b1), var1, list(s2), list(b2), var2)

    #Get the scanb over mu
    hypo_tests = [hypotest(mu, mi.expected_data([0, 0, 0]), mi,
                                     [0.5, 1, 1], [(0, np.max(mus)), (-1, 1), (-20, 20)],
                                     return_expected_set = True,
                                     return_test_statistics = True,
                                     qtilde= True)
                 for mu in mus]

    cls_exp = [np.array([test[1][i] for test in hypo_tests]).flatten() for i in range(5)]

    #Invert the interval
    band = [np.interp(alpha, list(reversed(y_vals)), list(reversed(mus)))
           for y_vals in cls_exp]
    return band

def getSystBand(s, b, var, mus, alpha = 0.05):

    # defining the model pyhf
    mi = pmd.bkgSyst(list(s),  list(b), var)

    #Get the scanb over mu
    hypo_tests = [hypotest(mu, mi.expected_data([0, 0, 0]), mi,
                                     [0.5, 1, 1], [(0, np.max(mus)), (-1, 1), (-20, 20)],
                                     return_expected_set = True,
                                     return_test_statistics = True,
                                     qtilde= True)
                 for mu in mus]

    cls_exp = [np.array([test[1][i] for test in hypo_tests]).flatten() for i in range(5)]

    #Invert the interval
    band = [np.interp(alpha, list(reversed(y_vals)), list(reversed(mus)))
           for y_vals in cls_exp]
    return band

def get4channelband(s1, b1, s2, b2, s3, b3, s4, b4, mus, alpha = 0.05):

    # defining the model pyhf
    mi = pmd.fourchannel(list(s1), list(b1), list(s2), list(b2), list(s3), list(b3), list(s4), list(b4))

    #Get the scanb over mu
    hypo_tests = [hypotest(mu, mi.expected_data([0]), mi,
                                     0.5, [(0, np.max(mus))],
                                     return_expected_set = True,
                                     return_test_statistics = True,
                                     qtilde= True)
                 for mu in mus]

    cls_exp = [np.array([test[1][i] for test in hypo_tests]).flatten() for i in range(5)]

    #Invert the interval
    band = [np.interp(alpha, list(reversed(y_vals)), list(reversed(mus)))
           for y_vals in cls_exp]
    return band

def getSystBand4channel(s1, b1, var1,s2,b2,var2, s3, b3, var3, s4, b4, var4,  mus, alpha = 0.05):

    # defining the model pyhf
    mi = pmd.bkgSyst4channelHT(list(s1),  list(b1), var1, list(s2), list(b2), var2, list(s3), list(b3), var3, list(s4), list(b4), var4)

    #Get the scanb over mu
    hypo_tests = [hypotest(mu, mi.expected_data([0, 0, 0]), mi,
                                     [0.5, 1, 1], [(0, np.max(mus)), (-1, 1), (-20, 20)],
                                     return_expected_set = True,
                                     return_test_statistics = True,
                                     qtilde= True)
                 for mu in mus]

    cls_exp = [np.array([test[1][i] for test in hypo_tests]).flatten() for i in range(5)]

    #Invert the interval
    band = [np.interp(alpha, list(reversed(y_vals)), list(reversed(mus)))
           for y_vals in cls_exp]
    return band