
import sys, os
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
sys.path.append(os.path.dirname(__file__))
import argparse

import pandas as pd

import NN_RW as nn

import numpy as np
from numpy import savez_compressed


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description= "please give me the correct inputs")
    parser.add_argument('-i', '--input-file', required = True, dest = 'input', type = str)
    parser.add_argument('-n', '--nruns', required = True, dest = 'nruns', type = int)
    parser.add_argument('-o', '--output-file', required = True, dest ='output', type = str)
    parser.add_argument('-e', '--epochs', required= True, dest='epochs', type = int)
    arguments  = parser.parse_args()
    arguments = vars(arguments)

    df = pd.read_pickle(arguments['input'])
    nruns = arguments['nruns']
    fname = arguments['output']
    epochs = arguments['epochs']


    weights, _, _ = nn.repeatNNtrain(df, 
                                    repeat = nruns,
                                    epochs = epochs,
                                    batch_size = 1024)

    savez_compressed(fname, np.exp(weights))
