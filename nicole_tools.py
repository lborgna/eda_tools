from __future__ import print_function
import sys, os
sys.path.append(os.path.dirname(__file__))

#Importing tools from Nicole Hartman Oct 2019 to produce statistical analysis

import numpy as np
import uproot
from uproot_methods.classes.TLorentzVector import TLorentzVectorArray

import os
#not importing hep_ml since i don't need to include bdt reweight
import pickle
import pyhf
import hep_ml
from hep_ml import reweight

import utilities as u


def getExpectedBand(s,b,mus,a1 = 0.5, a2 = 0, a3 = None, alpha=0.05):
    '''
    Given histograms for signal and a background and a grid of mu values to test,
    return a list for the -2 sigma, -1 sigma, exp mu, +1 sigma, +2 sigma limits.
    '''
    if a3 is None:
        a3 = np.max(mus)
    # Define the pyhf model
    mi = simple(list(s), list(b))

    # Get the scan over mu
    hypo_tests = [pyhf.utils.hypotest(mu, mi.expected_data([0]), mi,
                                      a1, [(a2,a3)],
                                      return_expected_set=True,
                                      return_test_statistics=True,
                                      qtilde=True)
                  for mu in mus]

    cls_exp = [np.array([test[1][i] for test in hypo_tests]).flatten() for i in range(5)]

    # Invert the interval
    band = [np.interp(alpha, list(reversed(y_vals)), list(reversed(mus)))
            for y_vals in cls_exp]

    return band

def getExpectedBandOld(s,b,mus,alpha=0.05):
    '''
    Given histograms for signal and a background and a grid of mu values to test,
    return a list for the -2 sigma, -1 sigma, exp mu, +1 sigma, +2 sigma limits.
    '''

    # Define the pyhf model
    mi = simple(list(s), list(b))

    # Get the scan over mu
    hypo_tests = [pyhf.utils.hypotest(mu, mi.expected_data([0]), mi,
                                      0.5, [(0,np.max(mus))],
                                      return_expected_set=True,
                                      return_test_statistics=True,
                                      qtilde=True)
                  for mu in mus]

    cls_exp = [np.array([test[1][i] for test in hypo_tests]).flatten() for i in range(5)]

    # Invert the interval
    band = [np.interp(alpha, list(reversed(y_vals)), list(reversed(mus)))
            for y_vals in cls_exp]

    return band



def getLambdaWeights(smnr,L=1, lambdaFile = "../data/LambdaWeightFile.root"):
    '''
    Add weights corresponding to the different lambda variations
    to the dataframe
    '''

    #lambdaFile = "../data/LambdaWeightFile.root"
    f = uproot.open(lambdaFile)

    for ttree in f.keys():

        tree = uproot.open(lambdaFile)[ttree]

        idx = np.digitize(smnr['m_hh_cor'],tree.edges)

        l = ttree.decode()[13:][:-2]
        smnr['w_lambda{}'.format(int(l))] = tree.allvalues[idx] * L * smnr['mc_sf']




def hackHistogram(n0):
    '''
    Zero out negative values of the histogram and interpolate b/w neighboring bins for bins w/o any entries
    '''

    n = np.where(n0<0, 0, n0)

    zeros = (n == 0)

    #You can't interpolate if you're in the first or last bin

    #assert (not zeros[0]) and (not zeros[1])

    n[1:-1][zeros[1:-1]] = 0.5 * (n[:-2][zeros[1:-1]] + n[2:][zeros[1:-1]])

    return n

def hackHistos2(h_s, h_b):
    """
    hack histoggrams by removing negative entries and filling them by interpolating between neighbouring bins
    then clip off zeros in edges (to help pyHF convergence)
    """
    #negative entries handled by Nicole's hackHistogram function
    h_s = hackHistogram(h_s)
    
    #find the zeros in edges and clip em
    m = h_s != 0
    lowerIndex = m.argmax() - 1
    upperIndex = m.size - m[::-1].argmax()
    
    #Clip the signal and background histograms
    h_s = h_s[lowerIndex+1:upperIndex]  #+1 since lower bound is inclusive
    h_b = h_b[lowerIndex+1:upperIndex] 
    return h_s, h_b

def hackHistos3(h_s, h_b, var):
    """
    hack histoggrams by removing negative entries and filling them by interpolating between neighbouring bins
    then clip off zeros in edges (to help pyHF convergence)
    """
    #negative entries handled by Nicole's hackHistogram function
    h_s = hackHistogram(h_s)
    
    #find the zeros in edges and clip em
    m = h_s != 0
    lowerIndex = m.argmax() - 1
    upperIndex = m.size - m[::-1].argmax()
    
    #Clip the signal and background histograms
    h_s = h_s[lowerIndex+1:upperIndex]  #+1 since lower bound is inclusive
    h_b = h_b[lowerIndex+1:upperIndex] 
    var['Low_HT'] = var['Low_HT'][lowerIndex+1:upperIndex]
    var['High_HT'] = var['High_HT'][lowerIndex+1:upperIndex]
    return h_s, h_b, var

def sigHist(df, bins = 50, hrange = (200, 1600),wc = 'mc_sf', year = 2017):
    SR4b = df[u.sigSR(df)]
    L = u.Lumi(year)
    hs, bin_edges = np.histogram(SR4b.m_hh_cor,
                                 bins = bins,
                                 weights = L * SR4b[wc],
                                 range = hrange)
    return hs, bin_edges

def bkgHist(df, bins = 50, norm = None, hrange = (200, 1600), weight_column = 'NN_d24_weight'):
    SR2b = df[u.bkgSR(df)]

    if norm is None:
        muQCD = u.getNorm(df, 2, weight_column = weight_column)
    else:
        muQCD = norm

    hb, bin_edges = np.histogram(SR2b.m_hh_cor,
                                 bins = bins,
                                 weights = muQCD * SR2b[weight_column],
                                 range = hrange)
    return hb, bin_edges


def bkgSyst(signal_data,bkg_data,var,batch_size=None):
    '''

    '''

    spec = {
        'channels':[
            {
                'name': 'singlechannel',
                'samples' : [
                    {
                        'name': 'signal',
                        'data': signal_data,
                        'modifiers': [
                            {'name': 'mu', 'type': 'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data':bkg_data,
                        'modifiers':[
                            {'name': 'low_HT',  'type':'histosys', "data":{"lo_data":var['Low_HT'][0], "hi_data":var['Low_HT'][1]}},
                            {'name': 'high_HT', 'type':'histosys', "data":{"lo_data":var['High_HT'][0],"hi_data":var['High_HT'][1]}}
                        ],
                    },
                ],
            }
        ]
    }
    return pyhf.Model(spec, batch_size=batch_size)

def bkgSyst2channel(s1, b1, v1, s2, b2, v2, batch_size = None):

    spec = {
        'channels':[
            {
                'name': 'firstchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': s1,
                        'modifiers' : [
                            {'name': 'mu', 'type': 'normfactor', 'data':None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': b1,
                        'modifiers': [
                            {'name': 'low_HT', 'type':'histosys', 'data':{'lo_data':v1['Low_HT'][0], "hi_data":v1['Low_HT'][1]}},
                            {'name': 'high_HT', 'type':'histosys', 'data':{'lo_data':v1['High_HT'][0], 'hi_data':v1['High_HT'][1]}}
                        ],
                    },
                ],
            },
            {
                'name': 'secondchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': s2,
                        'modifiers' : [
                            {'name': 'mu', 'type':'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': b2,
                        'modifiers': [
                            {'name': 'low_HT', 'type':'histosys', 'data':{'lo_data':v2['Low_HT'][0], 'hi_data':v2['Low_HT'][1]}},
                            {'name': 'high_HT', 'type':'histosys', 'data':{'lo_data':v2['High_HT'][0], 'hi_data':v2['High_HT'][1]}}
                        ],
                    },
                ],
            },
        ],
    }
    return pyhf.Model(spec, batch_size = batch_size)

def getBDTWeights(data,BDT_fname, trainBDT=False, kinematic_region=2, key = 'w_2b'):
    '''
    Add the BDT weights to the dataframe

    Note: I should add functionality for training in the SB before the cuts,
    but since min_dRjj_hc1 isn't looking promising, I'll do this later.

    Inputs:
    - data: pd DataFrame for the data
    - BDT_fname: Name of the pickle file to either save the BDT to or read it from
    - trainBDT: If true, retrain the BDT
    - kinematic_region: The region to use for the kinematic reweighting
                        * 2: SB (default)
                        * 1: CR (for the background systematic)

    '''

    mask_2b = (data.ntag == 2)&(data.kinematic_region == kinematic_region)
    mask_4b = (data.ntag >= 4)&(data.kinematic_region == kinematic_region)

    sort_rw_cols = ['pT_4', 'pT_2', 'eta_i', 'dRjj_1', 'dRjj_2', 'njets']

    if trainBDT:

        BDT_params = {
            'n_estimators' : 50,
            'learning_rate' : 0.1,
            'max_depth' : 3,
            'min_samples_leaf' : 125,
            'gb_args' : {'subsample': 0.4}
            }

        reweighter = reweight.GBReweighter(n_estimators=BDT_params['n_estimators'],
                                           learning_rate=BDT_params['learning_rate'],
                                           max_depth=BDT_params['max_depth'],
                                           min_samples_leaf=BDT_params['min_samples_leaf'],
                                           gb_args=BDT_params['gb_args'])

        original = data.loc[mask_2b,sort_rw_cols]
        target  = data.loc[mask_4b,sort_rw_cols]

        print("Training on columns:", sort_rw_cols)
        reweighter.fit(original, target)

        pickle.dump(reweighter, open( BDT_fname, "wb" ))

    else:

        reweighter = pickle.load(open( BDT_fname, "rb" ))


    #N4bTo2b = np.sum(mask_4b) / np.sum(mask_2b)
    #key = "w_2b"
    if kinematic_region == 1:
        key += "_CR"
    data[key] = reweighter.predict_weights(data[sort_rw_cols])


def calculateHT(*args):

    list_of_df = []
    for arg in args:
        list_of_df.append(arg)

    pT_columns = ['pT_h1_j1', 'pT_h1_j2', 'pT_h2_j1', 'pT_h2_j2']

    for df in list_of_df:
        df['HT_hc'] = np.sum(df[pT_columns], axis = 1)


def calculate_m4j(*args):
    list_of_df = []
    for arg in args:
        list_of_df.append(arg)

    mH = 125

    for df in list_of_df:
        hc1 = TLorentzVectorArray.from_ptetaphim(df.pT_h1, df.eta_h1, df.phi_h1, df.m_h1)
        hc2 = TLorentzVectorArray.from_ptetaphim(df.pT_h2, df.eta_h2, df.phi_h2, df.m_h2)

        cor_hc1 = mH * hc1._to_cartesian() / hc1.mag
        cor_hc2 = mH * hc2._to_cartesian() / hc2.mag

        df['m_hh_cor'] = (cor_hc1 + cor_hc2).mag
    
def threechannel(sig1, bkg1, sig2, bkg2, sig3, bkg3, batch_size = None):
    spec = {
        'channels':[
            {
                'name': 'firstchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': sig1,
                        'modifiers': [
                            {'name': 'mu', 'type': 'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': bkg1,
                        'modifiers':[
                        ],
                    },
                ],
            },
            {
                'name': 'secondchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': sig2,
                        'modifiers': [
                            {'name': 'mu', 'type': 'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': bkg2,
                        'modifiers':[
                        ],
                    },
                ],
            },
            {
                'name': 'thirdchannel',
                'samples': [
                    {
                        'name': 'signal',
                        'data': sig3,
                        'modifiers': [
                            {'name': 'mu', 'type': 'normfactor', 'data': None}
                        ],
                    },
                    {
                        'name': 'background',
                        'data': bkg3,
                        'modifiers':[
                        ],
                    },
                ],
            }
        ]
    }
    return pyhf.Model(spec, batch_size = batch_size)

def get3channelband(s1, b1, s2, b2, s3, b3, mus, alpha = 0.05):

    # defining the model pyhf
    mi = threechannel(list(s1), list(b1), list(s2), list(b2), list(s3), list(b3))

    #Get the scanb over mu
    hypo_tests = [pyhf.utils.hypotest(mu, mi.expected_data([0]), mi,
                                     0.5, [(0, np.max(mus))],
                                     return_expected_set = True,
                                     return_test_statistics = True,
                                     qtilde= True)
                 for mu in mus]

    cls_exp = [np.array([test[1][i] for test in hypo_tests]).flatten() for i in range(5)]

    #Invert the interval
    band = [np.interp(alpha, list(reversed(y_vals)), list(reversed(mus)))
           for y_vals in cls_exp]
    return band

def getSystBand(s, b, var, mus, alpha = 0.05):

    # defining the model pyhf
    mi = bkgSyst(list(s),  list(b), var)

    #Get the scanb over mu
    hypo_tests = [pyhf.utils.hypotest(mu, mi.expected_data([0, 0, 0]), mi,
                                     [0.5, 1, 1], [(0, np.max(mus)), (-1, 1), (-20, 20)],
                                     return_expected_set = True,
                                     return_test_statistics = True,
                                     qtilde= True)
                 for mu in mus]

    cls_exp = [np.array([test[1][i] for test in hypo_tests]).flatten() for i in range(5)]

    #Invert the interval
    band = [np.interp(alpha, list(reversed(y_vals)), list(reversed(mus)))
           for y_vals in cls_exp]
    return band

def pT100Mask(df, pT_cut = 100):
    mask = (df.pT_h1_j1 > pT_cut) | (df.pT_h1_j2 > pT_cut) | (df.pT_h2_j1 > pT_cut) | (df.pT_h2_j2 > pT_cut)

    return mask

def pTcut(df, pT_cut = 100):
    mask = (df.pT_h1_j1 > pT_cut) | (df.pT_h1_j2 > pT_cut) | (df.pT_h2_j1 > pT_cut) | (df.pT_h2_j2 > pT_cut)

    return mask

def HT300Mask(df, HT_cut = 300):
    mask = df.HT > HT_cut

    return mask

def bkgSR(df):

    return ((df.kinematic_region == 0) & (df.ntag == 2))

def sigSR(df):

    return ((df.kinematic_region == 0) & (df.ntag >= 4))

def bkgSystematics(df, bins = 50, range = (250, 1400), feature = 'm_hh_cor', SB_weights = 'BDT_d24_weight_18', CR_weights = 'BDT_d24_weight_CRderiv'):

    bkg_SR = bkgSR(df)
    #sig_SR = sigSR(df)

    NormSB = u.getNorm(df, 2)
    NormCR = u.getNorm(df, 1)

    var = {}

    sb, edges = np.histogram(df.loc[bkg_SR, feature],
                                bins = bins,
                                range = range,
                                weights = NormSB * df.loc[bkg_SR, SB_weights]
                                )

    HTbins = [(df.HT < 300), (df.HT >= 300)]
    labels =['Low', 'High']
    for HT, tag in zip(HTbins, labels):
        cr, _     = np.histogram(df.loc[bkg_SR, feature],
                                    bins = bins,
                                    range = range,
                                    weights = np.where(HT[bkg_SR], NormCR * df.loc[bkg_SR, CR_weights], NormSB * df.loc[bkg_SR, SB_weights])
                                    )

        inv, _    = np.histogram(df.loc[bkg_SR, feature],
                                    bins = bins,
                                    range = range,
                                    weights = np.where(HT[bkg_SR], 2 * NormSB * df.loc[bkg_SR, SB_weights] - NormCR * df.loc[bkg_SR, CR_weights], NormSB * df.loc[bkg_SR, SB_weights])
                                    )
        
        eps = 1e-15
        var[f'{tag}_HT'] = [list(cr + eps), list(inv + eps)]

    return sb, var, edges

