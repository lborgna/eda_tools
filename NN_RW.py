from __future__ import print_function
import sys, os
sys.path.append(os.path.dirname(__file__))

import numpy as np
import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import keras.backend as K

from keras.models import Model
from keras.layers import Dense, Dropout, Input
from keras.callbacks import EarlyStopping, ModelCheckpoint

from keras.layers.normalization import BatchNormalization
from tqdm import tqdm

import time

def louppe_loss(y_true, y_pred):
    return (y_true * (K.sqrt(K.exp(y_pred))) + (1.0 - y_true) * (1.0 / K.sqrt(K.exp(y_pred))))


def log_inputs(df, to_log = ['pT_4', 'pT_2', 'dRjj_1', 'dRjj_2', 'pt_hh']):

    for tl in to_log:
        if str(tl +'_log') in df.keys():
            print(tl, 'already in dataset, skipping')
            continue
        df[tl+'_log'] = np.log(df[tl])

def create_labels(original, target):

    Y_all = []

    for _df, ID in [(original, 1), (target, 0)]:
        Y_all.extend([ID] * _df.shape[0])
    Y_all = np.array(Y_all)

    return Y_all

def NNmodel(X_all):
    inputs = Input(shape=(X_all.shape[1],))
    hidden = Dense(20, activation = 'relu')(inputs)
    hidden = BatchNormalization()(hidden)
    hidden = Dropout(0.3)(hidden)
    hidden = Dense(20, activation = 'relu')(hidden)
    hidden = Dropout(0.3)(hidden)
    hidden = Dense(20, activation = 'relu')(hidden)
    outputs = Dense(1, activation = 'linear')(hidden)

    model = Model(inputs, outputs)

    return model

def VanillaNN(in_shape):
    inputs = Input(shape = (in_shape[1], ))
    hidden = Dense(20, activation = 'relu')(inputs)
    hidden = Dense(20, activation = 'relu')(hidden)
    hidden = Dense(20, activation = 'relu')(hidden)
    outputs = Dense(1, activation = 'linear')(hidden)

    model = Model(inputs, outputs)

    return model

def NN(in_shape, size = 20, batch_norm = True):
    inputs = Input(shape = (in_shape[1],))
    hidden = Dense(size, activation = 'relu')(inputs)

    if batch_norm:
        hidden = BatchNormalization()(hidden)
    hidden = Dense(size, activation = 'relu')(hidden)
    if batch_norm:
        hidden = BatchNormalization()(hidden)
    hidden = Dense(size, activation = 'relu')(hidden)
    if batch_norm:
        hidden = BatchNormalization()(hidden)
    outputs = Dense(1, activation = 'linear')(hidden)

    model = Model(inputs, outputs)

    return model

def getNNweights(df_in, kinematic_region = 2, model = None,
            sort_rw_cols = ['pT_4_log', 'pT_2_log','eta_i', 'dRjj_1_log', 'dRjj_2_log', 'njets', 'pt_hh_log'],
            epochs = 50, 
            batch_size = 1024,
            verbose = False,
            filename = "/mnt/storage/lborgna/models/current_model.h5"):

    min_cols = sort_rw_cols + ['ntag', 'kinematic_region', 'run_number','event_number']
    df = df_in[min_cols].copy()
    log_inputs(df)

    if verbose == True:
        print(df.columns)

    train_size = len(df[df.kinematic_region == kinematic_region])
    poisson_weightsKr = np.random.poisson(1, train_size)
    df.loc[df.kinematic_region == kinematic_region, 'poisson_weight'] = poisson_weightsKr

    #Create the kinematic region dataframe
    dfKr = df[df.kinematic_region == kinematic_region] 
    dfKr.reset_index(drop = True, inplace = True)

    #Generate the poisson weights for that region
    #poisson_weightsKr = np.random.poisson(1, len(dfKr))

    #dfKr['poisson_weight'] = poisson_weightsKr

    #Get the Original distributions to use for modelling (2b)
    original = dfKr[dfKr.ntag == 2][sort_rw_cols]

    #Get dataframe of all of the 2b events (for inferencing later)
    df2b = df.loc[df.ntag == 2, sort_rw_cols]

    #Get the Target distributions to model (4b)
    target = dfKr[dfKr.ntag >= 4][sort_rw_cols]

    #Create the X_all array for NN
    X_all = pd.concat((original, target), ignore_index = True).values
    #Create the labels for the X_all 
    Y_all = create_labels(original, target)

    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_all)
    Y_train = Y_all.copy()

    if verbose == True:
        print(X_all)
        print(Y_all)
    #Shuffle, cuz why not 
    idxs = np.random.permutation(X_train.shape[0])

    X_train = X_train[idxs]
    Y_train = Y_train[idxs]

    original_weights = dfKr[dfKr.ntag == 2].poisson_weight
    target_weights = dfKr[dfKr.ntag >= 4].poisson_weight

    pW = np.concatenate((original_weights, target_weights))[idxs]
    
    if model == None:
        model = NN(X_train.shape, size = 50)
    else:
        model = model 

    model.compile(loss = louppe_loss, metrics = ['accuracy'], optimizer = 'adam')

    start = time.time()
    history = model.fit(X_train, Y_train, sample_weight = pW,
                        callbacks = [
                                EarlyStopping(monitor='val_loss', patience=10, verbose=True),
                                ModelCheckpoint(filename, monitor='val_loss', verbose=True, save_best_only=True) ],
                                epochs=epochs,
                                validation_split = 0.4,
                                batch_size=batch_size,
                       )
    end = time.time()
    loss = history.history['loss']
    acc  = history.history['accuracy']
    val_loss = history.history['val_loss']
    val_acc  = history.history['val_accuracy']
    train_time = end - start
    NNhist = {'loss':loss, 'acc': acc, 'val_loss': val_loss, 'val_acc': val_acc, 'train_time': train_time}  

    model.load_weights(filename)
    pred = model.predict(scaler.transform(df2b.values), batch_size = batch_size)

    df.loc[df.ntag == 2, 'NN_weight'] = np.exp(pred)[:, 0]
    df.loc[df.ntag != 2, 'NN_weight'] = 1

    N4b = 1.0 * np.sum(dfKr[dfKr.ntag >= 4].poisson_weight)



    N2b = np.sum(dfKr[dfKr.ntag == 2].poisson_weight.values * df[(df.ntag == 2) & (df.kinematic_region == kinematic_region)].NN_weight.values)

    muQCD = N4b/N2b

    df.loc[df.ntag == 2, 'NN_weight'] = muQCD * df.loc[df.ntag == 2, 'NN_weight']


    return df, muQCD, poisson_weightsKr, N4b, N2b, NNhist

def repeatNNtrain(df, repeat = 3, kinematic_region = 2, model = None,
            sort_rw_cols = ['pT_4_log', 'pT_2_log','eta_i', 'dRjj_1_log', 'dRjj_2_log', 'njets', 'pt_hh_log'],
            epochs = 100, 
            batch_size = 20000,
            verbose = False,
            save = False,
            model_storage_directory = "/mnt/storage/lborgna/models/"):

    num2b = len(df.loc[(df.ntag == 2)])
    kr_label = None
    if kinematic_region == 2:
        kr_label = 'CR'
    elif kinematic_region == 1:
        kr_label = 'VR'

    weights = np.zeros((num2b, repeat))
    muQCD_BS = np.zeros(repeat)
    weight_column_labels = []
    df_results = df[['event_number','run_number','ntag']].copy()
    bs_loss = pd.DataFrame({})
    bs_acc = pd.DataFrame({})
    bs_val_loss = pd.DataFrame({})
    bs_val_acc = pd.DataFrame({})

    trainTimes = np.zeros(repeat)
    for i in tqdm(range(repeat)):
        df_loss = pd.DataFrame({})
        df_acc = pd.DataFrame({})
        df_valloss = pd.DataFrame({})
        df_valacc = pd.DataFrame({})
        model_filename = model_storage_directory + 'bootstrap_n' + str(i)+'.h5' 
        df_i, muQCD, pW, N4b, N2b, NNhist = getNNweights(df, kinematic_region = kinematic_region, model = model,
            sort_rw_cols = sort_rw_cols,
            epochs = epochs, 
            batch_size = batch_size,
            verbose = verbose,
            filename = model_filename)
    
        df_loss['bs_'+str(i)] = NNhist['loss']
        df_acc['bs_'+str(i)] = NNhist['acc']
        df_valloss['bs_'+str(i)] = NNhist['val_loss']
        df_valacc['bs_'+str(i)] = NNhist['val_acc'] 
        bs_loss = pd.concat([bs_loss, df_loss], ignore_index = True, axis = 1)
        bs_acc  = pd.concat([bs_acc, df_acc], ignore_index = True, axis = 1)
        bs_val_loss = pd.concat([bs_val_loss, df_valloss], ignore_index = True, axis = 1)
        bs_val_acc = pd.concat([bs_val_acc, df_valacc], ignore_index = True, axis = 1)

        trainTimes[i] = NNhist['train_time']

        new_label = 'NN_weight' + kr_label + '_bs_' + str(i)
        weight_column_labels.append(new_label)
        df_i.rename(columns =  {'NN_weight': new_label}, inplace = True)
        df_results = pd.merge(df_results, df_i[['event_number','run_number', new_label]], how = 'inner', left_on = ['event_number', 'run_number'], right_on =['event_number','run_number'])
        muQCD_BS[i] = muQCD
    print(weight_column_labels)
    print(muQCD_BS)
    print(df_results.columns)
    df_results.loc[df_results.ntag == 2, 'NN_weight_median_'+kr_label] = np.median(df_results[df_results.ntag == 2][weight_column_labels], axis = 1)
    df_results.loc[df_results.ntag == 2, 'NN_weight_IQR_'+kr_label] = np.subtract(*np.percentile(df_results[df_results.ntag==2][weight_column_labels], [75, 25], axis = 1))
    df_results.loc[df_results.ntag != 2, 'NN_weight_median_'+kr_label] = 1
    df_results.loc[df_results.ntag != 2, 'NN_weight_IQR_' + kr_label ] = 1
     

    out_cols =['event_number', 'run_number','NN_weight_median_'+kr_label, 'NN_weight_IQR_'+kr_label] + weight_column_labels
    dfReduced = df_results[out_cols].copy()
    if save:
        dfReduced.to_pickle(model_storage_directory + 'dat_weights_'+kr_label+'.pkl')
        np.save(model_storage_directory + 'bs_norms_'+kr_label +'.npy', muQCD_BS)
        bs_loss.to_pickle(model_storage_directory + 'bs_loss.pkl')
        bs_acc.to_pickle(model_storage_directory + 'bs_acc.pkl')
        bs_val_acc.to_pickle(model_storage_directory + 'bs_val_acc.pkl')
        bs_val_loss.to_pickle(model_storage_directory + 'bs_val_loss.pkl')
        np.save(model_storage_directory + 'trainTimes.npy', trainTimes)

    return dfReduced, muQCD_BS

def append_weight_median(df, w, column_name):
    i = 0
    w_NN = []
    
    for ntag, kr in zip(df.ntag, df.kinematic_region):
        if ((ntag == 2) & (kr == 2)):
            w_NN.append(np.exp(w[i]))
            i+=1
        else:
            w_NN.append(1)
    df[column_name] = w_NN

def append_weight(df, w, column, region = 2):
    i = 0
    w_NN = []
    for ntag, kr in zip(df.ntag, df.kinematic_region):
        if ((ntag == 2) & (kr == region)):
            w_NN.append(np.exp(w[i][0]))
            i+=1
        else:
            w_NN.append(1)
    df[column] = w_NN

def append_NNweights_toframe(df, w, column):
    i = 0
    w_NN = []
    for ntag in df.ntag:
        if (ntag == 2):
            w_NN.append(np.exp(w[i][0]))
            i+=1
        else:
            w_NN.append(1)
    df[column] = w_NN

def appendWeight(df, w, column):
    i = 0
    w_NN = []
    for ntag in df.ntag:
        if (ntag == 2):
            w_NN.append(np.exp(w[i]))
            i+=1
        else:
            w_NN.append(1)
    df[column] = w_NN

def nominal():
    return {"rw_cols_to_log": ['pT_2', 'pT_4', 'dRjj_1', 'dRjj_2','pt_hh','X_wt'],
            "rw_cols_log": ['pT_2_log', 'pT_4_log', 'eta_i', 'dRjj_1_log','dRjj_2_log','njets','pt_hh_log','X_wt_log','dR_hh','dPhi_h1','dPhi_h2']}